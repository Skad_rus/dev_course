//
//  LDSTextAndColor.h
//  FontsHW
//
//  Created by Admin on 20.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSTextAndColor : NSObject

@property (strong,nonatomic) UIColor* classColor;
@property (strong,nonatomic) NSString* classText;

@end

