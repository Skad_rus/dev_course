//
//  LDSStudent.h
//  FontsHW
//
//  Created by Admin on 20.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSStudent : NSObject

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* lastName;
@property (assign, nonatomic) NSInteger point;

@end
