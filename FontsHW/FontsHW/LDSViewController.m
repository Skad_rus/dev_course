//
//  LDSViewController.m
//  FontsTest
//
//  Created by Admin on 19.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Ученик.
 
 1. Создайте таблицу которая содержит 1000 разных ячеек.
 2. для каждой ячейки генирируйте радномный цвет.
 3. RGB данные пишите в тексте ячейки, например: RGB(10,20,255)
 4. Также раскрашивайте текст ячейки этим цветом.
 
 Студент.
 
 5. Теперь создайте класс, который содержит цвет и нейм.
 6. В viewDidLoad сгенерируйте 1000 объектов такого класса по принципу из ученика
 7. Положите их в массив и отобразите в таблице
 8. В этом случае когда вы будете листать назад вы увидете те же ячейки, что там и были, а не новые рандомные
 
 Мастер.
 
 9. Возвращаемся к слудентам. Сгенерируйте 20-30 разных студентов.
 10. В таблице создавайте не дефаулт ячейку а Value1. В этом случае у вас появится еще одна UILabel - detailLabel.
 11. В textLabel пишите имя и фамилию студента, а в detailLabel его средний бал.
 12. Если средний бал низкий - окрашивайте имя студента в красный цвет
 13. Отсортируйте студентов в алфовитном порядке и отобразите в таблице
 
 Супермен.
 
 14. Средний бал для студентов ставьте рандомно от 2 до 5
 15. После того, как вы сгенерировали 30 студентов вам надо их разбить на группы:
 отличники, хорошисты, троечники и двоечники
 16. Каждая группа это секция с соответствующим названием.
 17. Студенты внутри своих групп должны быть в алфовитном порядке
 18. Отобразите группы студентов с их оченками в таблице.
 
 Mission Impossible!
 
 19. Добавьте к супермену еще одну секцию, в которой вы отобразите 10 моделей цветов из задания Студент.
 20. Помните, это должно быть 2 разных типа ячеек Value1 для студентов и Default для цветов.
 */

#import "LDSViewController.h"
#import "LDSTextAndColor.h"
#import "LDSStudent.h"

@interface LDSViewController ()

@property (strong,nonatomic) NSMutableArray* arrayOfObj;
@property (strong,nonatomic) NSMutableArray* arrayOfStudents;
@property (strong,nonatomic) NSMutableArray* arrayOfStudentsFive;
@property (strong,nonatomic) NSMutableArray* arrayOfStudentsFour;
@property (strong,nonatomic) NSMutableArray* arrayOfStudentsThree;
@property (strong,nonatomic) NSMutableArray* arrayOfStudentsTwo;

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIEdgeInsets inset = UIEdgeInsetsMake(20, 0, 0, 0);
    
    self.tableView.contentInset = inset;
    self.tableView.scrollIndicatorInsets = inset;
    
    self.arrayOfObj = [[NSMutableArray alloc] init];
    self.arrayOfStudentsFive = [[NSMutableArray alloc] init];
    self.arrayOfStudentsFour = [[NSMutableArray alloc] init];
    self.arrayOfStudentsThree = [[NSMutableArray alloc] init];
    self.arrayOfStudentsTwo = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 10; i++) {
        
        LDSTextAndColor* object = [[LDSTextAndColor alloc] init];
        
        CGFloat r = [self randomFromZeroToOne];
        CGFloat g = [self randomFromZeroToOne];
        CGFloat b = [self randomFromZeroToOne];

        object.classText = [NSString stringWithFormat:@"RGB(%1.0f,%1.0f,%1.0f)",r*100,g*100,b*100];
        object.classColor = [UIColor colorWithRed:r green:g blue:b alpha:1.f];
        
        [self.arrayOfObj addObject:object];
        
    }
    
    NSLog(@"Count = %d", [self.arrayOfObj count]);
    
    LDSStudent* human0 = [[LDSStudent alloc] init];
    LDSStudent* human1 = [[LDSStudent alloc] init];
    LDSStudent* human2 = [[LDSStudent alloc] init];
    LDSStudent* human3 = [[LDSStudent alloc] init];
    LDSStudent* human4 = [[LDSStudent alloc] init];
    LDSStudent* human5 = [[LDSStudent alloc] init];
    LDSStudent* human6 = [[LDSStudent alloc] init];
    LDSStudent* human7 = [[LDSStudent alloc] init];
    LDSStudent* human8 = [[LDSStudent alloc] init];
    LDSStudent* human9 = [[LDSStudent alloc] init];
    LDSStudent* human10 = [[LDSStudent alloc] init];
    LDSStudent* human11 = [[LDSStudent alloc] init];
    LDSStudent* human12 = [[LDSStudent alloc] init];
    LDSStudent* human13 = [[LDSStudent alloc] init];
    LDSStudent* human14 = [[LDSStudent alloc] init];
    LDSStudent* human15 = [[LDSStudent alloc] init];
    LDSStudent* human16 = [[LDSStudent alloc] init];
    LDSStudent* human17 = [[LDSStudent alloc] init];
    LDSStudent* human18 = [[LDSStudent alloc] init];
    LDSStudent* human19 = [[LDSStudent alloc] init];
    

    human0.lastName = @"Kovalski";
    human1.lastName = @"Petrov";
    human2.lastName = @"Ivanov";
    human3.lastName = @"Sidorov";
    human4.lastName = @"Sleptsov";
    human5.lastName = @"Cherkasin";
    human6.lastName = @"Knyazev";
    human7.lastName = @"Vochehovskii";
    human8.lastName = @"Domogarov";
    human9.lastName = @"Metelskii";
    human10.lastName = @"Zaharenko";
    human11.lastName = @"Shylgin";
    human12.lastName = @"House";
    human13.lastName = @"Klezkov";
    human14.lastName = @"Lich";
    human15.lastName = @"Menetil";
    human16.lastName = @"Lenin";
    human17.lastName = @"Stalin";
    human18.lastName = @"Romanov";
    human19.lastName = @"Kostukevich";
    
    self.arrayOfStudents = [[NSMutableArray alloc] initWithObjects:human0, human1, human2, human3, human4, human5, human6, human7, human8, human9, human10, human11, human12, human13, human14, human15, human16, human17, human18, human19, nil];
    
    for (LDSStudent* student in self.arrayOfStudents) {
        
        student.point = 2 + arc4random() % 4;
        
    }
    
    NSArray *sortedArray;
    sortedArray = [self.arrayOfStudents sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [a lastName];
        NSString *second = [b lastName];
        return [first compare:second];
    }];
    
    [self.arrayOfStudents removeAllObjects];
    for (LDSStudent* student in sortedArray) {
        [self.arrayOfStudents addObject:student];
    }
    
    for (LDSStudent* student in self.arrayOfStudents) {
        
        switch (student.point) {
            case 5:
                [self.arrayOfStudentsFive addObject:student];
                break;
            case 4:
                [self.arrayOfStudentsFour addObject:student];
                break;
            case 3:
                [self.arrayOfStudentsThree addObject:student];
                break;
            case 2:
                [self.arrayOfStudentsTwo addObject:student];
                break;
                
        }
    }
    
    NSLog(@"%@", self.arrayOfStudentsFive);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Methods

//Метод для задания случайного флоат от 0.f до 1.f
- (CGFloat) randomFromZeroToOne {
    
    return (float)(arc4random() % 256) / 255;
    
}

//Метод для задания произвольного цвета
- (UIColor*) randomColor {
    
    CGFloat r = [self randomFromZeroToOne];
    CGFloat g = [self randomFromZeroToOne];
    CGFloat b = [self randomFromZeroToOne];
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    NSLog(@"numberOfSectionsInTableView");
    
    return 5;
    
}

- (void) printAr:(NSMutableArray*) arr{
    for (LDSStudent* student in arr){
        NSLog(@"%d", student.point);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSLog(@"numberOfRowsInSection = %d", section);
    
    NSInteger i = 0;
    
    switch (section) {
        case 0:
            i = [self.arrayOfStudentsFive count];
            break;
        case 1:
            i = [self.arrayOfStudentsFour count];
            break;
        case 2:
            i = [self.arrayOfStudentsThree count];
            break;
        case 3:
            i = [self.arrayOfStudentsTwo count];
            break;
        case 4:
            i = 10;
            break;
    }
    
    return i;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString* returnString;
    
    switch (section) {
        case 0:
            returnString = @"Point 5";
            break;
        case 1:
            returnString = @"Point 4";
            break;
        case 2:
            returnString = @"Point 3";
            break;
        case 3:
            returnString = @"Point 2";
            break;
        case 4:
            returnString = @"Random colors";
            break;
    }
    
    return returnString;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"cellForRowAtIndexPath: {%d,%d}", indexPath.section, indexPath.row);
    
    static NSString* indentifier = @"Cell";
    static NSString* indentifierForColor = @"Color Cell";
    UITableViewCell* cell = nil;
    
    if (indexPath.section == 4) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:indentifierForColor];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifierForColor];
            NSLog(@"Color cell created");
        } else {
            NSLog(@"Color cell reused");
        }
        
    } else {
        
        cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifier];
            NSLog(@"cell created");
        } else {
            NSLog(@"cell reused");
        }
        
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    
    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = [[self.arrayOfStudentsFive objectAtIndex:indexPath.row] lastName];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [[self.arrayOfStudentsFive objectAtIndex:indexPath.row] point]];
            break;
        case 1:
            cell.textLabel.text = [[self.arrayOfStudentsFour objectAtIndex:indexPath.row] lastName];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [[self.arrayOfStudentsFour objectAtIndex:indexPath.row] point]];
            break;
        case 2:
            cell.textLabel.text = [[self.arrayOfStudentsThree objectAtIndex:indexPath.row] lastName];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [[self.arrayOfStudentsThree objectAtIndex:indexPath.row] point]];
            break;
        case 3:
            cell.textLabel.text = [[self.arrayOfStudentsTwo objectAtIndex:indexPath.row] lastName];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [[self.arrayOfStudentsTwo objectAtIndex:indexPath.row] point]];
            break;
        case 4:
            cell.textLabel.text = [[self.arrayOfObj objectAtIndex:indexPath.row] classText];
            cell.textLabel.textColor = [[self.arrayOfObj objectAtIndex:indexPath.row] classColor];
            break;
    }

    return cell;
}

@end
