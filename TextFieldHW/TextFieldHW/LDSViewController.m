//
//  LDSViewController.m
//  TextFieldHW
//
//  Created by Admin on 17.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Ученик.
 
 1. Создайте поля (и лейблы напротив как в уроке): имя, фамилия, логин, пароль, возраст, телефон, имеил адрес.
 2. Установите правильные виды клавиатур для каждого текстового поля.
 3. У всех текстовых полей кроме последнего кнопка ретерн должна быть NEXT, а у последнего DONE.
 4. Осуществите переход по кнопкам NEXT и уберите клаву с экрана кнопкой DONE.
 5. Каждое поле при нажатии должно иметь кнопку очистки
 
 Студент
 
 6. Совет, чтобы осуществить переход по NEXT без проверки тонны пропертей, попробуйте использовать UIOutletCollection
 7. Создайте соответствующие каждому текстовому полю UILabel чтобы выводить туда информацию из текстовых полей. Сделайте их мелкими и другого цвета.
 8. По изменению текста (даже буквы) обновляйте эти лейблы (не забудте про CLEAR button)
 
 Мастер
 
 9. Для поля ввода телефона используйте мой код из видео, можете поместить его в какой-то оотдельный метод если надо
 10. Этот код должен работать только для поля телефона и не для какого другого
 
 Супермен
 
 11. Для поля ввода имеила ограничте ввод символов недопустимых в адресе
 12. Более того, сибвол "@" может быть введен только 1 раз
 13. установите разумное ограничение для этого поля
*/

#import "LDSViewController.h"

typedef enum {
    
    LDSTextFieldFirstName,
    LDSTextFieldLastName,
    LDSTextFieldLogin,
    LDSTextFieldPassword,
    LDSTextFieldAge,
    LDSTextFieldPhoneNumber,
    LDSTextFieldEmail
    
}LDSTextfieldType;

@interface LDSViewController () <UITextFieldDelegate>

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    switch (textField.tag) {

        case LDSTextFieldFirstName:
            [self.lastNameField becomeFirstResponder];
            break;
            
        case LDSTextFieldLastName:
            [self.loginField becomeFirstResponder];
            break;
            
        case LDSTextFieldLogin:
            [self.passwordField becomeFirstResponder];
            break;
            
        case LDSTextFieldPassword:
            [self.ageField becomeFirstResponder];
            break;
            
        case LDSTextFieldAge:
            [self.phoneNumberField becomeFirstResponder];
            break;
            
        case LDSTextFieldPhoneNumber:
            [self.emailField becomeFirstResponder];
            break;
            
        case LDSTextFieldEmail:
            [self.emailField resignFirstResponder];
            break;
            
    }
    
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    switch (textField.tag) {
            
        case LDSTextFieldFirstName:
            self.firstNameFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];
            break;
            
        case LDSTextFieldLastName:
            self.lastNameFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldLogin:
            self.loginFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldPassword:
            self.passwordFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldAge:
            self.ageFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldPhoneNumber:
            self.phoneNumberFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldEmail:
            self.emailFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
    }
    
    if (textField.tag == LDSTextFieldAge) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet: validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
    }
    
    if (textField.tag == LDSTextFieldEmail) {
        
        if ([textField.text rangeOfString:@"@"].location != NSNotFound && [string isEqualToString:@"@"]) {
            return NO;
        }
        
        if ([textField.text rangeOfString:@"@"].location != NSNotFound) {
            
            NSRange AdRange = NSMakeRange([textField.text rangeOfString:@"@"].location, textField.text.length - [textField.text rangeOfString:@"@"].location);
            
            if ([textField.text rangeOfString:@"." options:NSCaseInsensitiveSearch range:AdRange].location != NSNotFound && [string isEqualToString:@"."]) {
                return NO;
            }
            
        }
        
        NSCharacterSet* validationSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890qwertyuiopasdfghjklzxcvbnm.@"] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet: validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
    }
    
    if (textField.tag == LDSTextFieldPhoneNumber) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet: validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSLog(@"new string = %@", newString);
        
        NSArray* validComponents = [newString componentsSeparatedByCharactersInSet:validationSet];
        newString = [validComponents componentsJoinedByString:@""];
        
        static const int localNumberMaxLength = 7;
        static const int areaCodeMaxLength = 3;
        static const int countryCodeMaxLength = 3;
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength) {
            return NO;
        }
        
        NSMutableString* resultString = [NSMutableString string];
        
        NSInteger localNumberLength = MIN([newString length], localNumberMaxLength);
        
        if (localNumberLength > 0) {
            
            NSString* number = [newString substringFromIndex:(int)[newString length] - localNumberLength];
            
            [resultString appendString:number];
            
            if ([resultString length] > 3) {
                [resultString insertString:@"-" atIndex:3];
            }
        }
        
        if ([newString length] > localNumberMaxLength) {
            
            NSInteger areaCodeLength = MIN((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);
            
            NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
            
            NSString* area = [newString substringWithRange:areaRange];
            
            area = [NSString stringWithFormat:@"(%@) ", area];
            
            [resultString insertString:area atIndex:0];
            
        }
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength) {
            
            NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
            
            NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
            
            NSString* countryCode = [newString substringWithRange:countryCodeRange];
            
            countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
            
            [resultString insertString:countryCode atIndex:0];
            
        }
        
        textField.text = resultString;
        
        return NO;
        
    }
    
    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    switch (textField.tag) {
 
        case LDSTextFieldFirstName:
            self.firstNameFieldsLabel.text = @"First Name Label";
            break;
            
        case LDSTextFieldLastName:
            self.lastNameFieldsLabel.text = @"Last Name Label";
            break;
            
        case LDSTextFieldLogin:
            self.loginFieldsLabel.text = @"Login Label";
            break;
            
        case LDSTextFieldPassword:
            self.passwordFieldsLabel.text = @"Password Label";
            break;
            
        case LDSTextFieldAge:
            self.ageFieldsLabel.text = @"Age Label";
            break;
            
        case LDSTextFieldPhoneNumber:
            self.phoneNumberFieldsLabel.text = @"Phone Number Label";
            break;
            
        case LDSTextFieldEmail:
            self.emailFieldsLabel.text = @"e-mail Label";
            break;
            
    }
    
    return YES;
}

@end
