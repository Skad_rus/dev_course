//
//  LDSViewController.h
//  TextFieldHW
//
//  Created by Admin on 17.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *ageField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UILabel *firstNameFieldsLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameFieldsLabel;
@property (weak, nonatomic) IBOutlet UILabel *loginFieldsLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordFieldsLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageFieldsLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberFieldsLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailFieldsLabel;

@end
