//
//  LDSSettingsTableViewController.m
//  SettingsHW
//
//  Created by Admin on 18.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSSettingsTableViewController.h"

typedef enum {
    
    LDSTextFieldFirstName,
    LDSTextFieldLastName,
    LDSTextFieldLogin,
    LDSTextFieldPassword,
    LDSTextFieldAge,
    LDSTextFieldPhoneNumber,
    LDSTextFieldEmail
    
}LDSTextfieldType;

@interface LDSSettingsTableViewController () <UITextFieldDelegate>

@end

static NSString* kSettingsFirstName     = @"first name";
static NSString* kSettingsLastName      = @"last name";
static NSString* kSettingsLogin         = @"login";
static NSString* kSettingsPassword      = @"password";
static NSString* kSettingsAge           = @"age";
static NSString* kSettingsPhoneNumber   = @"phone number";
static NSString* kSettingsMail          = @"mail";

static NSString* kSettingsFirstNameLabel     = @"first name label";
static NSString* kSettingsLastNameLabel      = @"last name label";
static NSString* kSettingsLoginLabel         = @"login label";
static NSString* kSettingsPasswordLabel      = @"password label";
static NSString* kSettingsAgeLabel           = @"age label";
static NSString* kSettingsPhoneNumberLabel   = @"phone number label";
static NSString* kSettingsMailLabel          = @"mail label";

@implementation LDSSettingsTableViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self loadSettinds];
	
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Save and Load

- (void) saveSettings {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    //textFields
    [userDefaults setObject:self.firstNameField.text forKey:kSettingsFirstName];
    [userDefaults setObject:self.lastNameField.text forKey:kSettingsLastName];
    [userDefaults setObject:self.loginField.text forKey:kSettingsLogin];
    [userDefaults setObject:self.passwordField.text forKey:kSettingsPassword];
    [userDefaults setObject:self.ageField.text forKey:kSettingsAge];
    [userDefaults setObject:self.phoneNumberField.text forKey:kSettingsPhoneNumber];
    [userDefaults setObject:self.emailField.text forKey:kSettingsMail];
    
    //labels
    [userDefaults setObject:self.firstNameFieldsLabel.text forKey:kSettingsFirstNameLabel];
    [userDefaults setObject:self.lastNameFieldsLabel.text forKey:kSettingsLastNameLabel];
    [userDefaults setObject:self.loginFieldsLabel.text forKey:kSettingsLoginLabel];
    [userDefaults setObject:self.passwordFieldsLabel.text forKey:kSettingsPasswordLabel];
    [userDefaults setObject:self.ageFieldsLabel.text forKey:kSettingsAgeLabel];
    [userDefaults setObject:self.phoneNumberFieldsLabel.text forKey:kSettingsPhoneNumberLabel];
    [userDefaults setObject:self.emailFieldsLabel.text forKey:kSettingsMailLabel];
   
    [userDefaults synchronize];
    
}

- (void) loadSettinds {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    //textFiels
    self.firstNameField.text = [userDefaults objectForKey:kSettingsFirstName];
    self.lastNameField.text = [userDefaults objectForKey:kSettingsLastName];
    self.loginField.text = [userDefaults objectForKey:kSettingsLogin];
    self.passwordField.text = [userDefaults objectForKey:kSettingsPassword];
    self.ageField.text = [userDefaults objectForKey:kSettingsAge];
    self.phoneNumberField.text = [userDefaults objectForKey:kSettingsPhoneNumber];
    self.emailField.text = [userDefaults objectForKey:kSettingsMail];
    
    //labels
    self.firstNameFieldsLabel.text = [userDefaults objectForKey:kSettingsFirstNameLabel];
    self.lastNameFieldsLabel.text = [userDefaults objectForKey:kSettingsLastNameLabel];
    self.loginFieldsLabel.text = [userDefaults objectForKey:kSettingsLoginLabel];
    self.passwordFieldsLabel.text = [userDefaults objectForKey:kSettingsPasswordLabel];
    self.ageFieldsLabel.text = [userDefaults objectForKey:kSettingsAgeLabel];
    self.phoneNumberFieldsLabel.text = [userDefaults objectForKey:kSettingsPhoneNumberLabel];
    self.emailFieldsLabel.text = [userDefaults objectForKey:kSettingsMailLabel];
    
}

#pragma mark - Actions

- (IBAction)actionTextChanged:(UITextField *)sender {
    [self saveSettings];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    switch (textField.tag) {
            
        case LDSTextFieldFirstName:
            [self.lastNameField becomeFirstResponder];
            break;
            
        case LDSTextFieldLastName:
            [self.loginField becomeFirstResponder];
            break;
            
        case LDSTextFieldLogin:
            [self.passwordField becomeFirstResponder];
            break;
            
        case LDSTextFieldPassword:
            [self.ageField becomeFirstResponder];
            break;
            
        case LDSTextFieldAge:
            [self.phoneNumberField becomeFirstResponder];
            break;
            
        case LDSTextFieldPhoneNumber:
            [self.emailField becomeFirstResponder];
            break;
            
        case LDSTextFieldEmail:
            [self.emailField resignFirstResponder];
            break;
            
    }
    
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    switch (textField.tag) {
            
        case LDSTextFieldFirstName:
            self.firstNameFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];
            break;
            
        case LDSTextFieldLastName:
            self.lastNameFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldLogin:
            self.loginFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldPassword:
            self.passwordFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldAge:
            self.ageFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldPhoneNumber:
            self.phoneNumberFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
        case LDSTextFieldEmail:
            self.emailFieldsLabel.text = [NSString stringWithFormat:@"\"%@%@\"", textField.text, string];;
            break;
            
    }
    
    if (textField.tag == LDSTextFieldAge) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet: validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
    }
    
    if (textField.tag == LDSTextFieldEmail) {
        
        if ([textField.text rangeOfString:@"@"].location != NSNotFound && [string isEqualToString:@"@"]) {
            return NO;
        }
        
        if ([textField.text rangeOfString:@"@"].location != NSNotFound) {
            
            NSRange AdRange = NSMakeRange([textField.text rangeOfString:@"@"].location, textField.text.length - [textField.text rangeOfString:@"@"].location);
            
            if ([textField.text rangeOfString:@"." options:NSCaseInsensitiveSearch range:AdRange].location != NSNotFound && [string isEqualToString:@"."]) {
                return NO;
            }
            
        }
        
        NSCharacterSet* validationSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890qwertyuiopasdfghjklzxcvbnm.@"] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet: validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
    }
    
    if (textField.tag == LDSTextFieldPhoneNumber) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet: validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSLog(@"new string = %@", newString);
        
        NSArray* validComponents = [newString componentsSeparatedByCharactersInSet:validationSet];
        newString = [validComponents componentsJoinedByString:@""];
        
        static const int localNumberMaxLength = 7;
        static const int areaCodeMaxLength = 3;
        static const int countryCodeMaxLength = 3;
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength) {
            return NO;
        }
        
        NSMutableString* resultString = [NSMutableString string];
        
        NSInteger localNumberLength = MIN([newString length], localNumberMaxLength);
        
        if (localNumberLength > 0) {
            
            NSString* number = [newString substringFromIndex:(int)[newString length] - localNumberLength];
            
            [resultString appendString:number];
            
            if ([resultString length] > 3) {
                [resultString insertString:@"-" atIndex:3];
            }
        }
        
        if ([newString length] > localNumberMaxLength) {
            
            NSInteger areaCodeLength = MIN((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);
            
            NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
            
            NSString* area = [newString substringWithRange:areaRange];
            
            area = [NSString stringWithFormat:@"(%@) ", area];
            
            [resultString insertString:area atIndex:0];
            
        }
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength) {
            
            NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
            
            NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
            
            NSString* countryCode = [newString substringWithRange:countryCodeRange];
            
            countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
            
            [resultString insertString:countryCode atIndex:0];
            
        }
        
        textField.text = resultString;
        
        return NO;
        
    }
    
    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    switch (textField.tag) {
            
        case LDSTextFieldFirstName:
            self.firstNameFieldsLabel.text = @"First Name Label";
            break;
            
        case LDSTextFieldLastName:
            self.lastNameFieldsLabel.text = @"Last Name Label";
            break;
            
        case LDSTextFieldLogin:
            self.loginFieldsLabel.text = @"Login Label";
            break;
            
        case LDSTextFieldPassword:
            self.passwordFieldsLabel.text = @"Password Label";
            break;
            
        case LDSTextFieldAge:
            self.ageFieldsLabel.text = @"Age Label";
            break;
            
        case LDSTextFieldPhoneNumber:
            self.phoneNumberFieldsLabel.text = @"Phone Number Label";
            break;
            
        case LDSTextFieldEmail:
            self.emailFieldsLabel.text = @"e-mail Label";
            break;
            
    }
    
    return YES;
}

@end
