//
//  LDSViewController.m
//  AnimationsTest
//
//  Created by Admin on 25.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"

@interface LDSViewController ()

@property (weak, nonatomic) UIView* testView;
@property (weak, nonatomic) UIImageView* testImageView;
@property (strong, nonatomic) __block NSMutableArray* intermediateArray;

@end

@implementation LDSViewController

- (void)viewDidLoad
{

/*
 Ученик.
 
 1. Создайте 4 вьюхи у левого края ипада.
 2. Ваша задача всех передвинуть горизонтально по прямой за одно и тоже время
 3. Для каждой вьюхи используйте свою интерполяцию (EasyInOut, EasyIn и т.д.). Это для того, чтобы вы увидели разницу своими собственными глазами :)
 4. Добавте реверсивную анимацию и бесконечные повторения
 5. добавьте смену цвета на рандомный
 
 Студент
 
 5. Добавьте еще четыре квадратные вьюхи по углам - красную, желтую, зеленую и синюю
 6. За одинаковое время и при одинаковой интерполяции двигайте их всех случайно, либо по, либо против часовой стрелки в другой угол.
 7. Когда анимация закончиться повторите все опять: выберите направление и передвиньте всех :)
 8. Вьюха должна принимать в новом углу цвет той вьюхи, что была здесь до него ;)
 
 Мастер
 
 8. Нарисуйте несколько анимационных картинок человечка, который ходит.
 9. Добавьте несколько человечков на эту композицию и заставьте их ходить
*/
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    //[self gurusSolution];
    //[self mySolutionForPupil];
    [self mySolutionForStudent];
    [self gurusSolution];

    
}

- (void) mySolutionForStudent{
    
    CGRect rect = self.view.bounds;
    
    UIImageView* view1 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect), 100, 100)];
    view1.backgroundColor = [UIColor redColor];
    [self.view addSubview:view1];
    
    UIImageView* view2 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rect) - 100, CGRectGetMinY(rect), 100, 100)];
    view2.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:view2];
    
    UIImageView* view3 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(rect), CGRectGetMaxY(rect) - 100, 100, 100)];
    view3.backgroundColor = [UIColor greenColor];
    [self.view addSubview:view3];
    
    UIImageView* view4 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rect) - 100, CGRectGetMaxY(rect) - 100, 100, 100)];
    view4.backgroundColor = [UIColor blueColor];
    [self.view addSubview:view4];
    
    //NSArray* arrayOfViews = [[NSArray alloc] initWithObjects:view1, view2, view4, view3, nil];
    NSMutableArray* arrayOfViews = [[NSMutableArray alloc] initWithObjects:view1, view2, view4, view3, nil];
    
    self.intermediateArray = [[NSMutableArray alloc] initWithObjects:view1, view2, view4, view3, nil];
    
    [self studentMove:arrayOfViews];
    
    //[self moveView:self.testImageView];
    
    
}

- (void) manMove:(UIImageView*) imageView{
    
    ///
    
}

- (void) studentMove: (NSMutableArray*)arrayOfViews{
    
    CGRect rect = self.view.bounds;
    
    BOOL direction = arc4random() % 2;
    
    __block NSMutableArray* intermediateArray = [[NSMutableArray alloc] initWithArray:arrayOfViews];
    
    CGPoint point1 = CGPointMake(CGRectGetMinX(rect) + 50, CGRectGetMinY(rect) + 50);
    CGPoint point2 = CGPointMake(CGRectGetMaxX(rect) - 50, CGRectGetMinY(rect) + 50);
    CGPoint point4 = CGPointMake(CGRectGetMinX(rect) + 50, CGRectGetMaxY(rect) - 50);
    CGPoint point3 = CGPointMake(CGRectGetMaxX(rect) - 50, CGRectGetMaxY(rect) - 50);
    
    NSArray* arrayOfColors = [[NSArray alloc] initWithObjects: [UIColor redColor], [UIColor greenColor], [UIColor yellowColor], [UIColor blueColor],  nil];

    [UIView animateWithDuration:3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         
                         for (UIView* view in arrayOfViews) {
                             
                             NSInteger i = [arrayOfViews indexOfObject:view];
                             
                             view.transform = CGAffineTransformMakeRotation(M_PI_2);
                             
                             if (direction == YES) {
                                 i++;
                                 if (i == 4) {
                                     i = 0;
                                 }
                             } else {
                                 i--;
                                 if (i == - 1) {
                                     i = 3;
                                 }
                             }
                             
                             switch (i) {
                                 case 0:
                                     view.center = point1;
                                     break;
                                 case 1:
                                     view.center = point2;
                                     
                                     break;
                                 case 2:
                                     view.center = point3;
                                     
                                     break;
                                 case 3:
                                     view.center = point4;
                                     
                                     break;
                                     
                                 default:
                                     break;
                             }
                             
                             view.backgroundColor = [arrayOfColors objectAtIndex:i];
                             
                             [intermediateArray replaceObjectAtIndex:i withObject:view];

                         }
                         
                     }
                     completion:^(BOOL finished) {
                         
                         __weak NSMutableArray* v = intermediateArray;
                         [self studentMove:v];
                         
                     }];
    
}

- (void) mySolutionForPupil{
    
    CGRect rect = self.view.bounds;
    
    UIImageView* view1 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(rect) - 100, 0, 100, 100)];
    view1.backgroundColor = [self randomColor];
    [self.view addSubview:view1];
    
    [self myMoveForView:view1 withOption:UIViewAnimationOptionCurveEaseInOut toYCoord:CGRectGetMinY(view1.frame)];
    
    UIImageView* view2 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(rect) - 100, 150, 100, 100)];
    view2.backgroundColor = [self randomColor];
    [self.view addSubview:view2];
    
    [self myMoveForView:view2 withOption:UIViewAnimationOptionCurveEaseIn toYCoord:CGRectGetMinY(view2.frame)];
    
    UIImageView* view3 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(rect) - 100, 300, 100, 100)];
    view3.backgroundColor = [self randomColor];
    [self.view addSubview:view3];
    
    [self myMoveForView:view3 withOption:UIViewAnimationOptionCurveEaseOut toYCoord:CGRectGetMinY(view3.frame)];
    
    UIImageView* view4 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(rect) - 100, 450, 100, 100)];
    view4.backgroundColor = [self randomColor];
    [self.view addSubview:view4];
    
    [self myMoveForView:view4 withOption:UIViewAnimationOptionCurveLinear toYCoord:CGRectGetMinY(view4.frame)];
    

    

    
}

- (void) myMoveForView:(UIView*)view withOption:(UIViewAnimationOptions) myOption toYCoord:(NSUInteger) y{
    
    [UIView animateWithDuration:5
                          delay:0
                        options:myOption | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         
                         view.backgroundColor = [self randomColor];
                         view.frame = CGRectMake(0, y, 100, 100);
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
}

- (void) gurusSolution{
    
    
    UIImageView* view = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    view.backgroundColor = [UIColor clearColor];
    
    UIImage* image1 = [UIImage imageNamed:@"1.png"];
    UIImage* image2 = [UIImage imageNamed:@"2.png"];
    UIImage* image3 = [UIImage imageNamed:@"3.png"];
    UIImage* image4 = [UIImage imageNamed:@"4.png"];
    UIImage* image5 = [UIImage imageNamed:@"5.png"];
    UIImage* image6 = [UIImage imageNamed:@"6.png"];
    UIImage* image7 = [UIImage imageNamed:@"7.png"];
    UIImage* image8 = [UIImage imageNamed:@"8.png"];
    UIImage* image9 = [UIImage imageNamed:@"9.png"];
    
    NSArray* arrOfImages = [NSArray arrayWithObjects:image1, image2, image3, image4, image5, image6, image7, image8, image9, nil];
    
    view.animationImages = arrOfImages;
    view.animationDuration = 3.f;
    [view startAnimating];
    
    [self.view addSubview:view];
    
    self.testImageView = view;
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self moveView:self.testImageView];
    

}

- (void) moveView:(UIView*) view {
    
    
    CGRect rect = self.view.bounds;
    
    rect = CGRectInset(rect, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
    
    CGFloat x = arc4random() % (int)CGRectGetWidth(rect) + CGRectGetMinX(rect);
    CGFloat y = arc4random() % (int)CGRectGetHeight(rect) + CGRectGetMinY(rect);
    
    CGFloat s = (float)(arc4random() % 151) / 100.f + 0.5f;
    
    CGFloat r = (float)(arc4random() % (int)(M_PI * 2*10000)) / 10000 - M_PI;
    
    CGFloat d = (float)(arc4random() % 20001) / 10000 + 1;
     
    
    [UIView animateWithDuration:d
                          delay:0
                        options:UIViewAnimationOptionCurveLinear /*| UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse*/
                     animations:^{
                         view.center = CGPointMake(x, y);
                         view.backgroundColor = [self randomColor];
                         
                         CGAffineTransform scale = CGAffineTransformMakeScale(s, s);
                         CGAffineTransform rotation = CGAffineTransformMakeRotation(r);
                         
                         CGAffineTransform transform = CGAffineTransformConcat(scale, rotation);
                         
                         view.transform = transform;
                     }
                     completion:^(BOOL finished) {
                         
                         __weak UIView* v = view;
                         [self moveView:v];
                     }];
    
    
}

- (UIColor*) randomColor {
    CGFloat r = (CGFloat)(arc4random() % 256) / 255.f;
    CGFloat g = (CGFloat)(arc4random() % 256) / 255.f;
    CGFloat b = (CGFloat)(arc4random() % 256) / 255.f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
