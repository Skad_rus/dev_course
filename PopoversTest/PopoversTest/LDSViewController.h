//
//  LDSViewController.h
//  PopoversTest
//
//  Created by Admin on 01.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController

- (IBAction) actionAdd:(UIBarButtonItem*)sender;
- (IBAction) actionPressMe:(UIButton*)sender;

@end
