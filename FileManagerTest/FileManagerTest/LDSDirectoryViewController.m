//
//  LDSDirectoryViewController.m
//  FileManagerTest
//
//  Created by Admin on 27.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDirectoryViewController.h"
#import "LDSFileCell.h"
#import "UIView+UITableViewCell.h"

typedef enum {
    TTCreateFolder,
    TTBackToRoot
} TTActionSheetSection;

@interface LDSDirectoryViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) NSArray* contents;
@property (strong, nonatomic) NSString* selectedPath;
@property (strong, nonatomic) NSString* alertText;
@property (strong, nonatomic) UITableView* tableView;
@property (strong, nonatomic) NSIndexPath* indexPath;
@property (strong, nonatomic) NSMutableArray* arrOfFolders;
@property (strong, nonatomic) NSMutableArray* arrOfFiles;

@end

@implementation LDSDirectoryViewController

- (id) initWithFolderPath:(NSString*) path;
{
    
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
        self.path = path;
        
    }
    return self;
    
}

- (void) setPath:(NSString *)path{
    
    _path = path;
    
    NSError* error = nil;
    
    self.contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.path error:&error];
    
    /*
    NSMutableArray* tempMutArray = [[NSMutableArray alloc] initWithArray:self.contents];
    [tempMutArray insertObject:@"Tap to add new folder" atIndex:0];
    NSArray* tempArray = [[NSArray alloc] initWithArray:tempMutArray];
    self.contents = tempArray;
    */
    //
    
    
    NSArray *sortedArray = [self.contents sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        if ([self isFile:obj1] && [self isFile:obj2]) {
            return [obj1 compare:obj2];
        } else if ([self isFile:obj1] && ![self isFile:obj2]) {
            return NSOrderedDescending;
        } else {
            return ![obj1 compare:obj2];
        }
    }];
    
    self.contents = [NSMutableArray arrayWithArray:sortedArray];
    
    NSMutableArray* tempMutArray = [[NSMutableArray alloc] initWithArray:self.contents];
    [tempMutArray insertObject:@"Tap to add new folder" atIndex:0];
    NSArray* tempArray = [[NSArray alloc] initWithArray:tempMutArray];
    self.contents = tempArray;
    //
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    [self.tableView reloadData];
    
    self.navigationItem.title = [self.path lastPathComponent];
    
}

- (void) dealloc{
    NSLog(@"controller with path %@ had been deallocated", self.path);
}

//В случае, если путь не установлен, устанавливает его в папку Downloads
- (void)viewDidLoad
{
    
    [super viewDidLoad];

    self.arrOfFiles = [NSMutableArray array];
    self.arrOfFolders = [NSMutableArray array];
    
    if (!self.path) {
        
        self.path = @"/Users/admin/Downloads/temp";
        
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    //
    //[self sortArrays];
    //
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(actionEdit:)];
    
    self.navigationItem.rightBarButtonItem = item;
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    NSLog(@"path = %@", self.path);
    NSLog(@"view controllers = %d", [self.navigationController.viewControllers count]);
    NSLog(@"index on stack = %d", [self.navigationController.viewControllers indexOfObject:self]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

-(void)createDirectory:(NSString *)directoryName atFilePath:(NSString *)filePath
{
    NSString *filePathAndDirectory = [filePath stringByAppendingPathComponent:directoryName];
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }
}

//Метод обратобки нажатия на Edit
- (void) actionEdit:(UIBarButtonItem*) sender {
    
    BOOL isEditing = self.tableView.editing;
    
    [self.tableView setEditing:!isEditing animated:YES];
    
    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
    
    if (self.tableView.editing) {
        item = UIBarButtonSystemItemDone;
    }
    
    //Эти две сторки меняют правый бар баттон на Edit (в любом случае)
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                target:self
                                                                                action:@selector(actionEdit:)];
    
    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
    
}

//Метод возвращающий на корневой контроллер (не использован)
- (void) actionBackToRoot:(UIBarButtonItem*) sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

//Метод для адекватного отображения размера файла / папки
- (NSString*)fileSizeFromValue:(unsigned long long) size {
    
    static NSString* units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
    static int unitCounts = 5;
    
    int index = 0;
    
    double fileSize = (double)size;
    
    while (fileSize > 1024 && index < unitCounts) {
        fileSize /= 1024;
        index++;
    }
    
    return [NSString stringWithFormat:@"%.2f %@", fileSize, units[index]];
    
}

- (IBAction)actionInfoCell:(UIButton*)sender{
    
    NSLog(@"actionInfoCell");
    
    UITableViewCell* cell = [sender superCell];
    
    if (cell) {
        
        NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
        
        [[[UIAlertView alloc] initWithTitle:@"Yahoo!"
                                    message:[NSString stringWithFormat:@"action %d %d", indexPath.section, indexPath.row]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        
    }
    
}


#pragma mark - Methods

- (BOOL) isFile:(id)obj {
    
    BOOL isFile = NO;
    
    NSString *path = [self.path stringByAppendingPathComponent:obj];
    
    [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isFile];
    
    return !isFile;
}

- (void) sortArrays {
    /*
    NSArray *sortedArrayOfFolders;
    sortedArrayOfFolders = [self.arrOfFolders sortedArrayUsingComparator:^NSComparisonResult(UITableViewCell* a, UITableViewCell* b) {
        NSString *first = a.textLabel.text;
        NSString *second = b.textLabel.text;
        return [first compare:second];
    }];
    
    NSArray *sortedArrayOfFiles;
    sortedArrayOfFiles = [self.arrOfFiles sortedArrayUsingComparator:^NSComparisonResult(LDSFileCell* a, LDSFileCell* b) {
        NSString *first = a.nameLabel.text;
        NSString *second = b.nameLabel.text;
        return [first compare:second];
    }];
    
    NSMutableArray* tempArray = [NSMutableArray array];
    [tempArray addObjectsFromArray:sortedArrayOfFolders];
    [tempArray addObjectsFromArray:sortedArrayOfFiles];
    
    NSArray* returnArray = [[NSArray alloc] initWithArray:tempArray];
    */
    
    NSArray *sortedArrayOfFolders;
    sortedArrayOfFolders = [self.arrOfFolders sortedArrayUsingComparator:^NSComparisonResult(NSString* a, NSString* b) {
        NSString *first = a;
        NSString *second = b;
        return [first compare:second];
    }];
    
    NSArray *sortedArrayOfFiles;
    sortedArrayOfFiles = [self.arrOfFiles sortedArrayUsingComparator:^NSComparisonResult(NSString* a, NSString* b) {
        NSString *first = a;
        NSString *second = b;
        return [first compare:second];
    }];
    
    NSMutableArray* tempArray = [NSMutableArray array];
    [tempArray addObjectsFromArray:sortedArrayOfFolders];
    [tempArray addObjectsFromArray:sortedArrayOfFiles];
    
    NSArray* returnArray = [[NSArray alloc] initWithArray:tempArray];
    
    self.contents = returnArray;
    
    [self.tableView reloadData];
    
}

//Проверяет является ли папкой выбранный объект
- (BOOL) isDirectoryAtIndexPath: (NSIndexPath*) indexPath {
    
    NSString* fileName = [self.contents objectAtIndex:indexPath.row];
    
    NSString* filePath = [self.path stringByAppendingPathComponent:fileName];
    
    BOOL isDirectory = NO;
    
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    return isDirectory;
    
}

//Метод, создающиц ячейку с вееденным именем
- (void) createFolderWithName:(NSString *)name {
    
    NSString* folderName = name;
    
    [self createDirectory:folderName atFilePath:self.path];
    
    NSMutableArray* tempMutArray = [[NSMutableArray alloc] initWithArray:self.contents];
    [tempMutArray insertObject:folderName atIndex:self.indexPath.row + 1];
    NSArray* tempArray = [[NSArray alloc] initWithArray:tempMutArray];
    self.contents = tempArray;
    
    /*
    NSArray *sortedArray = [self.contents sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        if ([self isFile:obj1] && [self isFile:obj2]) {
            return [obj1 compare:obj2];
        } else if ([self isFile:obj1] && ![self isFile:obj2]) {
            return NSOrderedDescending;
        } else {
            return ![obj1 compare:obj2];
        }
    }];
    
    self.contents = [NSMutableArray arrayWithArray:sortedArray];
    */
    
    NSIndexPath *tempPath = [[NSIndexPath alloc]init];
    tempPath = [NSIndexPath indexPathForItem:(self.indexPath.row + 1) inSection:self.indexPath.section];
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[tempPath] withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];

}


#pragma mark - UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return indexPath.row == 0 ? UITableViewCellEditingStyleNone : UITableViewCellEditingStyleDelete;
    
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return NO;
    
}

//Запрещаем отступ для первой ячейки в режиме редактирования
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
    
    if (proposedDestinationIndexPath.row == 0) {
        return sourceIndexPath;
    } else {
        return proposedDestinationIndexPath;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.contents count];
    
}

//Метод, описывающий ячейки
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* fileIdentifier = @"FileCell";
    static NSString* folderIdentifier = @"FolderCell";
    
    NSString* fileName = [self.contents objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {
        
        static NSString* addStudentIdentifier = @"addStudentCell";
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:addStudentIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addStudentIdentifier];
            cell.textLabel.textColor = [UIColor blueColor];
            cell.textLabel.text = fileName;
        }
       return cell;
    }
    
    if ([self isDirectoryAtIndexPath:indexPath]) {
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:folderIdentifier];
        cell.textLabel.text = fileName;
        
        //[self.arrOfFolders addObject:cell];
        [self.arrOfFolders addObject:cell.textLabel.text];
        
        
        return cell;
        
    } else {
        
        NSString* path = [self.path stringByAppendingPathComponent:fileName];
        
        NSDictionary* attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
        
        LDSFileCell* cell = [tableView dequeueReusableCellWithIdentifier:fileIdentifier];
        
        cell.nameLabel.text = fileName;
        cell.sizeLabel.text = [self fileSizeFromValue:[attributes fileSize]];
        
        static NSDateFormatter* dateFormatter = nil;
        
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        }
        
        cell.dateLabel.text = [dateFormatter stringFromDate:[attributes fileModificationDate]];
        
        //[self.arrOfFiles addObject:cell];
        [self.arrOfFiles addObject:cell.nameLabel.text];
        
        return cell;
        
    }
    
   return nil;
    
}

#pragma mark - UITableViewDelegate

//Подтверждение удаления, и, собственно, само удаление.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        NSError* error = nil;
        
        NSString* fileName = [self.contents objectAtIndex:indexPath.row];
        NSString* path = [self.path stringByAppendingPathComponent:fileName];
        NSLog(@"Path = %@, object path = %@", self.path, path);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:path error:&error];
        
        NSMutableArray* tempMutArray = [[NSMutableArray alloc] initWithArray:self.contents];
        [tempMutArray removeObjectAtIndex:indexPath.row];
        NSArray* tempArray = [[NSArray alloc] initWithArray:tempMutArray];
        self.contents = tempArray;
        
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        [tableView endUpdates];
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return 44.f;
    }
    
    if ([self isDirectoryAtIndexPath:indexPath]) {
        return 44.f;
    } else {
        return 80.f;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        
        self.tableView = tableView;
        self.indexPath = indexPath;
        
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"set folder name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alertView textFieldAtIndex:0].keyboardAppearance = UIKeyboardAppearanceDark;
        
        [alertView show];
        
    }
    
    if ([self isDirectoryAtIndexPath:indexPath]) {
        
        NSString* fileName = [self.contents objectAtIndex:indexPath.row];
        
        NSString* path = [self.path stringByAppendingPathComponent:fileName];
        
        self.selectedPath = path;
        
        [self performSegueWithIdentifier:@"navigateDeep" sender:nil];
    }
    
}

#pragma mark - Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{

    NSLog(@"shouldPerformSegueWithIdentifier: %@", identifier);
    
    return YES;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    LDSDirectoryViewController* vc = segue.destinationViewController;
    vc.path = self.selectedPath;
    
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        UITextField *textfild = [alertView textFieldAtIndex:0];
        
        [self createFolderWithName:textfild.text];
        
    }
}

@end
