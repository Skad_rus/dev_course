//
//  LDSPatient.m
//  DelegatesTest
//
//  Created by Admin on 30.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSPatient.h"

@implementation LDSPatient



- (BOOL) howAreYou{
    
    BOOL iFeelGood = arc4random() %2;
    
    if (!iFeelGood) {
        NSLog(@"%@: I feel bad!", self.name);
        [self.delegate patientFeelsBad:self];
    }
    else{
        NSLog(@"%@: I'm ok!", self.name);
    }
    
    return iFeelGood;
}

- (void) becomeWorse{
    
    self.temperature = (350 + arc4random() % 81) / 10;
  //  self.temperature = [self temperature ] / 10;
 
    
    switch (arc4random() %4){
        case 3:
            self.headAche = 1;
            break;
        case 2:
            self.toothAche = 1;
            break;
        case 1:
            self.stomAchache = 1;
            break;
        case 0:
            self.heartAches = 1;
            break;
    }
}

- (void) сrossYourself: (NSInteger) times{
    NSLog(@"Cross yourself %d times!", times);
}

- (void) readPrayer: (NSInteger) times{
    NSLog(@"Read a prayer %d times!", times);
}

- (void) listenFor: (NSString*) groupName{
   NSLog(@"Just listen to %@", groupName);
}

- (void) drink: (NSString*) alcoholName{
   NSLog(@"Just drink some %@", alcoholName);
}

- (void) takePillFromHeadache{
    
    NSLog(@"%@ takes a pill from headache", self.name);
}

- (void) takePillFromToothache{
    
    NSLog(@"%@ takes a pill from toothache", self.name);
}

- (void) makeShotFromHeadache{
    
    NSLog(@"%@ makes a shot from headache", self.name);
}

- (void) makeShotFromToothache{
    
    NSLog(@"%@ makes a shot from toothache", self.name);
}

- (void) takePillFromStomAchache{
    
    NSLog(@"%@ takes a pill from stomachache", self.name);
}

- (void) takePillFromHeartAches{
    
    NSLog(@"%@ takes a pill from stomachache", self.name);
}

- (void) makeShotFromStomAchache{
    
    NSLog(@"%@ makes a shot from heart aches", self.name);
}

- (void) makeShotFromHeartAches{
    
    NSLog(@"%@ makes a shot from heart aches", self.name);
}

@end
