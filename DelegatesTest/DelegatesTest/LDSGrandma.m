//
//  LDSGrandma.m
//  DelegatesTest
//
//  Created by Admin on 01.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSGrandma.h"

@implementation LDSGrandma

#pragma mark - LDSPatientDelegate

- (void) patientFeelsBad: (LDSPatient*) patient{
    NSLog(@"Patient %@ feels bad", patient.name);
    NSLog(@"Patients temperature is %f", patient.temperature);
    
    NSInteger times = 0;
    
    if (patient.temperature >= 37.f && patient.temperature <= 39.f) {
        
        if ([patient headAche] == 1) {
            times = 2;
            [patient readPrayer: times];
        }
        if ([patient toothAche] == 1) {
            times = 3;
            [patient readPrayer: times];
        }
        if ([patient stomAchache] == 1) {
            times = 4;
            [patient readPrayer: times];
        }
        if ([patient heartAches] == 1) {
            times = 5;
            [patient readPrayer: times];
        }
        
    }
    else if (patient.temperature > 39.f) {
        
        if ([patient headAche] == 1) {
            times = 2;
            [patient сrossYourself: times];
        }
        if ([patient toothAche] == 1) {
            times = 3;
            [patient сrossYourself: times];
        }
        if ([patient stomAchache] == 1) {
            times = 4;
            [patient сrossYourself: times];
        }
        if ([patient heartAches] == 1) {
            times = 5;
            [patient сrossYourself: times];
        }
        
    }
    else {
        NSLog(@"Patient %@ should rest", patient.name);
    }
    
    //patient.docsMark = arc4random() %11;
    
}

@end
