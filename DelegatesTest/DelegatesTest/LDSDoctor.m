//
//  LDSDoctor.m
//  DelegatesTest
//
//  Created by Admin on 30.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDoctor.h"

@implementation LDSDoctor


- (void) docsReport{
    
    NSLog(@"Doctors report:");
    if (self.report == nil) {
        NSLog(@"Nothing to report");
    }
    else{
    NSLog(@"%@", self.report);
    }
}

#pragma mark - LDSPatientDelegate

- (void) patientFeelsBad: (LDSPatient*) patient{
    NSLog(@"Patient %@ feels bad", patient.name);
    NSLog(@"Patients temperature is %f", patient.temperature);
    
    self.report = @"";
    
    if ([patient headAche] == 1) {
        self.report = [NSString stringWithFormat:
        @"%@ %@ has a headache ",self.report, patient.name];
    }
    if ([patient toothAche] == 1) {
        self.report = [NSString stringWithFormat:
        @"%@ %@ has a toothache ",[self report], patient.name];
    }
    if ([patient stomAchache] == 1) {
        self.report = [NSString stringWithFormat:
        @"%@ %@ has a stomachache ",[self report], patient.name];
    }
    if ([patient heartAches] == 1) {
        self.report = [NSString stringWithFormat:
        @"%@ %@ has a heart aches ",[self report], patient.name];
    }
    
    
    if (patient.temperature >= 37.f && patient.temperature <= 39.f) {
        
        if ([patient headAche] == 1) {
            [patient takePillFromHeadache];
        }
        if ([patient toothAche] == 1) {
            [patient takePillFromToothache];
        }
        if ([patient stomAchache] == 1) {
            [patient takePillFromStomAchache];
        }
        if ([patient heartAches] == 1) {
            [patient takePillFromHeartAches];
        }
        
    }
    else if (patient.temperature > 39.f) {
        
        if ([patient headAche] == 1) {
            [patient makeShotFromHeadache];
        }
        if ([patient toothAche] == 1) {
            [patient makeShotFromToothache];
        }
        if ([patient stomAchache] == 1) {
            [patient makeShotFromStomAchache];
        }
        if ([patient heartAches] == 1) {
            [patient makeShotFromHeartAches];
        }
        
    }
    else {
        NSLog(@"Patient %@ should rest", patient.name);
    }
    
    patient.docsMark = arc4random() %11;
}
/*
 - (void) сrossYourself: (NSInteger*) times{
 NSLog(@"Cross yourself %ld times!",(long)times);
 }
 
 - (void) readPrayer: (NSInteger*) times{
 NSLog(@"Read a prayer %ld times!",(long)times);
 }
*/


/*- (void) patient:(LDSPatient*) patient hasQuestion : (NSString*) question{
    NSLog(@"Patient %@ has a question: %@", patient.name, question);
}*/

@end
