//
//  LDSDoctor.h
//  DelegatesTest
//
//  Created by Admin on 30.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSPatient.h"

@interface LDSDoctor : NSObject <LDSPatientDelegate>

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* report;

- (void) docsReport;

@end
