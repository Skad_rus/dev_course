//
//  LDSFriend.h
//  DelegatesTest
//
//  Created by Admin on 01.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSPatient.h"

@interface LDSFriend : NSObject <LDSPatientDelegate>

@property (strong, nonatomic) NSString* name;

@end
