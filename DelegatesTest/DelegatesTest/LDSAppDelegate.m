//
//  LDSAppDelegate.m
//  DelegatesTest
//
//  Created by Admin on 30.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSPatient.h"
#import "LDSDoctor.h"
#import "LDSGrandma.h"
#import "LDSFriend.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    LDSPatient* patient1 = [[LDSPatient alloc] init];
    patient1.name = @"Vova";
    
    LDSPatient* patient2 = [[LDSPatient alloc] init];
    patient2.name = @"Petya";
    
    LDSPatient* patient3 = [[LDSPatient alloc] init];
    patient3.name = @"Dima";
    
    LDSPatient* patient4 = [[LDSPatient alloc] init];
    patient4.name = @"Sasha";
    
    LDSPatient* patient5 = [[LDSPatient alloc] init];
    patient5.name = @"Lesha";
    
    LDSDoctor* doctor = [[LDSDoctor alloc] init];
    LDSGrandma* grandma = [[LDSGrandma alloc] init];
    LDSFriend* friend = [[LDSFriend alloc] init];
    
    doctor.name = @"Doctor";
    grandma.name = @"Grandmother";
    friend.name = @"Friend";
    

    BOOL randomDoctor = arc4random() %3;
    
    NSArray* arrayOfPatients = [NSArray arrayWithObjects:patient1, patient2, patient3, patient4, patient5, nil];
    
    for (LDSPatient *patient in arrayOfPatients) {

        [patient becomeWorse];
        
        switch (randomDoctor) {
                
            case 2:
                patient.delegate = friend;
                NSLog(@"%@: My doctor is %@!",patient.name, friend.name);
                break;
                
            case 1:
                patient.delegate = doctor;
                NSLog(@"%@: My doctor is %@!",patient.name, doctor.name);
                break;
                
            case 0:
                patient.delegate = grandma;
                NSLog(@"%@: My doctor is %@!",patient.name, grandma.name);
                break;
        }
        
        NSLog(@"%@ %@!", patient.name, [patient howAreYou] ? @"not need treatment" : @"was provided treatment" );
        NSLog(@" ");
        
    }
    
    [doctor docsReport];
    
    NSLog(@" ");
    
    for (LDSPatient *patient in arrayOfPatients) {
        
        if (patient.docsMark <= 5) {
            switch (arc4random() %2) {
                case 1:
                    patient.delegate = friend;
                    NSLog(@"%@: I changed doctor. Now %@ is my doctor!",patient.name, friend.name);
                    break;
                case 0:
                    patient.delegate = grandma;
                    NSLog(@"%@: I changed doctor. Now %@ is my doctor!",patient.name, grandma.name);
                    break;
               
            }
        }
        else{
            NSLog(@"%@: I am satisfied with everything. %@ is still my doctor!",patient.name, doctor.name);
        }
    }
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
