//
//  LDSPatient.h
//  DelegatesTest
//
//  Created by Admin on 30.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LDSPatientDelegate;


@interface LDSPatient : NSObject

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) CGFloat temperature;
@property (assign, nonatomic) BOOL headAche;
@property (assign, nonatomic) BOOL toothAche;
@property (assign, nonatomic) BOOL stomAchache;
@property (assign, nonatomic) BOOL heartAches;
@property (weak, nonatomic) id <LDSPatientDelegate> delegate;
@property (assign, nonatomic) NSInteger docsMark;

- (void) becomeWorse;
- (void) makeShotFromToothache;
- (void) makeShotFromHeadache;
- (void) takePillFromToothache;
- (void) takePillFromHeadache;
- (void) makeShotFromHeartAches;
- (void) makeShotFromStomAchache;
- (void) takePillFromHeartAches;
- (void) takePillFromStomAchache;
- (void) readPrayer: (NSInteger) times;
- (void) сrossYourself: (NSInteger) times;
- (BOOL) howAreYou;
- (void) listenFor: (NSString*) groupName;
- (void) drink: (NSString*) alcoholName;

@end



@protocol LDSPatientDelegate <NSObject>

typedef enum{
    
    LDSHead,
    LDSHearth,
    LDSTooth,
    LDSStomach
    
}LDSOrgan;

- (void) patientFeelsBad: (LDSPatient*) patient;


@end
