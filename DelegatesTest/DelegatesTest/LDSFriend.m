//
//  LDSFriend.m
//  DelegatesTest
//
//  Created by Admin on 01.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSFriend.h"

@implementation LDSFriend

#pragma mark - LDSPatientDelegate

- (void) patientFeelsBad: (LDSPatient*) patient{
    NSLog(@"Patient %@ feels bad", patient.name);
    NSLog(@"Patients temperature is %f", patient.temperature);
    
    
    if (patient.temperature >= 37.f && patient.temperature <= 39.f) {
        
        if ([patient headAche] == 1) {
            [patient drink: @"Heneken"];
        }
        if ([patient toothAche] == 1) {
            [patient drink: @"Krinica"];
        }
        if ([patient stomAchache] == 1) {
            [patient drink: @"Green Beer"];
        }
        if ([patient heartAches] == 1) {
            [patient drink: @"Alivaria"];
        }
        
    }
    else if (patient.temperature > 39.f) {
        
        if ([patient headAche] == 1) {
            [patient listenFor: @"AC/DC"];
        }
        if ([patient toothAche] == 1) {
            [patient listenFor: @"Metallica"];
        }
        if ([patient stomAchache] == 1) {
            [patient listenFor: @"Lumen"];
        }
        if ([patient heartAches] == 1) {
            [patient listenFor: @"Linkin park"];
        }
        
    }
    else {
        NSLog(@"Patient %@ should rest", patient.name);
    }
    
    //patient.docsMark = arc4random() %11;
    
}

@end
