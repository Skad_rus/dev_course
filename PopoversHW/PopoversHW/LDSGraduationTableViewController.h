//
//  LDSGraduationTableViewController.h
//  PopoversHW
//
//  Created by Admin on 02.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSGraduationTableViewController : UITableViewController

- (void) problematicTextField:(UITextField*) textField;

@property (strong, nonatomic) UITextField* textField;

@end
