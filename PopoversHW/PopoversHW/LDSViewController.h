//
//  LDSViewController.h
//  PopoversHW
//
//  Created by Admin on 01.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (void) problematicTextField:(UITextField*) textField;

@property (strong, nonatomic) UITextField* textField;

- (IBAction)valueChangedAction:(UIDatePicker *)sender;

@end
