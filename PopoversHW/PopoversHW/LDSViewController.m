//
//  LDSViewController.m
//  PopoversHW
//
//  Created by Admin on 01.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"
#import "LDSTableViewController.h"

@interface LDSViewController ()

@property (weak, nonatomic) NSDate* chosenDate;

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    if (![self.textField.text isEqualToString:@""]) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd yyyy"];
        
        self.datePicker.date = [formatter dateFromString:self.textField.text];
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (void) problematicTextField:(UITextField*) textField{
    
    self.textField = textField;
    
}

- (IBAction)valueChangedAction:(UIDatePicker *)sender {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd yyyy"];
    
    NSString* chosenDate = [formatter stringFromDate:sender.date];
    
    NSLog(@"%@", chosenDate);
    
    self.textField.text = [NSString stringWithFormat:@"%@", chosenDate];

    self.chosenDate = sender.date;
    
}

@end
