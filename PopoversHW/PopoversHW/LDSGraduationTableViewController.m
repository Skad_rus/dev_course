//
//  LDSGraduationTableViewController.m
//  PopoversHW
//
//  Created by Admin on 02.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSGraduationTableViewController.h"

@interface LDSGraduationTableViewController ()

@property (weak, nonatomic) NSArray* graduationArray;
@property (strong, nonatomic) NSMutableArray* routes;

@end

@implementation LDSGraduationTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.routes = [NSMutableArray array];
    NSArray* array = [NSArray arrayWithObjects:@"неполное среднее", @"среднее", @"неполное высшее", @"высшее", nil];
    self.graduationArray = array;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) problematicTextField:(UITextField*) textField{
    
    self.textField = textField;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.graduationArray count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

    }
    
    [self.routes addObject:cell];
    
    cell.textLabel.text = [self.graduationArray objectAtIndex:indexPath.row];
    
    if ([cell.textLabel.text isEqualToString:self.textField.text]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    //
    
    //
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    for (UITableViewCell* cell in self.routes) {
        NSLog(@"%d", [self.routes count]);

        cell.accessoryType = UITableViewCellAccessoryNone;

    }
    
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    self.textField.text = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    
}

@end
