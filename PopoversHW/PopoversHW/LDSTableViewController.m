//
//  LDSTableViewController.m
//  PopoversHW
//
//  Created by Admin on 01.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSTableViewController.h"
#import "LDSViewController.h"

@interface LDSTableViewController () <UITextFieldDelegate, UIPopoverControllerDelegate>

@property (strong, nonatomic) UIPopoverController* popover;

@end

@implementation LDSTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 5;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 1) {
        
        LDSViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LDSViewController"];
        
        [vc problematicTextField:textField];
        
        UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:vc];
        
        popover.delegate = self;
        
        self.popover = popover;
        
        CGRect textFieldRect = [self.view convertRect:textField.bounds fromView:textField];
        
        [popover presentPopoverFromRect:textFieldRect
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionUp
                               animated:YES];
        
    } else if (textField.tag == 2) {
        
        LDSViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LDSGraduationTableViewController"];
        
        [vc problematicTextField:textField];
        
        UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:vc];
        
        popover.delegate = self;
        
        self.popover = popover;
        
        CGRect textFieldRect = [self.view convertRect:textField.bounds fromView:textField];
        
        [popover presentPopoverFromRect:textFieldRect
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionUp
                               animated:YES];
    }
    
    return NO;
    
}

@end
