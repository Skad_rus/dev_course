//
//  LDSCityCell.m
//  WeatherTest
//
//  Created by Admin on 07.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSCityCell.h"

@implementation LDSCityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        
        NSDate* date = nil;//[NSDate dateWithTimeIntervalSinceNow:i*24*3600];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE"];
        
        date = [NSDate dateWithTimeIntervalSinceNow:1*24*3600];
        self.day1.text = [dateFormatter stringFromDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:2*24*3600];
        self.day2.text = [dateFormatter stringFromDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:3*24*3600];
        self.day3.text = [dateFormatter stringFromDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:4*24*3600];
        self.day4.text = [dateFormatter stringFromDate:date];
        
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        //self.backgroundColor = [UIColor redColor];
        
        NSDate* date = nil;//[NSDate dateWithTimeIntervalSinceNow:i*24*3600];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE"];
        
        date = [NSDate dateWithTimeIntervalSinceNow:1*24*3600];
        self.day1.text = [dateFormatter stringFromDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:2*24*3600];
        self.day2.text = [dateFormatter stringFromDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:3*24*3600];
        self.day3.text = [dateFormatter stringFromDate:date];
        date = [NSDate dateWithTimeIntervalSinceNow:4*24*3600];
        self.day4.text = [dateFormatter stringFromDate:date];
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
