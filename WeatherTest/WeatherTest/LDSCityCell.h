//
//  LDSCityCell.h
//  WeatherTest
//
//  Created by Admin on 07.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSCityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *weatherToday;
@property (weak, nonatomic) IBOutlet UIImageView *imgToday;

@property (weak, nonatomic) IBOutlet UILabel *day1;
@property (weak, nonatomic) IBOutlet UILabel *day2;
@property (weak, nonatomic) IBOutlet UILabel *day3;
@property (weak, nonatomic) IBOutlet UILabel *day4;

@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (weak, nonatomic) IBOutlet UIImageView *img4;

@property (weak, nonatomic) IBOutlet UILabel *weatherDay1;
@property (weak, nonatomic) IBOutlet UILabel *weatherDay2;
@property (weak, nonatomic) IBOutlet UILabel *weatherDay3;
@property (weak, nonatomic) IBOutlet UILabel *weatherDay4;

@property (weak, nonatomic) IBOutlet UILabel *weatherNight1;
@property (weak, nonatomic) IBOutlet UILabel *weatherNight2;
@property (weak, nonatomic) IBOutlet UILabel *weatherNight3;
@property (weak, nonatomic) IBOutlet UILabel *weatherNight4;

@end
