//
//  LDSAppDelegate.m
//  BitwiseOperationsTest
//
//  Created by Admin on 17.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Ученик.
 
 1. Повторите мой код и создайте класс студент с соответствующим набором предметов
 2. В цикле создайте 10 студентов и добавьте их в массив. Используйте мутабл массив
 3. У каждого рандомно установите предметы
 
 Студент
 
 4. В новом цикле пройдитесь по студентам и разделите их уже на два массива - технари и гуманитарии.
 5. Также посчитайте количество тех кто учит программирование
 
 Мастер.
 
 6. Если студенты выбрали биологию, то отмените ее у них и выведите сообщение в лог, предмет отменен
 7. Тут придется разобраться как сбросить бит, включите логику :)
 
 Супермен.
 8. Сгенерируйте случайный интежер в диапазоне от 0 до максимума.
 9. Используя цикл и битовые операыии (и возможно NSMutableString) выведите это число на экран в двоичном виде
*/

#import "LDSAppDelegate.h"
#import "LDSStudent.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    LDSStudent* student1 = [[LDSStudent alloc] init];
    LDSStudent* student2 = [[LDSStudent alloc] init];
    LDSStudent* student3 = [[LDSStudent alloc] init];
    LDSStudent* student4 = [[LDSStudent alloc] init];
    LDSStudent* student5 = [[LDSStudent alloc] init];
    LDSStudent* student6 = [[LDSStudent alloc] init];
    LDSStudent* student7 = [[LDSStudent alloc] init];
    LDSStudent* student8 = [[LDSStudent alloc] init];
    LDSStudent* student9 = [[LDSStudent alloc] init];
    LDSStudent* student10 = [[LDSStudent alloc] init];
    
    /*
    student.studiesAnatomy = YES;
    student.studiesDevelopment = YES;
    student.studiesEngineering = YES;
    student.studiesMath = YES;
    student.studiesPhysics = NO;
    student.studiesArt = NO;
    student.studiesBiology = NO;*/
    
    student1.subjectType = LDSStudentSubjectTypeAnatomy | LDSStudentSubjectTypeDevelopment |
                          LDSStudentSubjectTypeEngineering | LDSStudentSubjectTypeMath;
    
    NSMutableArray* mutArray = [NSMutableArray arrayWithObjects:student1, student2, student3, student4, student5, student6, student7, student8, student9, student10, nil];
    
    NSLog(@"%@", student1);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
