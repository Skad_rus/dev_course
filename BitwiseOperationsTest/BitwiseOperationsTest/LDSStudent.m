//
//  LDSStudent.m
//  BitwiseOperationsTest
//
//  Created by Admin on 17.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSStudent.h"

@implementation LDSStudent

- (NSString*) answerByType:(LDSStudentSubjectType) type{
    return self.subjectType & type ? @"yes" : @"no";
}

- (NSString *)description{
 
    return [NSString stringWithFormat:  @"Student studies:\n"
                                        "biology = %@\n"
                                        "math = %@\n"
                                        "development = %@\n"
                                        "engineering = %@\n"
                                        "art = %@\n"
                                        "physics = %@\n"
                                        "anatomy = %@\n",
                                        [self answerByType:LDSStudentSubjectTypeBiology],
                                        [self answerByType:LDSStudentSubjectTypeMath],
                                        [self answerByType:LDSStudentSubjectTypeDevelopment],
                                        [self answerByType:LDSStudentSubjectTypeEngineering],
                                        [self answerByType:LDSStudentSubjectTypeArt],
                                        [self answerByType:LDSStudentSubjectTypePhysics],
                                        [self answerByType:LDSStudentSubjectTypeAnatomy]
                                        ];
}




@end
