//
//  LDSStudent.h
//  BitwiseOperationsTest
//
//  Created by Admin on 17.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    LDSStudentSubjectTypeBiology        = 1 << 0,
    LDSStudentSubjectTypeMath           = 1 << 1,
    LDSStudentSubjectTypeDevelopment    = 1 << 2,
    LDSStudentSubjectTypeEngineering    = 1 << 3,
    LDSStudentSubjectTypeArt            = 1 << 4,
    LDSStudentSubjectTypePhysics        = 1 << 5,
    LDSStudentSubjectTypeAnatomy        = 1 << 6
    
} LDSStudentSubjectType;

@interface LDSStudent : NSObject

@property (assign, nonatomic) LDSStudentSubjectType subjectType;

@property (assign, nonatomic) BOOL studiesBiology;
@property (assign, nonatomic) BOOL studiesMath;
@property (assign, nonatomic) BOOL studiesDevelopment;
@property (assign, nonatomic) BOOL studiesEngineering;
@property (assign, nonatomic) BOOL studiesArt;
@property (assign, nonatomic) BOOL studiesPhysics;
@property (assign, nonatomic) BOOL studiesAnatomy;



@end
