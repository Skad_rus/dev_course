//
//  LDSViewController.m
//  VievControllersTest
//
//  Created by Admin on 18.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"

@interface LDSViewController ()

@end

@implementation LDSViewController

- (void)loadView{
    
    [super loadView];
    
    NSLog(@"loadView");
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    NSLog(@"viewDidLoad");
    
    self.view.backgroundColor = [UIColor redColor];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSLog(@"iPad");
    } else {
        NSLog(@"iPhone");
    }
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    NSLog(@"viewWillAppear");
    
}
- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    NSLog(@"viewDidAppear");
    
}
- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    NSLog(@"viewWillDisappear");
    
}
- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    NSLog(@"viewDidDisappear");
    
}

- (void)viewWillLayoutSubviews{
    
    [super viewWillLayoutSubviews];
    
    NSLog(@"viewWillLayoutSubviews");

}

- (BOOL)shouldAutorotate{
    
    return YES;
    
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortraitUpsideDown | UIInterfaceOrientationMaskPortrait;

}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    NSLog(@"willRotateToInterfaceOrientation from %d to %d", self.interfaceOrientation, toInterfaceOrientation);

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    NSLog(@"didRotateFromInterfaceOrientation from %d to %d", fromInterfaceOrientation, self.interfaceOrientation);

}


- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    NSLog(@"viewDidLayoutSubviews");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
