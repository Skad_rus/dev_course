//
//  LDSObject.h
//  Blocks Test
//
//  Created by Admin on 12.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ObjectBlock) (void);

@interface LDSObject : NSObject

@property (strong, nonatomic) NSString* name;

- (void) testMethod: (ObjectBlock) block;

@end
