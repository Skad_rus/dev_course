//
//  LDSAppDelegate.m
//  Blocks Test
//
//  Created by Admin on 11.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSObject.h"

typedef void (^OurTestBlock)(void);

typedef NSString* (^OurTestBlock2) (NSInteger);

@interface LDSAppDelegate ()

@property (copy, nonatomic) OurTestBlock testBlock;

@property (strong, nonatomic) NSString* name;

@end

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //[self testMethod];
    
    //NSObject* testObject;
    
    /*
     
    void (^testBlock) (void);
    
    testBlock = ^{
        NSLog(@"Test block");
    };
    
    //testBlock();
    //testBlock();
    //testBlock();
    //testBlock();
    
    void (^testBlockWithParams) (NSString*, NSInteger) = ^(NSString* string, NSInteger intValue){
        NSLog(@"test block with parameters %@ %d", string, intValue);

    };
    
    NSString* (^testBlockWithReturnValueAndParams) (NSString*, NSInteger) = ^(NSString* string, NSInteger intValue){
        return [NSString stringWithFormat:@"test block with return value and parameters %@ %d", string, intValue];
        
    };
    
    
    //NSString* result = testBlockWithReturnValueAndParams(@"Test string", 111);
    //testBlockWithParams(@"Test string", 111);
    //NSLog(@"%@", result);
    
    
    __block NSString* testString = @"How is it possible?";
    
    __block NSInteger i = 0;
    void (^testBlock2) (void);
    
    testBlock2 = ^{
        NSLog(@"test block2");
        //NSLog(@"%@", testString);
        
        //NSLog(@"%d", ++i);
        testString = [NSString stringWithFormat:@"How is it possible? %d", ++i];
        NSLog(@"%@", testString);
    };
    
    testBlock2();
    testBlock2();
    testBlock2();
     */
    
    /*
    void(^bbb)(void) = ^{
        NSLog(@"BLOCK!!!");
    };
    
    [self testBlocksMethod:bbb];
    
    OurTestBlock testBlock2 = ^{
        NSLog(@"BLOCK2!!!");
    };
        
    [self testBlocksMethod:testBlock2];
    
    //array = [array sortedUs
    //NSArray* array = nil;
    
    NSComparisonResult (^bbb) (id, id) = ^(id obj1, id obj2){
        return  NSOrderedAscending;
    };
    
    array = [array sortedArrayUsingComparator:bbb];
  
    
    
    OurTestBlock2 tb = ^(NSInteger intValue) {
        return [NSString stringWithFormat:@"%d", intValue];
    };
    
    NSLog(@"%@", tb(5));
    
    
    LDSObject* obj = [[LDSObject alloc] init];
    obj.name = @"OBJECT";
     
    
    OurTestBlock tb = ^{
        NSLog(@"%@", obj.name);
    };
    
    tb();
    
    __weak LDSObject * weakObj = obj;
    
    self.testBlock = ^{
        NSLog(@"%@", weakObj.name);
    };
    
    self.testBlock();
     */
    /*self.name = @"Hello!";
    
    LDSObject* obj1 = [[LDSObject alloc] init];
    obj1.name = @"OBJECT";
    
    [obj1 testMethod:^{
        NSLog(@"%@", obj1.name);
    }];
    
    self.testBlock();*/
    

    void (^testBlock) (void);
    
    testBlock = ^{
        NSLog(@"Test block");
    };
    
    NSString* (^testBlockWithParams) (NSString*, NSInteger) = ^(NSString* string, NSInteger intValue){
        //NSLog(@"test block with parameters %@ %d", string, intValue);
        return [NSString stringWithFormat:@"test block with parameters %@ %d", string, intValue];
    };
    
    
    NSLog(@"%@", testBlockWithParams(@"Test block with params", 111));
    testBlock();
    
    return YES;
}

- (void) testBlocksMethod:(void (^)(void)) testBlock {
    
    NSLog(@"testBlocksMethod");
    testBlock();
    
}

- (void) testBlocksMethod2:(void (^)(void)) testBlock {
    
    NSLog(@"testBlocksMethod");
    testBlock();
    
}

- (void) testMethod {
    NSLog(@"test method");
}

- (void) testMethodWithParams:(NSString*) string value:(NSInteger) intValue{
    NSLog(@"test method with parameters %@ %d", string, intValue);
}

-(void) testBlockWithParams:(NSString*) string{
    NSLog(@"%@", string);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
