//
//  LDSObject.m
//  Blocks Test
//
//  Created by Admin on 12.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSObject.h"

@interface LDSObject ()

@property (copy, nonatomic) ObjectBlock objectBlock;

@end

@implementation LDSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        __weak LDSObject* weakSelf = self;
        
        self.objectBlock = ^{
            NSLog(@"%@", weakSelf.name);
        };
    }
    return self;
}

- (void) testMethod: (ObjectBlock) block{
    block();
}

- (void) dealloc{
    NSLog(@"LDSObject is deallocated");
}

@end
