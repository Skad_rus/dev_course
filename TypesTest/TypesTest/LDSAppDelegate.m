//
//  LDSAppDelegate.m
//  TypesTest
//
//  Created by Admin on 16.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSStudent.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    /*
    BOOL boolVar = YES;
    
    NSInteger intVar = 10;
    
    NSUInteger uIntVar = 100;
    
    CGFloat floatVar = 1.5f;
    
    double doubleVar = 2.5f;
    
    NSNumber* boolObject = [NSNumber numberWithBool:boolVar];
    NSNumber* intObject = [NSNumber numberWithInt:intVar];
    NSNumber* uIntObject = [NSNumber numberWithUnsignedInt:uIntVar];
    NSNumber* floatObject = [NSNumber numberWithFloat:floatVar];
    NSNumber* doubleObject = [NSNumber numberWithDouble:doubleVar];
    
    NSArray* array = [NSArray arrayWithObjects:boolObject, intObject, uIntObject, floatObject, doubleObject, nil];
    NSLog(@"boolVar = %d, intVar = %d, uIntVar = %d, floatVar = %f, doubleVar = %f",
          [[array objectAtIndex:0] boolValue],
          [[array objectAtIndex:1] intValue],
          [[array objectAtIndex:2] unsignedIntegerValue],
          [[array objectAtIndex:3] floatValue],
          [[array objectAtIndex:4] doubleValue]);
    
    CGPoint point1 = CGPointMake(0, 5);
    CGPoint point2 = CGPointMake(3, 4);
    CGPoint point3 = CGPointMake(6, 5);
    CGPoint point4 = CGPointMake(7, 4);
    CGPoint point5 = CGPointMake(8, 5);
    
    
    NSArray*array2 = [NSArray arrayWithObjects:
                      [NSValue valueWithCGPoint:point1],
                      [NSValue valueWithCGPoint:point2],
                      [NSValue valueWithCGPoint:point3],
                      [NSValue valueWithCGPoint:point4],
                      [NSValue valueWithCGPoint:point5],
                      nil];
    
    for (NSValue* value in array2) {
        CGPoint p = [value CGPointValue];
        NSLog(@"point = %@", NSStringFromCGPoint(p));
    }
    */

    /*
    NSInteger a =floatVar;

    
    NSLog(@"boolVar = %d, intVar = %d, uIntVar = %d, floatVar = %f,doubleVar = %f", boolVar, intVar, uIntVar, floatVar, doubleVar);
    
    NSLog(@"boolVar = %ld, intVar = %ld, uIntVar = %ld, floatVar = %ld,doubleVar = %ld", sizeof(boolVar), sizeof(intVar), sizeof(uIntVar), sizeof(floatVar), sizeof(doubleVar));
    
    */
    
    
    /*
    LDSStudent* studentA = [[LDSStudent alloc] init];
    
    studentA.name = @"Best student";
    
    NSLog(@"StudentA name = %@", [studentA name]);
    
    LDSStudent* studentB = studentA;
    
    studentB.name = @"Bad student";
    
    NSLog(@"StudentA name = %@", [studentA name]);     
     */
    
    /*
    NSInteger a = 10;
    
    NSLog(@"a = %d",a);
    
    NSInteger b = 10;
    
    NSLog(@"a = %d, b = %d", a, b);
    
    NSInteger * c = &a;
    
    *c = 8;
    
    NSLog(@"a = %d, b = %d", a, b);
    
    NSInteger test = 0;
    NSInteger result = [self test:a testPointer:&test];
    NSLog(@"result = %d, test = %d", result, test);
     */

    
    //    UIViewAutoresizing

    /*
    LDSStudent* student = [[LDSStudent alloc] init];
    //student.gender = NO;
    [student setGender:LDSGenderMale];
     // LDSTaburetka t = 10;

    LDSStudent* profession = [[LDSStudent alloc] init];
    [profession setProfession:LDSProfessionDoctor];
    
    NSLog(@"%@", profession);
     */
    
    //NSMutableArray *mutArray1 = [NSMutableArray new];
    int conditionForI = 15 + arc4random() % 11;
    CGRect rect3x3 = CGRectMake(4, 4, 3, 3);
    NSLog(@"Condition for i is i <= %d", conditionForI);
    
    for (int i = 0; i <= conditionForI ; i++)
    {
        CGPoint point;
        point = CGPointMake(arc4random() % 11, arc4random() % 11);

        //[mutArray1 addObject:[NSValue valueWithCGPoint:point]];
        if (CGRectContainsPoint(rect3x3,point) == YES) {
            NSLog(@"%@ - point is IN sector", [NSValue valueWithCGPoint:point]);
        }
        else{
            NSLog(@"%@ - point is OUT OF sector", [NSValue valueWithCGPoint:point]);
        }
        /*
        if (point.x > 3 && point.x < 7 && point.y > 3 && point.y < 7) {
            NSLog(@"%@", mutArray1[i]);
        }
        else{
            NSLog(@"%@ - point is out of sector", mutArray1[i]);
        }
        */
    }
    


/*

    CGPoint point;
    
    point.x = arc4random() % 11;
    point.y = arc4random() % 11;
    
    point = CGPointMake(arc4random() % 11, arc4random() % 11);
    
    CGSize size;
    
    size.width = 10;
    size.height = 8.7f;
    
    CGRect rect;
    
    rect.origin = point;
    rect.size = size;
    
    //rect = CGRectMake(0, 0, 30, 60);
    BOOL result = CGRectContainsPoint(rect, point);

     */
    
    return YES;
}

-(NSInteger) test: (NSInteger) test testPointer:(NSInteger*) testPointer {
    *testPointer=5;
    return test;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
