//
//  LDSStudent.h
//  TypesTest
//
//  Created by Admin on 16.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    LDSGenderMale,
    LDSGenderFemale
    
} LDSGender;

typedef enum {
    
    LDSProfessionIOSDeveloper,
    LDSProfessionAndroiddeveloper = 7,
    LDSProfessionTaxiDriver,
    LDSProfessionDoctor,
    LDSProfessionTeacher
    
} LDSProfession;

typedef enum PlayerStateType : NSInteger PlayerStateType;
enum PlayerStateType : NSInteger {
    PlayerStateOff,
    PlayerStatePlaying,
    PlayerStatePaused
};

//typedef NSInteger LDSTaburetka;

@interface LDSStudent : NSObject

@property (strong, nonatomic) NSString* name;

@property (assign, nonatomic) LDSGender gender;
@property (assign, nonatomic) LDSProfession profession;

@end
