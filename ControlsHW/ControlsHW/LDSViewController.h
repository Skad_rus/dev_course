//
//  LDSViewController.h
//  ControlsHW
//
//  Created by Admin on 13.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *testView;
@property (weak, nonatomic) IBOutlet UIImageView *testImageView;
@property (weak, nonatomic) IBOutlet UISwitch *rotationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *scaleSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *translationSwitch;
@property (weak, nonatomic) IBOutlet UISlider *speedSlider;
@property (weak, nonatomic) IBOutlet UISegmentedControl *imageSelectingControl;
@property (assign, nonatomic) NSInteger duration;


- (IBAction)rotationSwitchAction:(UISwitch *)sender;
- (IBAction)scaleSwitchAction:(UISwitch *)sender;
- (IBAction)translationSwitchAction:(UISwitch *)sender;
- (IBAction)speedSliderAction:(UISlider *)sender;
- (IBAction)changedSelection:(UISegmentedControl *)sender;

@end
