//
//  LDSViewController.m
//  ControlsHW
//
//  Created by Admin on 13.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Ученик.
 
 1. Расположите тестируюмую вьюху на верхней половине экрана
 2. На нижней половине создайте 3 свича: Rotation, Scale, Translation. По умолчанию все 3 выключены
 3. Также создайте сладер скорость, со значениями от 0.5 до 2, стартовое значение 1
 4. Создайте соответствующие проперти для свитчей и слайдера, а также методы для события valueChanged
 
 Студент.
 
 5. Добавьте сегментед контрол с тремя разными сегментами
 6. Они должны соответствовать трем разным картинкам, которые вы должны добавить
 7. Когда переключаю сегменты, то картинка должна меняться на соответствующую
 
 Мастер.
 
 8. Как только мы включаем один из свичей, наша вьюха должна начать соответствующую анимацию
 (либо поворот, либо скеил, либо перенос). Используйте свойство transform из урока об анимациях
 9. Так же следует помнить, что если вы переключили свич, но какойто другой включен одновременно с ним, то вы должны делать уже двойную анимацию. Например и увеличение и поворот одновременно (из урока про анимации)
 10. Анимации должны быть бесконечно повторяющимися, единственное что их может остановить, так это когда все три свича выключены
 
 Супермен.
 
 11. Добавляем использование слайдера. Слайдер регулирует скорость. То есть когда значение на 0.5, то скорость анимаций должна быть в два раза медленнее, а если на 2, то в два раза быстрее обычной.
 12. Попробуйте сделать так, чтобы когда двигался слайдер, то анимация обновлялась без прерывания, а скорость изменялась в соответствующую сторону.
*/

#import "LDSViewController.h"

@interface LDSViewController ()

@end

typedef enum {
    
    LDSImageNumber1,
    LDSImageNumber2,
    LDSImageNumber3
    
} LDSImageNumberType;

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.imageSelectingControl.selectedSegmentIndex = arc4random() % 3;
    
    switch (self.imageSelectingControl.selectedSegmentIndex) {
            
        case LDSImageNumber1:
            self.testImageView.image = [UIImage imageNamed:@"1.png"];
            break;
            
        case LDSImageNumber2:
            self.testImageView.image = [UIImage imageNamed:@"2.png"];
            break;
            
        case LDSImageNumber3:
            self.testImageView.image = [UIImage imageNamed:@"3.png"];
            break;
            
    }
    
    [self moveView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

//Метод, реалирущий бесконечные трансформации (поворот, трансформацию, изменение размеров)
- (void) moveView {//:(UIImageView*) view {
    
    CGFloat x = arc4random() % (int)self.testView.bounds.size.width;
    CGFloat y = arc4random() % (int)self.testView.bounds.size.height;
    
    CGFloat s = (float)(arc4random() % 151) / 100.f + 0.5f;
    
    CGFloat r = (float)(arc4random() % (int)(M_PI * 2*10000)) / 10000 - M_PI;
    
    CGPoint center = self.testImageView.center;
    
    if (self.translationSwitch.isOn == YES) {
        
        center = CGPointMake(x, y);
        
    }
    
    CGAffineTransform transform;
    CGAffineTransform scale;
    CGAffineTransform rotation;
    
    if (self.scaleSwitch.isOn == YES) {
        
        scale = CGAffineTransformMakeScale(s, s);
        transform = scale;
        
    }
    
    if (self.rotationSwitch.isOn == YES) {
        
        rotation = CGAffineTransformMakeRotation(r);
        transform = rotation;
        
    }
    
    if (self.scaleSwitch.isOn == YES && self.rotationSwitch.isOn == YES) {
        
        transform = CGAffineTransformConcat(scale, rotation);
        
    }
    
    [UIView animateWithDuration:3 / self.speedSlider.value
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         self.testImageView.center = center;
                         
                         if (self.scaleSwitch.isOn == YES || self.rotationSwitch.isOn == YES) {
                         
                             self.testImageView.transform = transform;
                         
                         }

                     }
     
                     completion:^(BOOL finished) {
                         
                         [self moveView];
                         
                     }];
    
 
    
}

//Метод, реализующий проверку состояний свитчей
- (void) switchesCheck {

    if (self.scaleSwitch.isOn == NO && self.rotationSwitch.isOn == NO && self.translationSwitch.isOn == NO) {

        [self.testImageView.layer removeAllAnimations];
        
    }
    
}

#pragma mark - Actions

- (IBAction)rotationSwitchAction:(UISwitch *)sender {
    
    [self switchesCheck];
    
}

- (IBAction)scaleSwitchAction:(UISwitch *)sender {
    
    [self switchesCheck];
 
}

- (IBAction)translationSwitchAction:(UISwitch *)sender {
    
    [self switchesCheck];
    
}

- (IBAction)speedSliderAction:(UISlider *)sender {
    
    CGPoint center = self.testImageView.center;
    CGAffineTransform transform = self.testImageView.transform;
    
    [UIView animateWithDuration:0.75 / self.speedSlider.value
                          delay:0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.testImageView.center = center;
                         
                         if (self.scaleSwitch.isOn == YES || self.rotationSwitch.isOn == YES) {
                             
                             self.testImageView.transform = transform;
                             
                         }
                         
                     }
     
                     completion:^(BOOL finished) {
                         
                     }];
    
}

- (IBAction)changedSelection:(UISegmentedControl *)sender {
    
    switch (sender.selectedSegmentIndex) {
            
        case LDSImageNumber1:
            self.testImageView.image = [UIImage imageNamed:@"1.png"];
            break;
            
        case LDSImageNumber2:
            self.testImageView.image = [UIImage imageNamed:@"2.png"];
            break;
            
        case LDSImageNumber3:
            self.testImageView.image = [UIImage imageNamed:@"3.png"];
            break;
            
    }
    
}

@end
