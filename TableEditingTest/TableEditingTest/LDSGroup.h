//
//  LDSGroup.h
//  TableEditingTest
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSGroup : NSObject

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSArray* students;

@end
