//
//  main.m
//  StringsTest
//
//  Created by Admin on 08.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LDSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LDSAppDelegate class]));
    }
}
