//
//  LDSUniversity.m
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSUniversity.h"
#import "LDSStudent.h"


@implementation LDSUniversity

@dynamic name;
@dynamic students;
@dynamic courses;

@end
