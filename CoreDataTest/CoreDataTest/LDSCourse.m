//
//  LDSCourse.m
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSCourse.h"
#import "LDSStudent.h"
#import "LDSUniversity.h"


@implementation LDSCourse

@dynamic name;
@dynamic university;
@dynamic students;
@dynamic bestStudents;
@dynamic studentsWithManyCourses;

@end
