//
//  LDSStudent.m
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSStudent.h"
#import "LDSCar.h"
#import "LDSCourse.h"
#import "LDSUniversity.h"


@implementation LDSStudent

@dynamic dateOfBirth;
@dynamic firstName;
@dynamic lastName;
@dynamic score;
@dynamic car;
@dynamic university;
@dynamic courses;

@end
