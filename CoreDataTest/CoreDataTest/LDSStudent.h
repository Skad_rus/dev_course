//
//  LDSStudent.h
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "LDSObject.h"

@class LDSCar, LDSCourse, LDSUniversity;

@interface LDSStudent : LDSObject

@property (nonatomic, retain) NSDate * dateOfBirth;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * score;
@property (nonatomic, retain) LDSCar *car;
@property (nonatomic, retain) LDSUniversity *university;
@property (nonatomic, retain) NSSet *courses;
@end

@interface LDSStudent (CoreDataGeneratedAccessors)

- (void)addCoursesObject:(LDSCourse *)value;
- (void)removeCoursesObject:(LDSCourse *)value;
- (void)addCourses:(NSSet *)values;
- (void)removeCourses:(NSSet *)values;

@end
