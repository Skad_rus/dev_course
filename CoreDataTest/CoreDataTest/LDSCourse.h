//
//  LDSCourse.h
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "LDSObject.h"

@class LDSStudent, LDSUniversity;

@interface LDSCourse : LDSObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) LDSUniversity *university;
@property (nonatomic, retain) NSSet *students;
@property (nonatomic, retain) NSArray *bestStudents;
@property (nonatomic, retain) NSArray *studentsWithManyCourses;

@end

@interface LDSCourse (CoreDataGeneratedAccessors)

- (void)addStudentsObject:(LDSStudent *)value;
- (void)removeStudentsObject:(LDSStudent *)value;
- (void)addStudents:(NSSet *)values;
- (void)removeStudents:(NSSet *)values;

@end
