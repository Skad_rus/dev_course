//
//  LDSDataManager.m
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDataManager.h"
#import "LDSStudent.h"
#import "LDSCar.h"
#import "LDSUniversity.h"
#import "LDSCourse.h"

static NSString* firstNames[] = {
    @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
    @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
    @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
    @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
    @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
    @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
    @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
    @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
    @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
    @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie"
};

static NSString* lastNames[] = {
    
    @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
    @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
    @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
    @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
    @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
    @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
    @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
    @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
    @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
    @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook"
};

static NSString* carModelNames[] = {
    @"Dodge", @"Toyota", @"BMW", @"Lada", @"Volga"
};

static NSString* universityNames[] = {
    @"ONPU", @"BSU", @"BSUIR", @"MIT", @"Stanford"
};

@implementation LDSDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (LDSDataManager*) sharedManager{
    
    static LDSDataManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[LDSDataManager alloc] init];
    });
    
    return manager;
    
}

- (LDSStudent*) addRandomStudent {
    
    LDSStudent* student = [NSEntityDescription insertNewObjectForEntityForName:@"LDSStudent" inManagedObjectContext:self.managedObjectContext];
    
    student.score = @((float)arc4random_uniform(201) / 100.f + 2.f);
    student.dateOfBirth = [NSDate dateWithTimeIntervalSince1970: 60 * 60 * 24 * 365 * arc4random_uniform(31)];
    student.firstName = firstNames[arc4random_uniform(50)];
    student.lastName = lastNames[arc4random_uniform(50)];
    
    return student;
    
}

- (LDSCar*) addRandomCar {
    
    LDSCar* car = [NSEntityDescription insertNewObjectForEntityForName:@"LDSCar" inManagedObjectContext:self.managedObjectContext];
    
    car.model = carModelNames[arc4random_uniform(5)];
    
    return car;
    
}

- (LDSUniversity*) addUniversity {
    
    LDSUniversity* university = [NSEntityDescription insertNewObjectForEntityForName:@"LDSUniversity" inManagedObjectContext:self.managedObjectContext];
    
    university.name = universityNames[arc4random_uniform(5)];
    
    return university;
    
}

- (LDSCourse*) addCourseWithName:(NSString*) name {
    
    LDSCourse* course = [NSEntityDescription insertNewObjectForEntityForName:@"LDSCourse" inManagedObjectContext:self.managedObjectContext];
    
    course.name = name;
    
    return course;
    
}

- (NSArray*) allObjects {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description = [NSEntityDescription entityForName:@"LDSObject" inManagedObjectContext:self.managedObjectContext];
    
    
    [request setEntity:description];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    
    if (requestError) {
        NSLog(@"%@", [requestError localizedDescription]);
    }
    
    return resultArray;
}

- (void) printArray:(NSArray*) array {
    
    for (id object in array) {
        
        if ([object isKindOfClass:[LDSCar class]]) {
            
            LDSCar* car = (LDSCar*) object;
            NSLog(@"CAR: %@, OWNER: %@ %@", car.model, car.owner.firstName, car.owner.lastName);
            
        } else if ([object isKindOfClass:[LDSStudent class]]) {
            
            LDSStudent* student = (LDSStudent*) object;
            NSLog(@"STUDENT: %@ %@, SCORE: %1.2f, COURSES: %d", student.firstName, student.lastName, [student.score floatValue], [student.courses count]);
            
        } else if ([object isKindOfClass:[LDSUniversity class]]) {
            
            LDSUniversity* university = (LDSUniversity*) object;
            NSLog(@"UNIVERSITY: %@ Students: %d", university.name, [university.students count]);
            
        } else if ([object isKindOfClass:[LDSCourse class]]) {
            
            LDSCourse* course = (LDSCourse*) object;
            NSLog(@"COURSE: %@ Students: %d", course.name, [course.students count]);
            
        }
        
    }
    
    NSLog(@"COUNT = %d", [array count]);
    
}

- (void) printAllObjects {
    
    NSArray* allObjects = [self allObjects];
    
    [self printArray:allObjects];
    
}

- (void) deleteAllObjects {
    
    NSArray* allObjects = [self allObjects];
    
    for (id object in allObjects) {
        
        [self.managedObjectContext deleteObject:object];
        
    }
    
    [self.managedObjectContext save:nil];
    
}

- (void) generateAndAddUniversity {

    
    //[self deleteAllObjects];
     
     NSError* error = nil;
     
     NSArray* courses = @[[self addCourseWithName:@"iOS"],
     [self addCourseWithName:@"Android"],
     [self addCourseWithName:@"PHP"],
     [self addCourseWithName:@"Javascript"],
     [self addCourseWithName:@"HTML"]];
     
     LDSUniversity* university = [self addUniversity];
     
     [university addCourses:[NSSet setWithArray:courses]];
     
     for (int i = 0; i < 100; i++) {
     
     LDSStudent* student = [self addRandomStudent];
     
     if (arc4random_uniform(1000) < 500) {
     
     LDSCar* car = [self addRandomCar];
     student.car = car;
     
     }
     
     student.university = university;
     
     NSInteger number = arc4random_uniform(5) + 1;
     
     while ([student.courses count] < number) {
     
     LDSCourse* course = [courses objectAtIndex:arc4random_uniform(5)];
     
     if (![student.courses containsObject:course]) {
     [student addCoursesObject:course];
     }
     
     }
     
     //[university addStudentsObject:student];
     
     }
     
     if (![self.managedObjectContext save:&error]) {
     NSLog(@"%@", [error localizedDescription]);
     }
    
    
    //[self deleteAllObjects];
    
    //[self printAllObjects];
    
    /*
     NSFetchRequest* request = [[NSFetchRequest alloc] init];
     
     NSEntityDescription* description = [NSEntityDescription entityForName:@"LDSCourse" inManagedObjectContext:self.managedObjectContext];
     
     [request setEntity:description];
     */
    
    //[request setRelationshipKeyPathsForPrefetching:@[@"courses"]];
    
    /*
     NSSortDescriptor* firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
     NSSortDescriptor* lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
     
     [request setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
     
     NSArray* validNames = @[@"Clyde", @"Pam", @"Rosanna"];
     */
    
    /*
     NSSortDescriptor* nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
     
     [request setSortDescriptors:@[nameDescriptor]];
     */
    
    /*
     NSPredicate* predicate = [NSPredicate predicateWithFormat:@"score > %f AND score <= %f AND courses.@count >= %d AND firstName IN %@", 3.f, 3.5f, 3, validNames];
     */
    
    /*
     NSPredicate* predicate = [NSPredicate predicateWithFormat:@"@sum.students.score > %f", 156.f];
     
     [request setPredicate:predicate];
     */
    
    /*
     NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SUBQUERY(students, $student, $student.car.model == %@).@count >= %d", @"BMW", 5];
     
     [request setPredicate:predicate];
     */
    
    //[request setFetchBatchSize:20];
    //[request setFetchOffset:10];
    //[request setFetchLimit:35];
    
    /*
     NSError* requestError = nil;
     NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
     
     [self printArray:resultArray];
     */
    
    /*
     NSFetchRequest* request = [[self.managedObjectModel fetchRequestTemplateForName:@"FetchStudents"] copy];
     
     NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:nil];
     
     NSSortDescriptor* firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
     NSSortDescriptor* lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
     
     [request setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
     
     [self printArray:resultArray];
     */
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description = [NSEntityDescription entityForName:@"LDSCourse" inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:description];
    
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    for (LDSCourse* course in resultArray) {
        
        NSLog(@"COURSE NAME = %@", course.name);
        NSLog(@"BEST STUDENTS:");
        [self printArray:course.bestStudents];
        NSLog(@"BUZY STUDENTS:");
        [self printArray:course.studentsWithManyCourses];
        
    }
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CoreDataTest.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
        
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
