//
//  LDSCar.h
//  CoreDataTest
//
//  Created by Admin on 17.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "LDSObject.h"

@class LDSStudent;

@interface LDSCar : LDSObject

@property (nonatomic, retain) NSString * model;
@property (nonatomic, retain) LDSStudent *owner;

@end
