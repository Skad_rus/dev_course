//
//  LDSCoursesViewController.h
//  CoreDataTest
//
//  Created by Admin on 20.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSCoreDataTableViewController.h"

@class LDSUniversity;

@interface LDSCoursesViewController : LDSCoreDataTableViewController

@property (strong, nonatomic) LDSUniversity* university;

@end
