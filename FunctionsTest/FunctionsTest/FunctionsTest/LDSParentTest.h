//
//  LDSParentTest.h
//  FunctionsTest
//
//  Created by Admin on 02.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSParentTest : NSObject

+(void) whoAreYou;

- (void) sayHello;
- (void) say:(NSString*) string;
- (void) say:(NSString*) string1 and: (NSString*) string2;
+ (void) momSays;

-(NSString*) saySomething;
-(NSString*) saySomeNumberString;

@end
