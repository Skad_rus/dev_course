//
//  LDSChildTest.m
//  FunctionsTest
//
//  Created by Admin on 02.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSChildTest.h"

@implementation LDSChildTest

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"Child class is created");
    }
    return self;
}

-(NSString*) saySomeNumberString {
    return @"Something!";
    
}

-(NSString*) saySomething{
    return [super saySomeNumberString];
    
   // NSString* string = [self saySomeNumberString];
   // return string;
}
@end
