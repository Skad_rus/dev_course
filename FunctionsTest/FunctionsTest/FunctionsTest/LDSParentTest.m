//
//  LDSParentTest.m
//  FunctionsTest
//
//  Created by Admin on 02.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSParentTest.h"

@implementation LDSParentTest

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"Parent class is created");
    }
    return self;
}

+(void) whoAreYou{
    NSLog(@"I AM LDSParentTest Obj C class");
}

+ (void) momSays {
    NSLog(@"What did y do all day along?");
}

- (void) sayHello{
    NSLog(@"Parent says hello!");
}

- (void) say:(NSString*) string {
    NSLog(@"%@", string);
}


- (void) say:(NSString*) string1 and: (NSString*) string2 {
    NSLog(@"%@, %@", string1, string2);
    return;
}

-(NSString*) saySomeNumberString {
    return [NSString stringWithFormat:@"%@", [NSDate date]];
}

-(NSString*) saySomething{
//    return @"I won't say anything :)";
    
    NSString* string = [self saySomeNumberString];
    return string;
}





@end
