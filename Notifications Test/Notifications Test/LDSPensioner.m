//
//  LDSPensioner.m
//  Notifications Test
//
//  Created by Admin on 04.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSPensioner.h"
#import "LDSGovernment.h"
#import "LDSAppDelegate.h"

@implementation LDSPensioner

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSNotificationCenter * ns = [NSNotificationCenter defaultCenter];
        
        [ns addObserver:self
               selector:@selector(PensionChangedNotification:)
                   name:LDSGovernmentPensionDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(AveragePriceChangedNotification:)
                   name:LDSGovernmentAveragePriceDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(InflationChangedNotification:)
                   name:LDSGovernmentInflationDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(DidEnterBackgroundNotification:)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(DidBecomeActiveNotification:)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];
        

        
    }
    return self;
}

- (void) dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - Notifications

- (void) DidBecomeActiveNotification: (NSNotification*) notification{
    
    NSLog(@"Pensioner wakes up.");
    
}

- (void) DidEnterBackgroundNotification: (NSNotification*) notification{
    
    NSLog(@"Pensioner goes to sleep.");
    
}

- (void) InflationChangedNotification: (NSNotification*) notification{
    
    NSNumber* value = [notification.userInfo objectForKey:LDSGovernmentInflationUserInfoKey];
    
    CGFloat inflation = [value floatValue];
    
    if (inflation > self.inflation) {
        NSLog(@"Horrible president - inflation is rising!!!");
    }
    else {
        NSLog(@"Inflation decreases, but president is horrible!!!");
    }
    
    self.inflation = inflation;
    
}

- (void) PensionChangedNotification: (NSNotification*) notification{
    
    NSLog(@"Pensioner knows, that pension has changed");
    
}

- (void) AveragePriceChangedNotification: (NSNotification*) notification{
    
    NSLog(@"Pensioner knows, that average price has changed");
    
}

@end
