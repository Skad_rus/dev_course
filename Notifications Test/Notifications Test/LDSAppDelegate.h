//
//  LDSAppDelegate.h
//  Notifications Test
//
//  Created by Admin on 03.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString* const DidEnterBackgroundNotification;

extern NSString* const DidEnterBackgroundUserInfoKey;

@interface LDSAppDelegate : UIResponder <UIApplicationDelegate>



@property (strong, nonatomic) UIWindow *window;

@end
