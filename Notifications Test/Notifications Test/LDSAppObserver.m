//
//  LDSAppObserver.m
//  Notifications Test
//
//  Created by Admin on 04.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppObserver.h"

@implementation LDSAppObserver

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSNotificationCenter * ns = [NSNotificationCenter defaultCenter];
        
        [ns addObserver:self
               selector:@selector(ApplicationWillResignActiveNotification:)
                   name:UIApplicationWillResignActiveNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(ApplicationDidEnterBackgroundNotification:)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];
        
        
        [ns addObserver:self
               selector:@selector(ApplicationWillEnterForegroundNotification:)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(ApplicationDidBecomeActiveNotification:)
                   name:UIApplicationDidBecomeActiveNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(ApplicationWillTerminateNotification:)
                   name:UIApplicationWillTerminateNotification
                 object:nil];
    }
    return self;
}

- (void) ApplicationWillResignActiveNotification: (NSNotification*) notification{
    NSLog(@" Observer: ApplicationWillResignActiveNotification");
}

- (void) ApplicationDidEnterBackgroundNotification: (NSNotification*) notification{
    NSLog(@" Observer: ApplicationDidEnterBackgroundNotification");
}

- (void) ApplicationWillEnterForegroundNotification: (NSNotification*) notification{
    NSLog(@" Observer: ApplicationWillEnterForegroundNotification");
}

- (void) ApplicationDidBecomeActiveNotification: (NSNotification*) notification{
    NSLog(@" Observer: ApplicationDidBecomeActiveNotification");
}

- (void) ApplicationWillTerminateNotification: (NSNotification*) notification{
    NSLog(@" Observer: ApplicationWillTerminateNotification");
}


@end
