//
//  LDSDoctor.m
//  Notifications Test
//
//  Created by Admin on 03.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDoctor.h"
#import "LDSGovernment.h"
#import "LDSAppDelegate.h"

@implementation LDSDoctor

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSNotificationCenter * ns = [NSNotificationCenter defaultCenter];
        
        [ns addObserver:self
               selector:@selector(SalaryChangedNotification:)
                   name:LDSGovernmentSalaryDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(AveragePriceChangedNotification:)
                   name:LDSGovernmentAveragePriceDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(InflationChangedNotification:)
                   name:LDSGovernmentInflationDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(DidEnterBackgroundNotification:)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(DidBecomeActiveNotification:)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];
        
        
    }
    return self;
}

- (void) dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - Notifications

- (void) DidBecomeActiveNotification: (NSNotification*) notification{
    
    NSLog(@"Doctor wakes up.");
    
}

- (void) DidEnterBackgroundNotification: (NSNotification*) notification{
    
    NSLog(@"Doctor goes to sleep.");
    
}

- (void) SalaryChangedNotification: (NSNotification*) notification{
    
    NSLog(@"Doctor knows, that salary has changed");
    
}

- (void) AveragePriceChangedNotification: (NSNotification*) notification{
    
    NSLog(@"Doctor knows, that average price has changed");
    
}

- (void) InflationChangedNotification: (NSNotification*) notification{
    
     NSNumber* value = [notification.userInfo objectForKey:LDSGovernmentInflationUserInfoKey];
    
     CGFloat inflation = [value floatValue];
     
     if (inflation > self.inflation) {
     NSLog(@"Doctors are NOT happy");
     }
     else {
     NSLog(@"Doctors are happy");
     }
     
     self.inflation = inflation;
    
}

@end
