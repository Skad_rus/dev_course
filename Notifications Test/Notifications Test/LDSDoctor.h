//
//  LDSDoctor.h
//  Notifications Test
//
//  Created by Admin on 03.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSDoctor : NSObject

@property (assign, nonatomic) CGFloat salary;
@property (assign, nonatomic) CGFloat averagePrice;
@property (assign, nonatomic) CGFloat inflation;

@end
