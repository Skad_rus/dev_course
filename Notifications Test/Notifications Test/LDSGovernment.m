//
//  LDSGovernment.m
//  Notifications Test
//
//  Created by Admin on 03.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSGovernment.h"

NSString* const LDSGovernmentTaxLevelDidChangeNotification = @"LDSGovernmentTaxLevelDidChangeNotification";
NSString* const LDSGovernmentSalaryDidChangeNotification = @"LDSGovernmentSalaryDidChangeNotification";
NSString* const LDSGovernmentPensionDidChangeNotification = @"LDSGovernmentPensionDidChangeNotification";
NSString* const LDSGovernmentAveragePriceDidChangeNotification = @"LDSGovernmentAveragePriceDidChangeNotification";
NSString* const LDSGovernmentInflationDidChangeNotification = @"LDSGovernmentInflationDidChangeNotification";

NSString* const LDSGovernmentTaxLevelUserInfoKey = @"LDSGovernmentTaxLevelUserInfoKey";
NSString* const LDSGovernmentSalaryUserInfoKey = @"LDSGovernmentSalaryUserInfoKey";
NSString* const LDSGovernmentPensionUserInfoKey = @"LDSGovernmentPensionUserInfoKey";
NSString* const LDSGovernmentAveragePriceUserInfoKey = @"LDSGovernmentAveragePriceUserInfoKey";
NSString* const LDSGovernmentInflationUserInfoKey = @"LDSGovernmentInflationUserInfoKey";

@implementation LDSGovernment

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.taxLevel = 5.f;
        self.salary = 1100;
        self.pension = 500;
        self.averagePrice = 10;
        self.inflation = 101.3f;
    }
    return self;
}


- (void) setInflation: (CGFloat) inflation {
    NSLog(@" ");
    _inflation = inflation;
    
    NSDictionary* dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:inflation] forKey:LDSGovernmentInflationUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LDSGovernmentInflationDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setTaxLevel: (CGFloat) taxLevel {
    _taxLevel = taxLevel;
    
    NSDictionary* dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:taxLevel] forKey:LDSGovernmentTaxLevelUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LDSGovernmentTaxLevelDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setSalary: (CGFloat) salary {
    _salary = salary;
    
    NSDictionary* dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:salary] forKey:LDSGovernmentSalaryUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LDSGovernmentSalaryDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setPension: (CGFloat) pension {
    _pension = pension;
    
    NSDictionary* dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:pension] forKey:LDSGovernmentPensionUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LDSGovernmentPensionDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setAveragePrice: (CGFloat) averagePrice {
    _averagePrice = averagePrice;
    
    NSDictionary* dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:averagePrice] forKey:LDSGovernmentAveragePriceUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LDSGovernmentAveragePriceDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

@end
