//
//  LDSBusinessman.m
//  Notifications Test
//
//  Created by Admin on 04.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSBusinessman.h"
#import "LDSGovernment.h"
#import "LDSAppDelegate.h"

@implementation LDSBusinessman

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSNotificationCenter * ns = [NSNotificationCenter defaultCenter];
        
        [ns addObserver:self
               selector:@selector(TaxLevelChangedNotification:)
                   name:LDSGovernmentTaxLevelDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(AveragePriceChangedNotification:)
                   name:LDSGovernmentAveragePriceDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(InflationChangedNotification:)
                   name:LDSGovernmentInflationDidChangeNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(DidEnterBackgroundNotification:)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];
        
        [ns addObserver:self
               selector:@selector(DidBecomeActiveNotification:)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];
 
    }
    return self;
}

- (void) dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - Notifications

- (void) DidBecomeActiveNotification: (NSNotification*) notification{
    
    NSLog(@"Businessman wakes up.");
    
}

- (void) DidEnterBackgroundNotification: (NSNotification*) notification{
    
    NSLog(@"Businessman goes to sleep.");
    
}

- (void) InflationChangedNotification: (NSNotification*) notification{
    
    NSNumber* value = [notification.userInfo objectForKey:LDSGovernmentInflationUserInfoKey];
    
    CGFloat inflation = [value floatValue];
    
    if (inflation > self.inflation) {
        NSLog(@"Excellent, the nineties are back!!!");
    }
    else {
        NSLog(@"Inflation falls - excellent - it is useful for business.");
    }
    
    self.inflation = inflation;
    
}

- (void) TaxLevelChangedNotification: (NSNotification*) notification{
    
    NSLog(@"Businessman knows, that tax level has changed");
    
}

- (void) AveragePriceChangedNotification: (NSNotification*) notification{
    
    NSLog(@"Businessman knows, that average price has changed");
    
}

@end
