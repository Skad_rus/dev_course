//
//  LDSAppDelegate.m
//  Notifications Test
//
//  Created by Admin on 03.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSGovernment.h"
#import "LDSDoctor.h"
#import "LDSPensioner.h"
#import "LDSBusinessman.h"
#import "LDSAppObserver.h"

@interface LDSAppDelegate ()

@property (strong, nonatomic) LDSGovernment* government;
@property (strong, nonatomic) LDSDoctor* doctor;
@property (strong, nonatomic) LDSBusinessman* businessman;
@property (strong, nonatomic) LDSPensioner* pensioner;
@property (strong, nonatomic) LDSAppObserver * appObserver;

@end

NSString* const DidEnterBackgroundNotification = @"DidEnterBackgroundNotification";
NSString* const DidEnterBackgroundUserInfoKey = @"DidEnterBackgroundUserInfoKey";

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    self.government = [[LDSGovernment alloc] init];
    self.doctor = [[LDSDoctor alloc] init];
    self.businessman = [[LDSBusinessman alloc] init];
    self.pensioner = [[LDSPensioner alloc] init];
    self.appObserver = [[LDSAppObserver alloc] init];

    LDSDoctor* doctor1 = [[LDSDoctor alloc] init];
    
    LDSBusinessman* businessman1 = [[LDSBusinessman alloc] init];
    
    LDSPensioner* pensioner1 = [[LDSPensioner alloc] init];
    
    
    doctor1.averagePrice = businessman1.averagePrice = pensioner1.averagePrice = self.government.averagePrice;
    
    doctor1.inflation = businessman1.inflation = pensioner1.inflation = self.government.inflation;
    

    self.government.taxLevel = 5.5;
    self.government.salary = 1100;
    self.government.averagePrice = 15;
    self.government.pension = 550;
    self.government.inflation = 101.3f;
   
    self.government.salary = 1050;    
    self.government.salary = 1150;
    self.government.salary = 900;
    
    
    NSLog(@" ");
    
    self.government.inflation = 101.3f;
    self.government.inflation = 100.4f;
    self.government.inflation = 92.3f;
    self.government.inflation = 102.3f;
    self.government.inflation = 200.3f;
    

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@" AppDelegate: applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
     NSLog(@" AppDelegate: applicationDidEnterBackground");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@" AppDelegate: applicationWillEnterForeground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@" AppDelegate: applicationDidBecomeActive");

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@" AppDelegate: applicationWillTerminate");

    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
