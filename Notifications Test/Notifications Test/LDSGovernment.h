//
//  LDSGovernment.h
//  Notifications Test
//
//  Created by Admin on 03.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const LDSGovernmentTaxLevelDidChangeNotification;
extern NSString* const LDSGovernmentSalaryDidChangeNotification;
extern NSString* const LDSGovernmentPensionDidChangeNotification;
extern NSString* const LDSGovernmentAveragePriceDidChangeNotification;
extern NSString* const LDSGovernmentInflationDidChangeNotification;

extern NSString* const LDSGovernmentTaxLevelUserInfoKey;
extern NSString* const LDSGovernmentSalaryUserInfoKey;
extern NSString* const LDSGovernmentPensionUserInfoKey;
extern NSString* const LDSGovernmentAveragePriceUserInfoKey;
extern NSString* const LDSGovernmentInflationUserInfoKey;



@interface LDSGovernment : NSObject

@property (assign, nonatomic) CGFloat taxLevel;
@property (assign, nonatomic) CGFloat salary;
@property (assign, nonatomic) CGFloat pension;
@property (assign, nonatomic) CGFloat averagePrice;
@property (assign, nonatomic) CGFloat inflation;


@end
