//
//  LDSViewController.m
//  ControlsTest
//
//  Created by Admin on 12.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Ученик.
 
 1. Расположите тестируюмую вьюху на верхней половине экрана
 2. На нижней половине создайте 3 свича: Rotation, Scale, Translation. По умолчанию все 3 выключены
 3. Также создайте сладер скорость, со значениями от 0.5 до 2, стартовое значение 1
 4. Создайте соответствующие проперти для свитчей и слайдера, а также методы для события valueChanged
 
 Студент.
 
 5. Добавьте сегментед контрол с тремя разными сегментами
 6. Они должны соответствовать трем разным картинкам, которые вы должны добавить
 7. Когда переключаю сегменты, то картинка должна меняться на соответствующую
 
 Мастер.
 
 8. Как только мы включаем один из свичей, наша вьюха должна начать соответствующую анимацию
 (либо поворот, либо скеил, либо перенос). Используйте свойство transform из урока об анимациях
 9. Так же следует помнить, что если вы переключили свич, но какойто другой включен одновременно с ним, то вы должны делать уже двойную анимацию. Например и увеличение и поворот одновременно (из урока про анимации)
 10. Анимации должны быть бесконечно повторяющимися, единственное что их может остановить, так это когда все три свича выключены
 
 Супермен.
 
 11. Добавляем использование слайдера. Слайдер регулирует скорость. То есть когда значение на 0.5, то скорость анимаций должна быть в два раза медленнее, а если на 2, то в два раза быстрее обычной.
 12. Попробуйте сделать так, чтобы когда двигался слайдер, то анимация обновлялась без прерывания, а скорость изменялась в соответствующую сторону.
*/

#import "LDSViewController.h"

@interface LDSViewController ()

@end

typedef enum {
    
    LDSColorSchemeRGB,
    LDSColorSchemeHSV
    
} LDSColorSchemeType;

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self refreshScreen];
    
    self.colorSchemeControl.selectedSegmentIndex = LDSColorSchemeHSV;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Private Methods

- (void) refreshScreen {
    
    CGFloat red     =   self.redComponentSlider.value;
    CGFloat green   =   self.greenComponentSlider.value;
    CGFloat blue    =   self.blueComponentSlider.value;

    UIColor* color = nil;
    
    if (self.colorSchemeControl.selectedSegmentIndex == LDSColorSchemeRGB) {
        
        color = [UIColor colorWithRed:red green:green blue:blue alpha:1];
        
    } else {
        
        color = [UIColor colorWithHue:red saturation:green brightness:blue alpha:1];
        
    }
    
    CGFloat hue, saturation, brightness, alpha;
    
    NSString* result = @"";
    
    if ([color getRed:&red green:&green blue:&blue alpha:&alpha]) {
        
        result = [result stringByAppendingFormat:@"RGB:{%1.2f, %1.2f, %1.2f}", red, green, blue];
        
    } else {
        
        result = [result stringByAppendingFormat:@"RGB:{NO DATA}"];
        
    }
    
    if ([color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha]) {
        
        result = [result stringByAppendingFormat:@"\tHSV:{%1.2f, %1.2f, %1.2f}", hue, saturation, brightness];
        
    } else {
        
        result = [result stringByAppendingFormat:@"\tHSV:{NO DATA}"];
        
    }
    
    self.infoLabel.text = result;
    
    self.view.backgroundColor = color;
    
}

#pragma mark - Actions

- (IBAction)actionSlider:(UISlider *)sender {
    
    [self refreshScreen];
    
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];

    
}

- (IBAction)actionenabled:(UISwitch *)sender {
    
    self.redComponentSlider.enabled = sender.isOn;
    self.greenComponentSlider.enabled = sender.isOn;
    self.blueComponentSlider.enabled = sender.isOn;

}
@end
