//
//  LDSSchwarzenegger.h
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSSwimmer.h"
#import "LDSRunner.h"
#import "LDSJumper.h"

@interface LDSSchwarzenegger : NSObject

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) CGFloat maxHeight;
@property (assign, nonatomic) CGFloat jumperWeight;
@property (assign, nonatomic) CGFloat swimmerSpeed;
@property (assign, nonatomic) CGFloat swimmerWeight;
@property (assign, nonatomic) CGFloat runnerSpeed;
@property (assign, nonatomic) CGFloat runnerWeight;


-(void) run;
-(void) swim;
-(void) jump;
-(void) relax;
-(void) visitGym;

@end
