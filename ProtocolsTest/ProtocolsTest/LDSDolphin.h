//
//  LDSDolphin.h
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSSwimmer.h"

@interface LDSDolphin : NSObject

-(void) swim;

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) CGFloat swimmerSpeed;
@property (assign, nonatomic) CGFloat swimmerWeight;

@end
