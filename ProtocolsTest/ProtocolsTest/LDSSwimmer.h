//
//  LDSSwimmer.h
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LDSSwimmer <NSObject>

@property (assign, nonatomic) CGFloat swimmerSpeed;
@property (assign, nonatomic) CGFloat swimmerWeight;

-(void) swim;

@optional
-(void) relax;

@end
