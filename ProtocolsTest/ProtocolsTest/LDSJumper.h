//
//  LDSJumper.h
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LDSJumper <NSObject>

@property (assign, nonatomic) CGFloat maxHeight;
@property (assign, nonatomic) CGFloat jumperWeight;

-(void) jump;
-(void) relax;

@optional
-(void) visitGym;

@end
