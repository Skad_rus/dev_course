//
//  LDSCheetah.h
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSRunner.h"

@interface LDSCheetah : NSObject <LDSRunner>

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) CGFloat runnerSpeed;

-(void) run;
-(void) relax;

@end
