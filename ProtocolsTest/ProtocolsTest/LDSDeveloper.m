//
//  LDSDeveloper.m
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDeveloper.h"

@implementation LDSDeveloper

-(void) work{
    
}

#pragma mark - LDSPatient

-(BOOL) areYouOK{
    
    BOOL ok = arc4random() %2;
    
    NSLog(@"Is developer %@ ok? %@", self.name, ok ? @"YES" : @"NO");
    
    return ok;
}
-(void) takePill{
    NSLog(@"Developer %@ takes a pill", self.name);
}
-(void) makeShot{
    NSLog(@"Developer %@ makes a shot", self.name);
}


-(NSString*) howIsYourJob{
    return @"My job is awsome!";
}




-(void) swim{
    NSLog(@"Student is swimming!");
}

@end
