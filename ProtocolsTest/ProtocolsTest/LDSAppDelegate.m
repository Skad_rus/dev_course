//
//  LDSAppDelegate.m
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSPatient.h"
#import "LDSStudent.h"
#import "LDSDancer.h"
#import "LDSDeveloper.h"
#import "LDSJumper.h"
#import "LDSRunner.h"
#import "LDSSwimmer.h"
#import "LDSKangaroo.h"
#import "LDSCheetah.h"
#import "LDSDolphin.h"
#import "LDSKoala.h"
#import "LDSSchwarzenegger.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    LDSDancer* dancer1 = [[LDSDancer alloc] init];
    LDSDancer* dancer2 = [[LDSDancer alloc] init];
    
    LDSStudent* student1 = [[LDSStudent alloc] init];
    LDSStudent* student2 = [[LDSStudent alloc] init];
    LDSStudent* student3 = [[LDSStudent alloc] init];
    
    LDSDeveloper* developer1 = [[LDSDeveloper alloc] init];
    
    LDSSchwarzenegger* arnold = [[LDSSchwarzenegger alloc] init];    
    
    dancer1.name = @"dancer 1";
    dancer2.name = @"dancer 2";
    
    student1.name = @"student 1";
    student2.name = @"student 2";
    student3.name = @"student 3";
    
    developer1.name = @"developer 1";
/*
    NSObject* fake = [[NSObject alloc] init];
    
    NSArray* patients = [NSArray arrayWithObjects: fake, dancer1, student3,
                         developer1, dancer2, student1, student2, nil];
    
    for (id <LDSPatient> patient in patients) {
        
        if ([patient conformsToProtocol:@protocol(LDSPatient)]) {
            NSLog(@"Patient name = %@", patient.name);
            
            if ([patient respondsToSelector:@selector(howIsYourFamily)]) {
                NSLog(@"How is your family? \n%@", [patient howIsYourFamily]);
            }
            
            if ([patient respondsToSelector:@selector(howIsYourJob)]) {
                NSLog(@"How is your job? \n%@", [patient howIsYourJob]);
            }
            
            if (![patient areYouOK]) {
                
                [patient takePill];
                
                if (![patient areYouOK]) {
                    
                    [patient makeShot];
                }
            }
            
            
        }
        else{
            NSLog(@"Fake!!!");
        }
        
        
        
    }
    */
    
    LDSKangaroo* kangaroo1 = [[LDSKangaroo alloc] init];
    LDSKangaroo* kangaroo2 = [[LDSKangaroo alloc] init];
    
    LDSCheetah* cheetah1 = [[LDSCheetah alloc] init];
    LDSCheetah* cheetah2 = [[LDSCheetah alloc] init];
    
    LDSDolphin* dolphin1 = [[LDSDolphin alloc] init];
    LDSDolphin* dolphin2 = [[LDSDolphin alloc] init];
    
    LDSKoala* koala1 = [[LDSKoala alloc] init];
    
    kangaroo1.MaxHeight = 7.8f;
    kangaroo2.MaxHeight = 6.5f;
    
    kangaroo1.jumperWeight = 56.2;
    kangaroo2.jumperWeight = 58.2;

    
    cheetah1.runnerSpeed = 54;
    cheetah2.runnerSpeed = 64.3f;
    
    dolphin1.swimmerSpeed = 14;
    dolphin2.swimmerSpeed = 94;
    
    dolphin1.swimmerWeight = 65;
    dolphin2.swimmerWeight = 95;
    
    developer1.swimmerSpeed = 32.4f;
    developer1.swimmerWeight = 64;
    
    student1.runnerSpeed = 43.2f;
    student2.runnerSpeed = 77.2f;
    student3.runnerSpeed = 45.2f;
    
    dancer1.maxHeight = 1.3f;
    dancer2.maxHeight = 4.8f;
    
    dancer1.jumperWeight = 43;
    dancer2.jumperWeight = 57.3f;
    
    
    kangaroo1.name = @"Kangaroo1";
    kangaroo2.name = @"Kangaroo2";
    
    dolphin1.name = @"Dolphin1";
    dolphin2.name = @"Dolphin2";
    
    cheetah1.name = @"Cheetah1";
    cheetah2.name = @"Cheetah2";
    
    koala1.name = @"Koala1";
    
    arnold.name = @"Arnold";
    arnold.maxHeight = 100.1f;
    arnold.jumperWeight =  100.1f;
    arnold.swimmerSpeed =  100.1f;
    arnold.swimmerWeight =  100.1f;
    arnold.runnerSpeed =  100.1f;
    arnold.runnerWeight =  100.1f;
    
    NSArray* array = [NSArray arrayWithObjects:kangaroo1, kangaroo2, cheetah1, arnold, cheetah2, dolphin1, dolphin2, koala1, dancer1, dancer2, student1, student2, student3, developer1, nil];
    
    for (id obj in array) {

        NSLog(@"%@", [obj name]);
        
        if ([obj respondsToSelector:@selector(swim)]) {
            [obj swim];
            NSLog(@"Swimmer speed is %f",[obj swimmerSpeed]);
            NSLog(@"Swimmer weight is %f",[obj swimmerWeight]);
            if ([obj respondsToSelector:@selector(relax)]) {
                [obj relax];
            }
        }
        if ([obj respondsToSelector:@selector(run)]) {
            [obj run];
            NSLog(@"Runner speed is %f", [obj runnerSpeed]);
            if ([obj respondsToSelector:@selector(relax)]) {
                [obj relax];
            }
        }
        if ([obj respondsToSelector:@selector(jump)]) {
            [obj jump];
            NSLog(@"Jumper maximum jump height is %f", [obj maxHeight]);
            NSLog(@"Jumper weight is %f", [obj jumperWeight]);
            if ([obj respondsToSelector:@selector(visitGym)]) {
                [obj visitGym];
            }
        }
        if (![obj respondsToSelector:@selector(swim)] &&
            ![obj respondsToSelector:@selector(run)] &&
            ![obj respondsToSelector:@selector(jump)]) {
            NSLog(@"Slacker detected ;-)");
        }
        
        NSLog(@" ");
        
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
