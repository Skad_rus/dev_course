//
//  LDSSchwarzenegger.m
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSSchwarzenegger.h"

@implementation LDSSchwarzenegger

-(void) run{
    NSLog(@"Arnold is running!");
}

-(void) swim{
    NSLog(@"Arnold is swimming!");
}

-(void) jump{
    NSLog(@"Arnold is jumping!");
}

-(void) relax{
    NSLog(@"Arnold never rests!");
}

-(void) visitGym{
    NSLog(@"Arnold is visiting gym!");
}

@end
