//
//  LDSPatient.h
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LDSPatient <NSObject>

@property (strong, nonatomic) NSString* name;

//@required
-(BOOL) areYouOK;
-(void) takePill;
-(void) makeShot;

@optional

-(NSString*) howIsYourFamily;
-(NSString*) howIsYourJob;

@end
