//
//  LDSDancer.m
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDancer.h"

@implementation LDSDancer

-(void) dance{
    
}

#pragma mark - LDSPatient

-(BOOL) areYouOK{
    
    BOOL ok = arc4random() %2;
    
    NSLog(@"Is dancer %@ ok? %@", self.name, ok ? @"YES" : @"NO");
    
    return ok;
}
-(void) takePill{
    NSLog(@"Dancer %@ takes a pill", self.name);
}
-(void) makeShot{
    NSLog(@"Dancer %@ makes a shot", self.name);
}



-(void) jump{
    NSLog(@"Dancer is jumping!");
}
-(void) relax{
    NSLog(@"Dancer is relaxing... again.");
}
-(void) visitGym{
    NSLog(@"Dancer is visiting gym, wow!");
}


@end
