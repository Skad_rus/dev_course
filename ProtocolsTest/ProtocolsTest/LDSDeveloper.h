//
//  LDSDeveloper.h
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSPatient.h"
#import "LDSSwimmer.h"

@interface LDSDeveloper : NSObject <LDSPatient>

@property (strong, nonatomic) NSString* name;

@property (assign, nonatomic) CGFloat* expirience;

-(void) work;

-(BOOL) areYouOK;
-(void) takePill;
-(void) makeShot;


@property (assign, nonatomic) CGFloat swimmerSpeed;
@property (assign, nonatomic) CGFloat swimmerWeight;

-(void) swim;


@end
