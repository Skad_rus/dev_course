//
//  LDSStudent.h
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSPatient.h"
#import "LDSRunner.h"

@interface LDSStudent : NSObject <LDSPatient>

@property (strong, nonatomic) NSString* universityName;
@property (strong, nonatomic) NSString* name;

-(void) study;

-(BOOL) areYouOK;
-(void) takePill;
-(void) makeShot;


@property (assign, nonatomic) CGFloat runnerSpeed;

-(void) run;
-(void) relax;


@end
