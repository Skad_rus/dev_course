//
//  LDSRunner.h
//  ProtocolsTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LDSRunner <NSObject>

@property (assign, nonatomic) CGFloat runnerSpeed;

@optional
@property (assign, nonatomic) CGFloat runnerWeight;

@required
-(void) run;

@optional
-(void) relax;

@end
