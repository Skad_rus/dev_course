//
//  LDSDancer.h
//  ProtocolsTest
//
//  Created by Admin on 23.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDSPatient.h"
#import "LDSJumper.h"

@interface LDSDancer : NSObject <LDSPatient>

@property (strong, nonatomic) NSString* favouriteDance;
@property (strong, nonatomic) NSString* name;

-(void) dance;

-(BOOL) areYouOK;
-(void) takePill;
-(void) makeShot;


@property (assign, nonatomic) CGFloat maxHeight;
@property (assign, nonatomic) CGFloat jumperWeight;

-(void) jump;
-(void) relax;
-(void) visitGym;

@end
