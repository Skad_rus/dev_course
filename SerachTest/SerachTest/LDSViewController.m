//
//  LDSViewController.m
//  SerachTest
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"
#import "NSString+Random.h"
#import "LDSSection.h"
#import "LDSStudent.h"

typedef enum {
    
    LDSModeDayOfBirth,
    LDSModeFirstName,
    LDSModeLastName
    
} LDSMode;

@interface LDSViewController ()

@property (strong, nonatomic) NSArray* namesArray;
@property (strong, nonatomic) NSArray* sectionsArray;
@property (strong, nonatomic) NSOperation* currentOperation;

@property (strong, nonatomic) NSArray* studentsArray;

@end


@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //Задаем массив студентов
    NSMutableArray* array = [NSMutableArray array];
    
    //for (int i = 0; i < 200000; i++) {
    for (int i = 0; i < 100; i++) {
        
        LDSStudent* student = [[LDSStudent alloc] init];
        [array addObject:student];

    }

    //Сортировка массива
    
    NSArray* sortedArray = [NSArray array];
    
    switch (self.searchBar.selectedScopeButtonIndex) {
        case LDSModeDayOfBirth:
            
            sortedArray = [array sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                
                if (![obj1.studentDayOfBirth isEqualToDate:obj2.studentDayOfBirth]) {
                    
                    NSDateFormatter* monthOfDateFormatter = [[NSDateFormatter alloc] init];
                    [monthOfDateFormatter setDateFormat:@"MMM"];
                    
                    NSString* month1Str = [monthOfDateFormatter stringFromDate:obj1.studentDayOfBirth];
                    NSDate* month1Date = [monthOfDateFormatter dateFromString: month1Str];
                    
                    NSString* month2Str = [monthOfDateFormatter stringFromDate:obj2.studentDayOfBirth];
                    NSDate* month2Date = [monthOfDateFormatter dateFromString: month2Str];
                    
                    return [month1Date compare:month2Date];
                    
                } else if (![obj1.studentFirstName isEqualToString: obj2.studentFirstName]) {
                    
                    return [obj1.studentFirstName compare:obj2.studentFirstName];
                    
                } else {
                    
                    return [obj1.studentLastName compare:obj2.studentLastName];
                    
                }
                
            }];
            
            break;
            
        case LDSModeFirstName:
            
            sortedArray = [array sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                
                if (![obj1.studentFirstName isEqualToString: obj2.studentFirstName]) {
                    
                    return [obj1.studentFirstName compare:obj2.studentFirstName];
                    
                } else if (![obj1.studentLastName isEqualToString: obj2.studentLastName]) {
                    
                    return [obj1.studentLastName compare:obj2.studentLastName];
                    
                } else {
                    
                    NSDateFormatter* monthOfDateFormatter = [[NSDateFormatter alloc] init];
                    [monthOfDateFormatter setDateFormat:@"MMM"];
                    
                    NSString* month1Str = [monthOfDateFormatter stringFromDate:obj1.studentDayOfBirth];
                    NSDate* month1Date = [monthOfDateFormatter dateFromString: month1Str];
                    
                    NSString* month2Str = [monthOfDateFormatter stringFromDate:obj2.studentDayOfBirth];
                    NSDate* month2Date = [monthOfDateFormatter dateFromString: month2Str];
                    
                    return [month1Date compare:month2Date];
                    
                }
                
            }];
            
            break;
            
        case LDSModeLastName:
            
            sortedArray = [array sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                
                if (![obj1.studentLastName isEqualToString: obj2.studentLastName]) {
                    
                    return [obj1.studentLastName compare:obj2.studentLastName];
                    
                } else if (![obj1.studentFirstName isEqualToString: obj2.studentFirstName]) {
                    
                    return [obj1.studentFirstName compare:obj2.studentFirstName];
                    
                } else {
                    
                    NSDateFormatter* monthOfDateFormatter = [[NSDateFormatter alloc] init];
                    [monthOfDateFormatter setDateFormat:@"MMM"];
                    
                    NSString* month1Str = [monthOfDateFormatter stringFromDate:obj1.studentDayOfBirth];
                    NSDate* month1Date = [monthOfDateFormatter dateFromString: month1Str];
                    
                    NSString* month2Str = [monthOfDateFormatter stringFromDate:obj2.studentDayOfBirth];
                    NSDate* month2Date = [monthOfDateFormatter dateFromString: month2Str];
                    
                    return [month1Date compare:month2Date];
                    
                }
                
            }];
            
            break;
    }
    
    
    self.studentsArray = sortedArray;
    
    //Распечатка массива
    
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy MMM dd"];
     
     for (LDSStudent* student in sortedArray) {
     
     NSLog(@"Student #%d first name = %@",student.i , student.studentFirstName);
     NSLog(@"Student #%d last name = %@",student.i , student.studentLastName);
     NSLog(@"Student #%d day of birth = %@",student.i , [dateFormatter stringFromDate:student.studentDayOfBirth]);
     NSLog(@" ");
     
     }

    [self generateSectionsInBackgroungFromArray:self.studentsArray withFiler:self.searchBar.text];

}

- (void) generateSectionsInBackgroungFromArray:(NSArray*) array withFiler:(NSString*) filterString   {
    
    [self.currentOperation cancel];
    
    __weak LDSViewController* weakSelf = self;
    
    self.currentOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSArray* sectionsArray = [weakSelf generateSectionsFromArray:array withFiler:filterString];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            weakSelf.sectionsArray = sectionsArray;
            [weakSelf.tableView reloadData];
            
            self.currentOperation = nil;
            
        });
        
    }];
    
    [self.currentOperation start];
    
}

- (NSArray*) generateSectionsFromArray:(NSArray*) array withFiler:(NSString*) filterString {
    
    NSMutableArray* sectionsArray = [NSMutableArray array];
    
    NSString* currentChosenParamString = nil;
    
    for (LDSStudent* student in array) {
        
        NSString* fullName = [[NSString stringWithFormat:@"%@ %@", student.studentLastName, student.studentFirstName] lowercaseString];
        
        if ([filterString length] > 0 && [fullName rangeOfString:filterString].location == NSNotFound) {
            
            continue;
            
        }
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM"];
        
        NSString* chosenParamString;
        
        switch (self.searchBar.selectedScopeButtonIndex) {
                
            case LDSModeDayOfBirth:
                
                chosenParamString = [dateFormatter stringFromDate:student.studentDayOfBirth];
                
                break;
                
            case LDSModeFirstName:
                
                chosenParamString = [student.studentFirstName substringToIndex:1];
                
                break;
                
            case LDSModeLastName:
                
                chosenParamString = [student.studentLastName substringToIndex:1];
                
                break;
                
        }
        
        LDSSection* section = nil;
        
        if (![currentChosenParamString isEqualToString:chosenParamString]) {
            
            section = [[LDSSection alloc] init];
            section.sectionName = chosenParamString;
            section.itemsArray = [NSMutableArray array];
            currentChosenParamString = chosenParamString;
            [sectionsArray addObject:section];
            
        } else {
            
            section = [sectionsArray lastObject];
            
        }
        
        [section.itemsArray addObject: student];
        
    }
    
    //Сортировка внутри секции по имени и фамилии
    for (LDSSection* section in sectionsArray) {
        
        NSArray *sortedArray = [NSArray array];
        //
        switch (self.searchBar.selectedScopeButtonIndex) {
                
            case LDSModeDayOfBirth:
                
                sortedArray = [section.itemsArray sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                    
                    if (![obj1.studentLastName isEqualToString: obj2.studentLastName]) {
                        
                        return [obj1.studentLastName compare:obj2.studentLastName];
                        
                    }
                    
                    else {
                        
                        return [obj1.studentFirstName compare:obj2.studentFirstName];
                        
                    }
                    
                }];
                
                break;
                
            case LDSModeFirstName:
                
                sortedArray = [section.itemsArray sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                    
                    if (![obj1.studentLastName isEqualToString: obj2.studentLastName]) {
                        
                        return [obj1.studentLastName compare:obj2.studentLastName];
                        
                    }
                    
                    else {
                        
                        return [obj1.studentDayOfBirth compare:obj2.studentDayOfBirth];
                        
                    }
                    
                }];
                
                break;
                
            case LDSModeLastName:
                
                sortedArray = [section.itemsArray sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                    
                    if (![obj1.studentDayOfBirth isEqualToDate: obj2.studentDayOfBirth]) {
                        
                        return [obj1.studentDayOfBirth compare:obj2.studentDayOfBirth];
                        
                    }
                    
                    else {
                        
                        return [obj1.studentFirstName compare:obj2.studentFirstName];
                        
                    }
                    
                }];
                
                break;
                
        }
        //
        
        section.itemsArray = [NSMutableArray arrayWithArray:sortedArray];
        
    }
    
    return sectionsArray;

}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    LDSSection* sec = [self.sectionsArray objectAtIndex:section];
    
    return [sec.itemsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* identifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        
    }
    
    LDSSection* section = [self.sectionsArray objectAtIndex:indexPath.section];
    
    LDSStudent* student =[section.itemsArray objectAtIndex:indexPath.row];
    
    NSString* fullName = [NSString stringWithFormat:@"%@ %@",student.studentLastName ,student.studentFirstName];
    
    cell.textLabel.text = fullName;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy MM dd"];
    
    cell.detailTextLabel.text = [dateFormatter stringFromDate:student.studentDayOfBirth];
    
    return cell;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return [self.sectionsArray count];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return [[self.sectionsArray objectAtIndex:section] sectionName];
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    NSMutableArray* array = [NSMutableArray array];
    
    for (LDSSection* section in self.sectionsArray) {
        
        [array addObject:section.sectionName];
        
    }
    
    return array;
    
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    
    NSLog(@"selectedScopeButtonIndexDidChange %d", selectedScope);
    
    NSArray* sortedArray = [NSArray array];
    
    switch (self.searchBar.selectedScopeButtonIndex) {
        case LDSModeDayOfBirth:
            
            sortedArray = [self.studentsArray sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                
                if (![obj1.studentDayOfBirth isEqualToDate:obj2.studentDayOfBirth]) {
                    
                    NSDateFormatter* monthOfDateFormatter = [[NSDateFormatter alloc] init];
                    [monthOfDateFormatter setDateFormat:@"MMM"];
                    
                    NSString* month1Str = [monthOfDateFormatter stringFromDate:obj1.studentDayOfBirth];
                    NSDate* month1Date = [monthOfDateFormatter dateFromString: month1Str];
                    
                    NSString* month2Str = [monthOfDateFormatter stringFromDate:obj2.studentDayOfBirth];
                    NSDate* month2Date = [monthOfDateFormatter dateFromString: month2Str];
                    
                    return [month1Date compare:month2Date];
                    
                } else if (![obj1.studentFirstName isEqualToString: obj2.studentFirstName]) {
                    
                    return [obj1.studentFirstName compare:obj2.studentFirstName];
                    
                } else {
                    
                    return [obj1.studentLastName compare:obj2.studentLastName];
                    
                }
                
            }];
            
            break;
            
        case LDSModeFirstName:
            
            sortedArray = [self.studentsArray sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                
                if (![obj1.studentFirstName isEqualToString: obj2.studentFirstName]) {
                    
                    return [obj1.studentFirstName compare:obj2.studentFirstName];
                    
                } else if (![obj1.studentLastName isEqualToString: obj2.studentLastName]) {
                    
                    return [obj1.studentLastName compare:obj2.studentLastName];
                    
                } else {
                    
                    NSDateFormatter* monthOfDateFormatter = [[NSDateFormatter alloc] init];
                    [monthOfDateFormatter setDateFormat:@"MMM"];
                    
                    NSString* month1Str = [monthOfDateFormatter stringFromDate:obj1.studentDayOfBirth];
                    NSDate* month1Date = [monthOfDateFormatter dateFromString: month1Str];
                    
                    NSString* month2Str = [monthOfDateFormatter stringFromDate:obj2.studentDayOfBirth];
                    NSDate* month2Date = [monthOfDateFormatter dateFromString: month2Str];
                    
                    return [month1Date compare:month2Date];
                    
                }
                
            }];
            
            break;
            
        case LDSModeLastName:
            
            sortedArray = [self.studentsArray sortedArrayUsingComparator:^(LDSStudent* obj1, LDSStudent* obj2) {
                
                if (![obj1.studentLastName isEqualToString: obj2.studentLastName]) {
                    
                    return [obj1.studentLastName compare:obj2.studentLastName];
                    
                } else if (![obj1.studentFirstName isEqualToString: obj2.studentFirstName]) {
                    
                    return [obj1.studentFirstName compare:obj2.studentFirstName];
                    
                } else {
                    
                    NSDateFormatter* monthOfDateFormatter = [[NSDateFormatter alloc] init];
                    [monthOfDateFormatter setDateFormat:@"MMM"];
                    
                    NSString* month1Str = [monthOfDateFormatter stringFromDate:obj1.studentDayOfBirth];
                    NSDate* month1Date = [monthOfDateFormatter dateFromString: month1Str];
                    
                    NSString* month2Str = [monthOfDateFormatter stringFromDate:obj2.studentDayOfBirth];
                    NSDate* month2Date = [monthOfDateFormatter dateFromString: month2Str];
                    
                    return [month1Date compare:month2Date];
                    
                }
                
            }];
            
            break;
    }
    
    self.studentsArray = sortedArray;
    
    [self generateSectionsInBackgroungFromArray:self.studentsArray withFiler:self.searchBar.text];
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSLog(@"textDidChange %@", searchText);
    
    [self generateSectionsInBackgroungFromArray:self.studentsArray withFiler:self.searchBar.text];

}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
}

@end
