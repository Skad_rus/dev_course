//
//  LDSViewController.h
//  SerachTest
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UITableViewController <UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UISearchBar* searchBar;

@end
