//
//  LDSStudent.h
//  SerachTest
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Random.h"

@interface LDSStudent : NSObject

@property (strong, nonatomic) NSString* studentFirstName;
@property (strong, nonatomic) NSString* studentLastName;
@property (strong, nonatomic) NSDate* studentDayOfBirth;
@property (strong, nonatomic) NSMutableArray* itemsArray;
@property (assign, nonatomic) NSInteger i;

@end
