//
//  LDSStudent.m
//  SerachTest
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSStudent.h"

@implementation LDSStudent

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.studentFirstName = [[NSString randomAlphanumericString] capitalizedString];
        self.studentLastName = [[NSString randomAlphanumericString] capitalizedString];
        self.studentDayOfBirth = [self generateRandomDateWithinDaysBeforeToday:(365 * 56)];
        
        static int i = 0;
        self.i = i;
        i++;
        
    }
    return self;
}

- (NSDate *) generateRandomDateWithinDaysBeforeToday:(NSInteger)days
{
    int r1 = arc4random_uniform(days);
    int r2 = arc4random_uniform(23);
    int r3 = arc4random_uniform(59);
    
    NSDate *today = [NSDate new];
    NSCalendar *gregorian =
    [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *offsetComponents = [NSDateComponents new];
    [offsetComponents setDay:(r1*-1)];
    [offsetComponents setHour:r2];
    [offsetComponents setMinute:r3];
    
    NSDate *rndDate1 = [gregorian dateByAddingComponents:offsetComponents
                                                  toDate:today options:0];
    
    return rndDate1;
}

@end
