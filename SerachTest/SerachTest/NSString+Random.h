//
//  NSString+Random.h
//  SerachTest
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Random)

+ (NSString *)randomAlphanumericString;
+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length;
+ (NSString *)randomName;

@end
