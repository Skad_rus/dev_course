//
//  NSString+Random.m
//  SerachTest
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "NSString+Random.h"

@implementation NSString (Random)

+ (NSString *)randomName{
    
    int firstNameLength = arc4random() % 6 + 3;
    int lastNameLength = arc4random() % 6 + 3;
    
    NSString* firstName = [[self randomAlphanumericStringWithLength:firstNameLength] capitalizedString];
    NSString* lastName = [[self randomAlphanumericStringWithLength:lastNameLength] capitalizedString];
    
    return [NSString stringWithFormat:@"%@ %@", lastName, firstName];
    
}

+ (NSString *)randomAlphanumericString {
    
    int length = arc4random() % 4 + 3;
    
    return [self randomAlphanumericStringWithLength:length];
}

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyz";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }
    
    return randomString;
}

@end
