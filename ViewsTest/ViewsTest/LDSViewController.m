//
//  LDSViewController.m
//  ViewsTest
//
//  Created by Admin on 22.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//
 
#import "LDSViewController.h"

typedef enum {
    UIViewTypeBlackCell,
    UIViewTypeRedChecker,
    UIViewTypeYellowChecker
} UIViewType;

@interface LDSViewController ()
@property (weak, nonatomic) UIView* testView;
@property (copy, nonatomic) NSMutableArray* mutArrayCopy;
@property (strong, nonatomic) UIView* boardView;
@property (copy, nonatomic) NSMutableArray* mutArrayForRedСheckers;
@property (copy, nonatomic) NSMutableArray* mutArrayForYellowСheckers;
@property (copy, nonatomic) NSMutableArray* mutArrayForСheckersRects;

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    /*
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIView* view1 = [[UIView alloc] initWithFrame:CGRectMake(100, 150, 200, 50)];
    view1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
    [self.view addSubview:view1];
    
    UIView* view2 = [[UIView alloc] initWithFrame:CGRectMake(80, 130, 50, 250)];
    view2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
    view2.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:view2];
    
    self.testView = view2;
    
    [self.view bringSubviewToFront:view1];
     */
    
    /*
     Ученик
     
     1. В цикле создавайте квадратные UIView с черным фоном и расположите их в виде шахматной доски
     2. доска должна иметь столько клеток, как и настоящая шахматная
     
     Студент
     
     3. Доска должна быть вписана в максимально возможный квадрат, т.е. либо бока, либо верх или низ должны касаться границ экрана
     4. Применяя соответствующие маски сделайте так, чтобы когда устройство меняет ориентацию, то все клетки растягивались соответственно и ничего не вылетало за пределы экрана.
     
     Мастер
     5. При повороте устройства все черные клетки должны менять цвет :)
     6.Сделайте так, чтобы доска при поворотах всегда строго находилась по центру
     
     Супермен
     8. Поставьте белые и красные шашки (квадратные вьюхи) так как они стоят на доске. Они должны быть сабвьюхами главной вьюхи (у них и у клеток один супервью)
     9. После каждого переворота шашки должны быть перетасованы используя соответствующие методы иерархии UIView

    */
    
    
    [super viewDidLoad];
    
    //Задание квадратной вью по центру, для удобства
    UIView* view1 = [[UIView alloc] initWithFrame:CGRectMake( 0, [self requiredYCoordinate:self.view], [self comparisonForViewSides:self.view], [self comparisonForViewSides:self.view])];
    [self.view addSubview:view1];
    view1.autoresizingMask =    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
                                UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    self.boardView = view1;
    //
    
    NSMutableArray* mutArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < 4; i++) {
        
        NSInteger k =0;
        
        for (NSInteger j = 0; j < 8; j++) {
            
            CGFloat requiredCoordinate = self.boardView .bounds.size.height / 8;
            
            UIView* blackRectView = [[UIView alloc] init];
            
            if (j % 2 == 0) {
                
                k = requiredCoordinate;
                
            } else {
                
                k = 0;
                
            }
            
            
            blackRectView = [self createRectView:CGRectMake(requiredCoordinate * i * 2 + k,[self requiredYCoordinate:self.boardView ] + requiredCoordinate * j,requiredCoordinate, requiredCoordinate) colorViewRect:[UIColor blackColor] andParentView:self.boardView ];
            blackRectView.tag = UIViewTypeBlackCell;
            
            [mutArray addObject:blackRectView];
        }
        
        
    }
    
    self.mutArrayCopy = mutArray;

    
    [self addCheckers];
    


    
}

#pragma mark - addCheckers

//Метод добавления шашачек
- (void) addCheckers{
    
    NSMutableArray* mutArrChRed = [[NSMutableArray alloc ] init];
    NSMutableArray* mutArrChYellow = [[NSMutableArray alloc ] init];
    NSMutableArray* mutArrChRect = [[NSMutableArray alloc ] init];
    
    for (NSInteger i = 0; i < 8; i++) {
        
        for (NSInteger j = 0; j < 8; j++) {
            
            if (j < 3 || j > 4 ) {
                
                CGFloat requiredCoordinate = [self comparisonForViewSides:self.boardView] / 8;
                
                UIView* checkerView = [[UIView alloc] init];
                
                CGRect rectChecker = CGRectMake(requiredCoordinate * i + 8, [self requiredYCoordinate:self.boardView] + requiredCoordinate * j + 8, requiredCoordinate - 16, requiredCoordinate - 16);
                
                if (j < 3) {
                    
                    checkerView = [self createRectView:rectChecker colorViewRect:[UIColor redColor] andParentView:self.boardView];
                    
                    NSNumber* redCheckerIndex = [NSNumber numberWithInt:[self.boardView.subviews indexOfObject:checkerView]];
                    [mutArrChRed addObject:redCheckerIndex];
                    //[mutArrChRed addObject:checkerView];
                    checkerView.tag = UIViewTypeRedChecker;
                    
                } else {
                    
                    checkerView = [self createRectView:rectChecker colorViewRect:[UIColor yellowColor] andParentView:self.boardView];
                    
                    NSNumber* yellowCheckerIndex = [NSNumber numberWithInt:[self.boardView.subviews indexOfObject:checkerView]];
                    [mutArrChYellow addObject:yellowCheckerIndex];
                    //[mutArrChYellow addObject:checkerView];
                    checkerView.tag = UIViewTypeYellowChecker;

                }
                

                NSValue *rectValue = [NSValue valueWithCGRect:rectChecker];
                [mutArrChRect addObject:rectValue];
                
            }
            
        }
        
        
    }

    self.mutArrayForRedСheckers = mutArrChRed;
    self.mutArrayForYellowСheckers = mutArrChYellow;

    self.mutArrayForСheckersRects = mutArrChRect;

    
}

//Метод для задания случайного флоат от 0.f до 1.f
- (CGFloat) randomFromZeroToOne {
    
    return (float)(arc4random() % 256) / 255;
    
}

//Метод для задания произвольного цвета
- (UIColor*) randomColor {
    
    CGFloat r = [self randomFromZeroToOne];
    CGFloat g = [self randomFromZeroToOne];
    CGFloat b = [self randomFromZeroToOne];
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

//Метод создания квадрата на вью
- (UIView*) createRectView:(CGRect) viewRect colorViewRect:(UIColor*) viewColor andParentView:(UIView*) parentView{
    
    UIView* localView = [[UIView alloc] initWithFrame:viewRect];
    localView.backgroundColor = viewColor;
    [parentView addSubview:localView];
    
    localView.autoresizingMask =    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
                                    UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    
    return localView;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}

//Метод поиска координаты для установки квадрата в середину по oY
- (CGFloat) requiredYCoordinate:(UIView*) requiredView{
    
    if (requiredView.bounds.size.height > requiredView.bounds.size.width) {

        return (requiredView.bounds.size.height - requiredView.bounds.size.width) / 2;
        
    } else {
        
        return (requiredView.bounds.size.width - requiredView.bounds.size.height) / 2;
        
    }
    
}

//Метод, возвращающий меньшее из ширины / высоты
- (CGFloat) comparisonForViewSides:(UIView*) requiredView{
    
    if (requiredView.bounds.size.height > requiredView.bounds.size.width) {
        
        return requiredView.bounds.size.width;
        
    } else {
        
        return requiredView.bounds.size.height;
        
    }
    
    
}


#pragma mark - didRotateFromInterfaceOrientation
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    /*
    //NSLog(@"\nframe = %@\nbounds = %@", NSStringFromCGRect(self.testView.frame), NSStringFromCGRect(self.testView.bounds));
    NSLog(@"\nframe = %@\nbounds = %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
    
    
    CGPoint origin = CGPointZero;
    
    origin = [self.view convertPoint:origin fromView:self.view.window];
    
    NSLog(@"origin = %@", NSStringFromCGPoint(origin));
    
    CGRect r = self.view.bounds;
    r.origin.y = 0;
    r.origin.x = CGRectGetWidth(r) - 100;
    r.size = CGSizeMake(100, 100);
    
    UIView* v = [[UIView alloc] initWithFrame:r];
    v.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent: 0.8];
    [self.view addSubview:v];
    */
    
    //Уровень мастер (смена цвета)
    
    /*
    UIColor* randomColor = [self randomColor];
    
    for (UIView* viev in self.mutArrayCopy) {
        
        viev.backgroundColor = randomColor;
        
    }
    */
    
    //Уровень супермен перетасовка
    for (UIView* obj in self.boardView.subviews) {
        
        if (obj.tag == UIViewTypeRedChecker || obj.tag == UIViewTypeYellowChecker) {
            
            NSInteger currentChekerIndex = [self.boardView.subviews indexOfObject:obj];
            
            //Выбираем рандомную шашечку, например, из красных
            NSInteger rndArrayIndex = arc4random() % [self.mutArrayForRedСheckers count];
            NSInteger rndCheckerIndex = [[self.mutArrayForRedСheckers objectAtIndex:rndArrayIndex] integerValue];
            NSLog(@"rndArrayIndex = %d, rndCheckerIndex = %d", rndCheckerIndex, rndCheckerIndex);
            
            UIView* vievRedChecker = [self.boardView.subviews objectAtIndex:rndCheckerIndex];
            
            //Меняем местами
            CGRect tempFrame = obj.frame;
            obj.frame = CGRectMake(vievRedChecker.frame.origin.x, vievRedChecker.frame.origin.y,
                                   vievRedChecker.frame.size.width, vievRedChecker.frame.size.height);
            
            vievRedChecker.frame = CGRectMake(tempFrame.origin.x, tempFrame.origin.y,
                                              tempFrame.size.width, tempFrame.size.height);
            
            [self.boardView exchangeSubviewAtIndex:currentChekerIndex withSubviewAtIndex:rndCheckerIndex];
        }
     
    }
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
