//
//  LDSObject.m
//  ParpmetersTest
//
//  Created by Admin on 04.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSObject.h"

@implementation LDSObject

- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"object is created");
    }
    return self;
}

- (void) dealloc{
    NSLog(@"object is deallocated");
}

@end
