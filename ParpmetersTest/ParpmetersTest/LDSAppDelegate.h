//
//  LDSAppDelegate.h
//  ParpmetersTest
//
//  Created by Admin on 04.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LDSObject;

@interface LDSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LDSObject* object;

@end
