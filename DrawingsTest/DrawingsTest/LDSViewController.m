//
//  LDSViewController.m
//  DrawingsTest
//
//  Created by Admin on 08.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"
#import "LDSDrawingView.h"

@interface LDSViewController ()

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIPanGestureRecognizer* panGesture =
    [[UIPanGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handlePan:)];
    
    [self.view addGestureRecognizer:panGesture];

}

#pragma mark - Gesture Pan

- (void) handlePan:(UIPanGestureRecognizer*) panGesture {
    
    NSLog(@"handlePan");
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake([panGesture locationInView:self.view].x, [panGesture locationInView:self.view].y, 10, 10)];
    view.backgroundColor = [UIColor brownColor];
    [self.view addSubview:view];

    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark - Orientation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    [self.drawingView setNeedsDisplay];
    
}

@end
