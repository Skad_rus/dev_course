//
//  LDSDrawingView.m
//  DrawingsTest
//
//  Created by Admin on 08.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSDrawingView.h"


@implementation LDSDrawingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    /*
    NSLog(@"drawRect %@", NSStringFromCGRect(rect));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Рисуем квадраты
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    
    CGRect square1 = CGRectMake(100, 100, 100, 100);
    CGRect square2 = CGRectMake(200, 200, 100, 100);
    CGRect square3 = CGRectMake(300, 300, 100, 100);
    
    CGContextAddRect(context, square1);
    CGContextAddRect(context, square2);
    CGContextAddRect(context, square3);
    
    CGContextStrokePath(context);
    
    //Рисуем круги
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    
    CGContextAddEllipseInRect(context, square1);
    CGContextAddEllipseInRect(context, square2);
    CGContextAddEllipseInRect(context, square3);
    
    CGContextFillPath(context);
    
    //Рисуем линии
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGContextSetLineWidth(context, 1.f);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextMoveToPoint(context, CGRectGetMinX(square1), CGRectGetMaxY(square1));
    CGContextAddLineToPoint(context, CGRectGetMinX(square3), CGRectGetMaxY(square3));

    CGContextMoveToPoint(context, CGRectGetMaxX(square3), CGRectGetMinY(square3));
    CGContextAddLineToPoint(context, CGRectGetMaxX(square1), CGRectGetMinY(square1));
    
    CGContextStrokePath(context);
    
    //Рисуем секторы
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    CGContextMoveToPoint(context, CGRectGetMinX(square1), CGRectGetMaxY(square1));
    CGContextAddArc(context, CGRectGetMaxX(square1), CGRectGetMaxY(square1), CGRectGetWidth(square1), M_PI, 0, YES);
    
    CGContextMoveToPoint(context, CGRectGetMaxX(square3), CGRectGetMaxY(square3));
    CGContextAddArc(context, CGRectGetMinX(square3), CGRectGetMinY(square3), CGRectGetWidth(square3), 0, M_PI, YES);
    
    CGContextStrokePath(context);
    
    //Пишем текст
    NSString* text = @"test";
    
    UIFont* font = [UIFont systemFontOfSize:14.f];
    
    NSShadow* shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeMake(1, 1);
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowBlurRadius = 0.5;
    
    NSDictionary* attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor grayColor],    NSForegroundColorAttributeName,
                                font,                   NSFontAttributeName,
                                shadow,                 NSShadowAttributeName,
                                nil];
    
    CGSize textSize = [text sizeWithAttributes:attributes];
    
    CGRect textRect = CGRectMake(CGRectGetMidX(square2) - textSize.width / 2,
                                 CGRectGetMidY(square2) - textSize.height / 2,
                                 textSize.width,
                                 textSize.height);
    
    textRect = CGRectIntegral(textRect);
    
    [text drawInRect:textRect withAttributes:attributes];
    //[text drawAtPoint:CGPointMake(CGRectGetMidX(square2) - textSize.width / 2, CGRectGetMidY(square2) - textSize.height / 2) withAttributes:attributes];
    */
    //Домашнее задание
    
    /*
     Ученик.
     
     1. Нарисуйте пятиконечную звезду :)
     2. Добавьте окружности на концах звезды
     3. Соедините окружности линиями
     
     Студент.
     
     4. Закрасте звезду любым цветом цветом оО
     5. При каждой перерисовке рисуйте пять таких звезд (только мелких) в рандомных точках экрана
     
     Мастер
     
     6. После того как вы попрактиковались со звездами нарисуйте что-то свое, проявите творчество :)
     
     Супермен
     
     7. Сделайте простую рисовалку - веду пальцем по экрану и рисую :)
    */
    
    [self makeStudent];

}


- (void) makeStudent{
    
    for (int i = 0; i < 5; i++) {
        
        NSInteger randX = 60 + arc4random() % 650;
        NSInteger randY = 60 + arc4random() % 650;
        
        [self drawStar:CGPointMake(randX, randY)];
        
    }
    
}

- (void) drawStar:(CGPoint) atPoint{
    
    
    //Рисуем звезду
    //CGPoint     startPoint = CGPointMake(100, 100);
    CGPoint     startPoint = atPoint;
    NSInteger   starRadius = 50;
    CGFloat     starAngle = M_PI * 2 / 5;
    
    CGPoint point1;
    CGPoint point2;
    CGPoint point3;
    CGPoint point4;
    CGPoint point5;
    
    NSValue* value1 = [NSValue valueWithCGPoint:point1];
    NSValue* value2 = [NSValue valueWithCGPoint:point2];
    NSValue* value3 = [NSValue valueWithCGPoint:point3];
    NSValue* value4 = [NSValue valueWithCGPoint:point4];
    NSValue* value5 = [NSValue valueWithCGPoint:point5];
    
    NSMutableArray* edgesPoints = [[NSMutableArray alloc] initWithObjects: value1, value2, value3, value4, value5, nil];
    
    for (int i = 0; i < 5; i++) {
        
        CGPoint     varPoint = CGPointMake( startPoint.x + starRadius * sin(starAngle * i), startPoint.y - starRadius * cos(starAngle * i) );
        
        NSValue*    varValue = [NSValue valueWithCGPoint:varPoint];
        [edgesPoints replaceObjectAtIndex:i withObject:varValue];
        
        NSLog(@"#%d = %@", i, NSStringFromCGPoint(varPoint));
        
    }
    //CGPoint newPoint = [val CGPointValue];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGContextSetLineWidth(context, 1.f);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextMoveToPoint(context, [[edgesPoints objectAtIndex:0] CGPointValue].x, [[edgesPoints objectAtIndex:0] CGPointValue].y);
    CGContextAddLineToPoint(context, [[edgesPoints objectAtIndex:3] CGPointValue].x, [[edgesPoints objectAtIndex:3] CGPointValue].y);
    CGContextAddLineToPoint(context, [[edgesPoints objectAtIndex:1] CGPointValue].x, [[edgesPoints objectAtIndex:1] CGPointValue].y);
    CGContextAddLineToPoint(context, [[edgesPoints objectAtIndex:4] CGPointValue].x, [[edgesPoints objectAtIndex:4] CGPointValue].y);
    CGContextAddLineToPoint(context, [[edgesPoints objectAtIndex:2] CGPointValue].x, [[edgesPoints objectAtIndex:2] CGPointValue].y);
    CGContextAddLineToPoint(context, [[edgesPoints objectAtIndex:0] CGPointValue].x, [[edgesPoints objectAtIndex:0] CGPointValue].y);
    
    CGContextFillPath(context);
    
    //Соединяем концы
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    CGContextMoveToPoint(context, [[edgesPoints objectAtIndex:4] CGPointValue].x, [[edgesPoints objectAtIndex:4] CGPointValue].y);
    
    for (int i = 0; i < 5; i++) {
        
        CGContextAddLineToPoint(context, [[edgesPoints objectAtIndex:i] CGPointValue].x, [[edgesPoints objectAtIndex:i] CGPointValue].y);
        
    }
    
    CGContextStrokePath(context);
    
    //Рисуем круги
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    
    for (NSValue* varValue in edgesPoints) {
        
        CGRect ellipseRect = CGRectMake([varValue CGPointValue].x - starRadius / 8,
                                        [varValue CGPointValue].y - starRadius / 8,
                                        starRadius / 4,
                                        starRadius / 4);
        
        CGContextAddEllipseInRect(context, ellipseRect);
        
    }
    
    CGContextFillPath(context);
    
    
}


@end
