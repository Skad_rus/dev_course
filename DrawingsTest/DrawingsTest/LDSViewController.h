//
//  LDSViewController.h
//  DrawingsTest
//
//  Created by Admin on 08.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LDSDrawingView;

@interface LDSViewController : UIViewController

//@property (weak, nonatomic) UIView* testImageView;
@property (weak, nonatomic) IBOutlet LDSDrawingView* drawingView;

@end
