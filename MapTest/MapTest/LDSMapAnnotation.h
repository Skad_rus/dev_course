//
//  LDSMapAnnotation.h
//  MapTest
//
//  Created by Admin on 05.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>


@interface LDSMapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end