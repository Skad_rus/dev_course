//
//  LDSViewController.h
//  MapTest
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MKMapView;

@interface LDSViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView* mapView;

@end
