//
//  LDTableViewController.h
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDTableViewController : UITableViewController

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *firstNameOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *lastNameOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *ageOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *birthDayOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *graduationOutletCollection;

- (IBAction)firstNameAction:(UITextField *)sender;
- (IBAction)lastNameAction:(UITextField *)sender;
- (IBAction)ageAction:(UITextField *)sender;
- (IBAction)birthDayAction:(UITextField *)sender;

@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *ageField;
@property (weak, nonatomic) IBOutlet UITextField *birthDayField;
@property (weak, nonatomic) IBOutlet UITextField *graduationField;

@end
