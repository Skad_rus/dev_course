//
//  LDDatePickerViewController.h
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
Подсказка. Вам надо сделать контроллер с дейт пикером, а дейт пикер это наследник от UIControl, то есть у него есть акшин valueChanged или типо того. У контроллера нужно создать проперти делегат, по которому мы будем отправлять данные, полученные с барабана. То есть по простому: контроллер следит за барабаном и отправляет изменения своему делегату. Не забудьте установить делегат перед создания поповера.
*/

#import <UIKit/UIKit.h>
#import "LDDatePickerDelegate.h"

@interface LDDatePickerViewController : UIViewController

@property (weak,nonatomic) id <LDDatePickerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong,nonatomic) NSDate *date;

@property (strong, nonatomic) NSString* testString;

- (IBAction)valueChangedAction:(UIDatePicker *)sender;

@end

