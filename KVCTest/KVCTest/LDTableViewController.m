//
//  LDTableViewController.m
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDTableViewController.h"
#import "LDStudent.h"
#import "LDGroup.h"
#import "LDDatePickerViewController.h"
#import "LDDatePickerDelegate.h"

@interface LDTableViewController () <UITableViewDelegate, UITextFieldDelegate, LDDatePickerDelegate>

@property (strong, nonatomic) LDDatePickerViewController* datePickerController;
@property (strong, nonatomic) NSArray* groups;
@property (strong, nonatomic) NSArray* students;
@property (strong, nonatomic) LDStudent* student;

@end

@implementation LDTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
     LDStudent* student = [[LDStudent alloc] init];
     
     [student addObserver:self
     forKeyPath:@"name"
     options:NSKeyValueObservingOptionNew |
     NSKeyValueObservingOptionOld
     context:nil];
     
     student.name = @"Alex";
     student.age = 20;
     
     NSLog(@"%@", student);
     
     [student setValue:@"Roger" forKey:@"name"];
     [student setValue:@25 forKey:@"age"];
     
     NSLog(@"name1 = %@, name2 = %@",student.name, [student valueForKey:@"name"]);
     
     NSLog(@"%@", student);
     
     [student changeName];
     
     NSLog(@"%@", student);
     
     self.student = student;
     */
    
    /*
     LDStudent* student1 = [[LDStudent alloc] init];
     
     student1.name = @"Alex";
     student1.age = 20;
     
     LDStudent* student2 = [[LDStudent alloc] init];
     
     student2.name = @"Roger";
     student2.age = 25;
     
     LDStudent* student3 = [[LDStudent alloc] init];
     
     student3.name = @"Jack";
     student3.age = 22;
     
     LDStudent* student4 = [[LDStudent alloc] init];
     
     student4.name = @"Vova";
     student4.age = 28;
     
     LDGroup* group1 = [[LDGroup alloc] init];
     group1.students = @[student1, student2, student3, student4];
     
     NSLog(@"%@", group1.students);
     
     NSMutableArray* array = [group1 mutableArrayValueForKey:@"students"];
     
     [array removeLastObject];
     
     NSLog(@"%@", array);
     */
    
    /*
     LDStudent* student1 = [[LDStudent alloc] init];
     
     student1.name = @"Alex";
     student1.age = 20;
     
     LDStudent* student2 = [[LDStudent alloc] init];
     
     student2.name = @"Roger";
     student2.age = 25;
     
     LDStudent* student3 = [[LDStudent alloc] init];
     
     student3.name = @"Jack";
     student3.age = 22;
     
     LDStudent* student4 = [[LDStudent alloc] init];
     
     student4.name = @"Vova";
     student4.age = 28;
     
     self.student = student1;
     
     //NSLog(@"%@",[self valueForKeyPath:@"student.friend.name"]);
     
     NSLog(@"name = %@",[self valueForKeyPath:@"student.name"]);
     
     //NSString* name = @"Alex111";
     NSNumber* name = @6;
     
     NSError* error = nil;
     
     if (![self.student validateValue:&name forKey:@"name" error:&error]) {
     
     NSLog(@"%@", error);
     
     }
     */
    /*
     
     6. Создайте несколько студентов и положите их в массив, но обсервер оставьте только на одном из них
     7. У студентов сделайте weak проперти "friend". Сделайте цепочку из нескольких студентов, чтобы один был друг второму, второй третьему, тот четвертому, а тот первому :)
     8. Используя метод setValue: forKeyPath: начните с одного студента (не того, что с обсервером) и переходите на его друга, меняя ему проперти, потом из того же студента на друга его друга и тд (то есть путь для последнего студента получится очень длинный)
     9. Убедитесь что на каком-то из друзей, когда меняется какой-то проперти, срабатывает ваш обсервер
     
    */
    
    LDStudent* student1 = [[LDStudent alloc] init];
    
    student1.firstName = @"Alex";
    student1.age = 20;
    
    LDStudent* student2 = [[LDStudent alloc] init];
    
    student2.firstName = @"Roger";
    student2.age = 25;
    
    LDStudent* student3 = [[LDStudent alloc] init];
    
    student3.firstName = @"Jack";
    student3.age = 22;
    
    LDStudent* student4 = [[LDStudent alloc] init];
    
    student4.firstName = @"Vova";
    student4.age = 28;
    
    LDGroup* group1 = [[LDGroup alloc] init];
    group1.students = @[student1, student2, student3, student4];
    
    LDStudent* student5 = [[LDStudent alloc] init];
    
    student5.firstName = @"Vasya";
    student5.age = 18;
    
    LDStudent* student6 = [[LDStudent alloc] init];
    
    student6.firstName = @"Kolya";
    student6.age = 24;
    
    LDGroup* group2 = [[LDGroup alloc] init];
    group2.students = @[student5, student6];
    
    student1.friend = student2;
    student2.friend = student3;
    student3.friend = student4;
    student4.friend = student5;
    student5.friend = student6;
    student6.friend = student1;
    
    self.student = student3;
    
    [self.student addObserver:self
              forKeyPath:@"friend"
                 options:NSKeyValueObservingOptionNew |
     NSKeyValueObservingOptionOld
                 context:nil];
    
    
    
    
    self.groups = @[group1, group2];
    
    NSLog(@"groups count %@", [self valueForKeyPath:@"groups.@count"]);
    
    NSArray* allStudents = [self.groups valueForKeyPath:@"@distinctUnionOfArrays.students"];
    
    int i = 0;
    
    for (i = 0; i < 100; i++) {
        
        NSInteger randOne = arc4random() % [allStudents count];
        NSInteger randTwo = arc4random() % [allStudents count];
        
        while (randTwo == randOne) {
            randTwo = arc4random() % [allStudents count];
        }
        
        LDStudent* studentOne = [allStudents objectAtIndex:randOne];
        LDStudent* studentTwo = [allStudents objectAtIndex:randTwo];
        
        [studentOne setValue:studentTwo forKeyPath:@"friend"];
        //student.friend = [allStudents objectAtIndex:randTwo];

    }
    
    NSLog(@"allStudents = %@", allStudents);
    
    NSNumber* minAge = [allStudents valueForKeyPath:@"@min.age"];
    NSNumber* maxAge = [allStudents valueForKeyPath:@"@max.age"];
    NSNumber* sumAge = [allStudents valueForKeyPath:@"@sum.age"];
    NSNumber* avgAge = [allStudents valueForKeyPath:@"@avg.age"];
    
    NSLog(@"minAge = %@", minAge);
    NSLog(@"maxAge = %@", maxAge);
    NSLog(@"sumAge = %@", sumAge);
    NSLog(@"avgAge = %@", avgAge);
    
    NSArray* allNames = [allStudents valueForKeyPath:@"@distinctUnionOfObjects.firstName"];
    
    NSLog(@"allNames = %@", allNames);
    
    ////////////////////////
    
    self.students = allStudents;
    
}

- (void) dealloc {
    [self.student removeObserver:self forKeyPath:@"name"];
}

#pragma mark - Observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    NSLog(@"\nobserveValueForKeyPath %@\n of object: %@\nchange: %@", keyPath, object, change);
    
    //id value = [change objectForKey:NSKeyValueChangeNewKey];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.firstNameField]) {
        [self.lastNameField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if ([textField isEqual:self.birthDayField] || [textField isEqual:self.graduationField]) {
        
        [textField resignFirstResponder];
        
    }
    
}

#pragma mark - LDDatePickerDelegate

- (void) nowChangeTextOnThis:(NSString*) dateString{
    
    NSLog(@"nowChangeTextOnThis");
    
    self.birthDayField.text = dateString;
    
}

#pragma mark - UITableViewDataSource

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return [self.students count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 5;
    
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString* title = [[self.students objectAtIndex:section] name];
    
    return title;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        if (indexPath.row == 0) {
            
            cell.textLabel.text = [self.students objectAtIndex:indexPath.section];
            
        }
        
    }

    return cell;
}
*/

#pragma mark - Segue


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LDDatePickerViewController"]) {
        
        self.datePickerController = segue.destinationViewController;
        self.datePickerController.testString = @"testString";
        self.datePickerController.delegate = self;
        NSLog(@"%@", self.datePickerController.delegate);
    
    }
}


#pragma mark - Actions

- (IBAction)firstNameAction:(UITextField *)sender {
    
    NSLog(@"firstNameAction");
    
}

- (IBAction)lastNameAction:(UITextField *)sender {
    
    NSLog(@"lastNameAction");
    

}

- (IBAction)ageAction:(UITextField *)sender {
    
    [sender resignFirstResponder];
    
    NSLog(@"ageAction");

}

- (IBAction)birthDayAction:(UITextField *)sender {
    
    NSLog(@"birthDayAction");

}

@end
