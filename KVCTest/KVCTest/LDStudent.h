//
//  LDStudent.h
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDStudent : NSObject

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (assign, nonatomic) NSInteger age;
@property (strong, nonatomic) NSDate * dateOfBirth;
@property (strong, nonatomic) NSString* graduation;
@property (weak, nonatomic) LDStudent* friend;
//- (void) changeName;

@end
