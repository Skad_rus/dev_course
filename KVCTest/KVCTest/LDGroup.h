//
//  LDGroup.h
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDGroup : NSObject

@property (strong, nonatomic) NSArray* students;

@end
