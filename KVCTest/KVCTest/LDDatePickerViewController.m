//
//  LDDatePickerViewController.m
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDDatePickerViewController.h"

@interface LDDatePickerViewController ()

@end

@implementation LDDatePickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
        /*
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd yyyy"];
        
        //self.datePicker.date = [formatter dateFromString:self.textField.text];
        */
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%@", self.testString);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (IBAction)valueChangedAction:(UIDatePicker *)sender {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd yyyy"];
    
    [self.delegate nowChangeTextOnThis:[formatter stringFromDate:sender.date]];
    
}

/*
#pragma mark - LDDatePickerDelegate

- (void) nowChangeTextOnThis:(NSString*) dateString{
    
    NSLog(@"1");
    
}
*/

@end
