//
//  LDDatePickerDelegate.h
//  KVCTest
//
//  Created by Admin on 09.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LDDatePickerDelegate <NSObject>

- (void) nowChangeTextOnThis:(NSString*) dateString;

@end
