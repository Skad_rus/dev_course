//
//  LDStudent.m
//  KVCTest
//
//  Created by Admin on 08.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDStudent.h"

@implementation LDStudent

- (void) setFirstName:(NSString *)firstName {
    
    _firstName = firstName;
    
    NSLog(@"student setName: %@", firstName);
    
}

- (void) setAge:(NSInteger)age {
    
    _age = age;
    
    NSLog(@"student setAge: %d", age);
    
}

- (NSString*) description {
    
    return [NSString stringWithFormat:@"student: %@ %d", self.firstName, self.age];
    
}

- (void) setValue:(id)value forKey:(NSString *)key {
    
    NSLog(@"student setValue: %@ forKey: %@", value, key);
    
    [super setValue:value forKey:key];
    
}

- (void) setValue:(id)value forUndefinedKey:(NSString *)key {
    
    NSLog(@"setValueForUndefinedKey");
    
}

- (id) valueForUndefinedKey:(NSString *)key{
    
    NSLog(@"valueForUndefinedKey");
    
    return nil;

}

- (void) changeName{
    
    [self willChangeValueForKey:@"firstName"];
    
    _firstName = @"FakeName";
    
    [self didChangeValueForKey:@"firstName"];
    
}

/*
- (BOOL)validateValue:(inout id *)ioValue forKey:(NSString *)inKey error:(out NSError **)outError{
    
    if ([inKey isEqualToString:@"name"]) {
        
        NSString* newName = *ioValue;
        
        if (![newName isKindOfClass:[NSString class]]) {
            
            *outError = [[NSError alloc] initWithDomain:@"Not NSString" code:123 userInfo:nil];
            
            return NO;
            
        }
        
        if ([newName rangeOfString:@"1"].location != NSNotFound) {
            
            *outError = [[NSError alloc] initWithDomain:@"Has numbers" code:324 userInfo:nil];
            
            return NO;
            
        }
    }
    
    return YES;
    
}
*/

/*
- (BOOL) validateName:(inout id *)ioValue error:(out NSError **)outError{
    
    NSLog(@"AAAAA");
    
    return YES;
    
}*/

@end
