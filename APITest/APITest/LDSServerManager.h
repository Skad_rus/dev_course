//
//  LDSServerManager.h
//  APITest
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LDSUser;

@interface LDSServerManager : NSObject

+ (LDSServerManager*) sharedManager;

- (void) getFriendsWithOffset:(NSInteger) offset
                        count:(NSInteger) count
                    onSuccess:(void(^)(NSArray* friends)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) getUsersInfo:(NSString*) userID
            onSuccess:(void(^)(LDSUser* user)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

@end
