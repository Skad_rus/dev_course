//
//  LDSUser.h
//  APITest
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSUser : NSObject

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSURL* imageURL;
@property (strong, nonatomic) NSString* userID;
@property (assign, nonatomic) NSInteger sex;
@property (strong, nonatomic) NSURL* photo200;
//@property (strong, nonatomic) NSString* bdate;

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@end
