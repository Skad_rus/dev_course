//
//  LDSServerManager.m
//  APITest
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSServerManager.h"
#import "AFNetworking.h"
#import "LDSUser.h"

@interface LDSServerManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager* requestOperationManager;

@end

@implementation LDSServerManager

+ (LDSServerManager*) sharedManager {
    
    static LDSServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        manager = [[LDSServerManager alloc] init];
    
    });
    
    return manager;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSURL* url = [NSURL URLWithString:@"https://api.vk.com/method/"];
        
        self.requestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
        
    }
    
    return self;
    
}

- (void) getFriendsWithOffset:(NSInteger) offset
                       count:(NSInteger) count
                   onSuccess:(void(^)(NSArray* friends)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failture {
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"11375499",   @"user_id",
                            @"name",       @"order",
                            @(count),      @"count",
                            @(offset),     @"offset",
                            @"photo_50",   @"fields",
                            @"nom",        @"name_case", nil];
    
    
    [self.requestOperationManager
     GET:@"friends.get"
     parameters:params
     success:^(AFHTTPRequestOperation *operation, NSDictionary* responseObject) {
        NSLog(@"JSON: %@", responseObject);
         
         NSArray* dictArray = [responseObject objectForKey:@"response"];
         
         NSMutableArray* objectsArray = [NSMutableArray array];
         
         for (NSDictionary* dict in dictArray) {
             LDSUser* user = [[LDSUser alloc] initWithServerResponse:dict];
             [objectsArray addObject:user];
         }
         
         if (success) {
             success(objectsArray);
         }
        
    }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
         
         if (failture) {
             failture(error, operation.response.statusCode);
         }
         
    }];

}

- (void) getUsersInfo:(NSString*) userID
            onSuccess:(void(^)(LDSUser* user)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    
    NSString* fields = @"sex, bdate, city, country, photo_50, photo_200";
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            userID,     @"user_ids",
                            fields,     @"fields",
                            @"nom",     @"name_case", nil];
    
    [self.requestOperationManager
     GET:@"users.get"
     parameters:params
     success:^(AFHTTPRequestOperation *operation, NSDictionary* responseObject) {
         NSLog(@"JSON: %@", responseObject);
         
         NSArray* dictArray = [responseObject objectForKey:@"response"];
         NSDictionary* dict = [dictArray lastObject];
         
         LDSUser* user = [[LDSUser alloc] initWithServerResponse:dict];
         
         if (success) {
             success(user);
         }
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
         if (failure) {
             failure(error, operation.response.statusCode);
         }
        
     }];
    
}

@end
