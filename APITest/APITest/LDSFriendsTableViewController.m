//
//  LDSFriendsTableViewController.m
//  APITest
//
//  Created by Admin on 24.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSFriendsTableViewController.h"
#import "LDSUser.h"
#import "LDSServerManager.h"
#import "UIImageView+AFNetworking.h"

@interface LDSFriendsTableViewController ()

@property (strong, nonatomic) NSMutableArray* friendsArray;
@property (strong, nonatomic) LDSUser* user;

@end

@implementation LDSFriendsTableViewController

- (id)initWithStyle:(UITableViewStyle)style forUser:(NSString*) userID{
    
    self = [super initWithStyle:style];

    if (self) {
        self.friendsID = userID;
        [self getUsersInfo];
    }
    
    NSLog(@"%@", self.friendsID);
    
    
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self getUsersInfo];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = [NSString stringWithFormat:@"ID %@", self.friendsID];
    self.friendsArray = [NSMutableArray array];
    
    //[self getUsersInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - API

- (void) getUsersInfo {
    
    [[LDSServerManager sharedManager]
     
     getUsersInfo:self.friendsID
     onSuccess:^(LDSUser* user) {
         self.user = user;
         
         [self.tableView reloadData];
         
    }
     onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
     }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier = @"Cell";
    
    
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    
    switch (indexPath.row) {
            
        case 0: {
            
            NSURLRequest* request = [NSURLRequest requestWithURL:self.user.photo200];
            
            __weak UITableViewCell* weakCell = cell;
            
            cell.imageView.image = nil;
            
            [cell.imageView
             setImageWithURLRequest:request
             placeholderImage:nil
             success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                 weakCell.imageView.image = image;
                 [weakCell layoutSubviews];
             } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                 
             }];
        }
            break;

        case 1:
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", self.user.firstName, self.user.lastName];
            break;
            
        case 2:
            cell.textLabel.text = @"Birth date & sex";
            break;
            
        case 3:
            cell.textLabel.text = @"Country, sity";
            break;
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return 120;
    } else {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    if (indexPath.row == [self.friendsArray count]) {
        [self getFriendsFromServer];
    }  else {
        
        LDSFriendsTableViewController* vc = [[LDSFriendsTableViewController alloc] initWithStyle:UITableViewStyleGrouped forUser:[[self.friendsArray objectAtIndex:indexPath.row] userID]];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    */
    
}

@end
