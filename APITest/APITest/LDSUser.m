//
//  LDSUser.m
//  APITest
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSUser.h"

@implementation LDSUser

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        self.userID = [responseObject objectForKey:@"user_id"];
        
        NSString* urlString = [responseObject objectForKey:@"photo_50"];
        
        if (urlString) {
            self.imageURL = [NSURL URLWithString:urlString];
        }

        self.sex = (int)[responseObject objectForKey:@"sex"];
        
        urlString = [responseObject objectForKey:@"photo_200"];
        
        if (urlString) {
            self.photo200 = [NSURL URLWithString:urlString];
        }
        
    }
    return self;
}

@end
