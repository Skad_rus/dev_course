//
//  LDSViewController.m
//  APITest
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"
#import "LDSServerManager.h"
#import "LDSUser.h"
#import "UIImageView+AFNetworking.h"
#import "LDSFriendsTableViewController.h"

@interface LDSViewController ()

@property (strong, nonatomic) NSMutableArray* friendsArray;

@end

@implementation LDSViewController

static NSInteger friendsInRequest = 20;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.friendsArray = [NSMutableArray array];
    
    [self getFriendsFromServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - API

- (void) getFriendsFromServer {
    
    [[LDSServerManager sharedManager]
     getFriendsWithOffset:[self.friendsArray count]
                    count:friendsInRequest
                onSuccess:^(NSArray *friends) {
                    
                    [self.friendsArray addObjectsFromArray:friends];
                                                    
                    NSMutableArray* newPath = [NSMutableArray array];
                                                    
                    for (int i = (int)[self.friendsArray count] - (int)[friends count]; i < [self.friendsArray count]; i++) {
                        [newPath addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                                                    
                    [self.tableView beginUpdates];
                                                    
                    [self.tableView insertRowsAtIndexPaths:newPath withRowAnimation:UITableViewRowAnimationTop];
                                                    
                    [self.tableView endUpdates];
                                                    
                    }
                    onFailure:^(NSError *error, NSInteger statusCode) {
                        NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                    }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.friendsArray count] + 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* identifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    if (indexPath.row == [self.friendsArray count]) {
        
        cell.textLabel.text = @"LOAD MORE";
        cell.imageView.image = nil;

    } else {
        
        LDSUser* friend = [self.friendsArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",
                               friend.firstName,
                               friend.lastName];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:friend.imageURL];
        
        __weak UITableViewCell* weakCell = cell;
        
        cell.imageView.image = nil;
        
        [cell.imageView
         setImageWithURLRequest:request
         placeholderImage:nil
         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
             weakCell.imageView.image = image;
             [weakCell layoutSubviews];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
        }];
        
    }

    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == [self.friendsArray count]) {
        [self getFriendsFromServer];
    }  else {
        
        LDSFriendsTableViewController* vc = [[LDSFriendsTableViewController alloc] initWithStyle:UITableViewStyleGrouped forUser:[[self.friendsArray objectAtIndex:indexPath.row] userID]];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}

@end
