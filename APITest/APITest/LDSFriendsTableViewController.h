//
//  LDSFriendsTableViewController.h
//  APITest
//
//  Created by Admin on 24.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSFriendsTableViewController : UITableViewController

@property (strong, nonatomic) NSString* friendsID;

- (id)initWithStyle:(UITableViewStyle)style forUser:(NSString*) userID;

@end
