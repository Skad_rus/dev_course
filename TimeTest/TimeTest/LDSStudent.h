//
//  LDSStudent.h
//  TimeTest
//
//  Created by Admin on 17.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSStudent : NSObject

@property (strong, nonatomic) NSDate* dayOfBirth;
@property (weak, nonatomic) NSString* name;
@property (weak, nonatomic) NSString* lastName;

@end
