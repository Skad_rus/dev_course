//
//  LDSStudent.m
//  TimeTest
//
//  Created by Admin on 17.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSStudent.h"

@implementation LDSStudent

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSInteger randomNumber = arc4random() % 10;
        
        switch (randomNumber)
        {
            case 0:
                self.name = @"Ivan";
                break;
            case 1:
                self.name = @"Fritz";
                break;
            case 2:
                self.name = @"Tom";
                break;
            case 3:
                self.name = @"Sergey";
                break;
            case 4:
                self.name = @"Vladimir";
                break;
            case 5:
                self.name = @"Daniil";
                break;
            case 6:
                self.name = @"Aleksei";
                break;
            case 7:
                self.name = @"Denis";
                break;
            case 8:
                self.name = @"Sam";
                break;
            case 9:
                self.name = @"Dave";
                break;
        }
        
        NSInteger randomNumber2 = arc4random() % 10;
        
        switch (randomNumber2)
        {
            case 0:
                self.lastName = @"Lobanov";
                break;
            case 1:
                self.lastName = @"Knyazev";
                break;
            case 2:
                self.lastName = @"Hardy";
                break;
            case 3:
                self.lastName = @"Hitler";
                break;
            case 4:
                self.lastName = @"Putin";
                break;
            case 5:
                self.lastName = @"Lukashenko";
                break;
            case 6:
                self.lastName = @"Kalachev";
                break;
            case 7:
                self.lastName = @"Kostukevich";
                break;
            case 8:
                self.lastName = @"Dunbar";
                break;
            case 9:
                self.lastName = @"Shagal";
                break;
        }

    }
    return self;
}

@end
