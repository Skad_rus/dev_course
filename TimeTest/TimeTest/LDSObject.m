//
//  LDSObject.m
//  TimeTest
//
//  Created by Admin on 17.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSObject.h"

@interface LDSObject ()

@property (strong, nonatomic) NSTimer* timer;

@end
@implementation LDSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"LDSObject is initialized");
        
        __weak id weakSelf = self;
        
        NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:1 target:weakSelf selector:@selector(timerTest:) userInfo:nil repeats:YES];
        [timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:2]];
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"LDSObject is deallocated");
}

- (void) timerTest:(NSTimer*) timer{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss:SSS"];
    NSLog(@"%@", [dateFormatter stringFromDate:[NSDate date]]);
    
    [timer invalidate];
    
}

@end
