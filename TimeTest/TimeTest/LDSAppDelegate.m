//
//  LDSAppDelegate.m
//  TimeTest
//
//  Created by Admin on 17.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Немного практики с датами нам не помешает, так что вот задание.
 (немного повышу градус сложности)
 
 Ученик.
 
 1. Создайте класс студент у когторого будет проперти dateOfBirth (дата рождения), которая собственно NSDate.
 2. Инициализируйте NSMutableArray и в цикле создайте 30 студентов.
 3. Каждому установите дату рождения. Возраст рандомный от 16 до 50 лет.
 4. В другом цикле пройдитесь по всему массиву и выведите день рождения каждого студента в адекватном формате.
 
 Студент.
 
 5. Отсортируйте массив студентов по дате рождения, начиная от самого юного.
 6. Используя массивы имен и фамилий (по 5-10 имен и фамилий), каждому студенту установите случайное имя и случайную фамилию.
 7. Выведите отсортированных студентов: Имя, Фамилия, год рождения
 
 Мастер.
 
 10. Создайте таймер в апп делегате, который отсчитывает один день за пол секунды.
 11. Когда таймер доходит до дня рождения любого их студентов - поздравлять его с днем рождения.
 12. Выведите на экран разницу в возрасте между самым молодым и самым старым студентом (количество лет, месяцев, недель и дней)
 
 Супермен.
 
 13. Выведите на экран день недели, для каждого первого дня каждого месяца в текущем году (от начала до конца)
 14. Выведите дату (число и месяц) для каждого воскресенья в текущем году (от начала до конца)
 15. Выведите количество рабочих дней для каждого месяца в текущем году (от начала до конца)
*/

#import "LDSAppDelegate.h"
#import "LDSObject.h"
#import "LDSStudent.h"

@interface LDSAppDelegate ()

@property (strong, nonatomic) NSMutableArray* mutArrayForTimer;
@property (strong, nonatomic) NSDate* timerDate;

@end

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    /*
    NSDate* date = [NSDate date];
    
    NSLog(@"%@", [date dateByAddingTimeInterval:6]);
    NSLog(@"%@", [date dateByAddingTimeInterval:-6]);
    
    //[date compare:[date dateByAddingTimeInterval:6]];
    
    NSDate* date2 = [NSDate dateWithTimeIntervalSince1970:10.5f];
    
    NSLog(@"%@", date2);
    */
    
    /*
    NSDate* date = [NSDate date];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    
    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    

    //[dateFormatter setDateFormat:@"yyyy M MM MMM MMMM MMMMM"];
    //[dateFormatter setDateFormat:@"yyyy/MM/dd"];
    //[dateFormatter setDateFormat:@"yyyy/MM/dd EEEE hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    
    NSDate* date3 = [dateFormatter dateFromString:@"2008/05/17 15:43"];
    NSLog(@"%@", date3);
     */
    
    /*
     NSDateComponents* components =
     [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
     fromDate:date];
     
     NSLog(@"%@", components);
     
     NSInteger hours = [components hour];
     
     NSLog(@"%d", hours);
     */
    
    /*
    NSDate* date1 = [NSDate date];
    NSDate* date2 = [NSDate dateWithTimeIntervalSinceNow:-1000000];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components =
    [calendar components:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute
                fromDate:date2
                  toDate:date1
                 options:0];
    
    NSLog(@"%@", components);
    */

/*
 Ученик.
 
 1. Создайте класс студент у когторого будет проперти dateOfBirth (дата рождения), которая собственно NSDate.
 2. Инициализируйте NSMutableArray и в цикле создайте 30 студентов.
 3. Каждому установите дату рождения. Возраст рандомный от 16 до 50 лет.
 4. В другом цикле пройдитесь по всему массиву и выведите день рождения каждого студента в адекватном формате.
*/
    
    NSLog(@"______________________________________");
    NSLog(@"____________Level_pupil_______________");
    NSLog(@"______________________________________");
    
    NSMutableArray* mutArray = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 30; i++) {
        
        LDSStudent* student = [[LDSStudent alloc] init];
        
        NSDate* date = [[NSDate alloc] init];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components =
        [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                    fromDate:date];
        
        [components setYear:    2014 - 50 + arc4random() % 35];
        [components setMonth:   arc4random() % 13];
        [components setDay:     arc4random() % 32];
        
        student.dayOfBirth = [calendar dateFromComponents:components];
        
        [mutArray addObject:student];
    }
    
    NSInteger i = 1;
    
    for (LDSStudent* student in mutArray) {
        
        NSLog(@"Student #%d", i);
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy MM dd"];
        NSLog(@"%@", [dateFormatter stringFromDate:student.dayOfBirth]);
        
        i++;
    
    }
    
/*
 Студент.
 
 5. Отсортируйте массив студентов по дате рождения, начиная от самого юного.
 6. Используя массивы имен и фамилий (по 5-10 имен и фамилий), каждому студенту установите случайное имя и случайную фамилию.
 7. Выведите отсортированных студентов: Имя, Фамилия, год рождения
 
*/
 
    NSLog(@"______________________________________");
    NSLog(@"____________Level_student_____________");
    NSLog(@"______________________________________");
    
    //for (LDSStudent* student in mutArray) {
    
    NSArray *sortedArray = [mutArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSDate *first = [(LDSStudent*)a dayOfBirth];
        NSDate *second = [(LDSStudent*)b dayOfBirth];
        return [second compare:first];
    }];;
    

    
    for (LDSStudent* student in sortedArray) {
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy MM dd"];
        NSLog(@"%@ %@   %@", student.name, student.lastName, [dateFormatter stringFromDate:student.dayOfBirth]);
        
    }
    
/*
 Мастер.
 
 10. Создайте таймер в апп делегате, который отсчитывает один день за пол секунды.
 11. Когда таймер доходит до дня рождения любого из студентов - поздравлять его с днем рождения.
 12. Выведите на экран разницу в возрасте между самым молодым и самым старым студентом (количество лет, месяцев, недель и дней)
*/
 
    NSLog(@"______________________________________");
    NSLog(@"_____________Level_master_____________");
    NSLog(@"______________________________________");
    

    self.mutArrayForTimer = mutArray;    //пришлось сделать property для апп делегата чтобы работать с данными (их копией) из массива в методе timerTest
    self.timerDate = [NSDate date];      // -//-
    [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(timerTest:) userInfo:nil repeats:YES];
    
    
    NSDate* date1 = [[mutArray firstObject] dayOfBirth];
    NSDate* date2 = [[mutArray lastObject] dayOfBirth];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components =
    [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                fromDate:date2
                  toDate:date1
                 options:0];
    
    NSLog(@"Different between the birthday of the youngest and the oldest is %d year(s), %d month(s), %d week(s) and %d day(s)!", components.year, components.month, components.day / 7, components.day % 7);
    
    NSLog(@"Stop the timer for superman."); // поясняю что стоплю таймер ля уровня супермен
    
/*
 Супермен.
 
 13. Выведите на экран день недели, для каждого первого дня каждого месяца в текущем году (от начала до конца)
 14. Выведите дату (число и месяц) для каждого воскресенья в текущем году (от начала до конца)
 15. Выведите количество рабочих дней для каждого месяца в текущем году (от начала до конца)
*/
 
    NSLog(@"______________________________________");
    NSLog(@"____________Level_superman____________");
    NSLog(@"______________________________________");
    
    NSInteger countDaysSuperman = 0;
    NSDate* currentDateSuperman = [NSDate date];
    NSCalendar* calendarSuperman = [NSCalendar currentCalendar];
    NSDateFormatter* dateFormatterSuperman = [[NSDateFormatter alloc] init];
    NSDateFormatter* weekFormatterSuperman = [[NSDateFormatter alloc] init];
    
    NSDateComponents* componentsSuperman = [calendarSuperman components:NSCalendarUnitYear
                                                       fromDate:currentDateSuperman];
    
    for (NSInteger j = 1; j <= 12; j++){
        
        [componentsSuperman setDay:1];
        [componentsSuperman setMonth:j];
        
        [weekFormatterSuperman setDateFormat:@"EEEE"];
        [dateFormatterSuperman setDateStyle:NSDateFormatterShortStyle];
        [dateFormatterSuperman setDateFormat:@"dd MM yyyy"];
        
        NSLog(@"%@ was %@ week day.", [dateFormatterSuperman stringFromDate:[calendarSuperman dateFromComponents:componentsSuperman]], [weekFormatterSuperman stringFromDate:[calendarSuperman dateFromComponents:componentsSuperman]]);
    }
    
    NSDateComponents* componentsSupermanNew = [[NSDateComponents alloc] init];
    
    [componentsSupermanNew setYear:2014];
    [componentsSupermanNew setMonth:12];
    [componentsSupermanNew setDay:31];
    NSDate* dateLastSuperman = [calendarSuperman dateFromComponents:componentsSupermanNew];
    
    [componentsSupermanNew setMonth:1];
    [componentsSupermanNew setDay:1];
    NSDate* dateCurrentSuperman = [calendarSuperman dateFromComponents:componentsSupermanNew];
    
    while (![dateCurrentSuperman isEqualToDate:dateLastSuperman]) {
        
        dateCurrentSuperman = [dateCurrentSuperman dateByAddingTimeInterval:60*60*24];
        componentsSupermanNew = [calendarSuperman components:NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate:dateCurrentSuperman];
        
        if ([componentsSupermanNew weekday] == 1) {
            NSLog(@"%d sunday = %d day in %d month!", ++countDaysSuperman, [componentsSupermanNew day], [componentsSupermanNew month]);
        }
    }
    
    NSDate* dateSupermanLast = [NSDate date];
    NSCalendar* calendarSupermanLast = [NSCalendar currentCalendar];
    NSDateComponents* componentsSupermanLast = [calendarSupermanLast components:NSCalendarUnitDay | NSCalendarUnitMonth fromDate:dateSupermanLast];
    NSInteger yearSupermanLast = [componentsSupermanLast year];
    NSInteger countMonthSupermanLast = [componentsSupermanLast month];
    
    for (NSInteger month = 1; month <= countMonthSupermanLast; month++) {
        
        NSInteger weekDay = 0;
        NSInteger countWorkDays = 0;
        [componentsSupermanLast setMonth:month];
        [componentsSupermanLast setYear:yearSupermanLast];
        dateSupermanLast = [calendarSupermanLast dateFromComponents:componentsSupermanLast];
        
        NSRange monthDays = [calendarSupermanLast rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:dateSupermanLast];
        NSInteger daysInMonth = monthDays.length;
        
        for (NSInteger day = 0; day < daysInMonth; day++) {
            
            componentsSupermanLast = [calendarSupermanLast components:NSCalendarUnitDay fromDate:dateSupermanLast];
            weekDay = [componentsSupermanLast weekday];
            dateSupermanLast = [dateSupermanLast dateByAddingTimeInterval:(60*60*24)];
            
            if (weekDay != 7 && weekDay != 1) {
                
                countWorkDays++;
                
            }
            
        }
        
        NSLog(@"Month %d have %d days. %d  of them are work days.", month, daysInMonth, countWorkDays);
        
    }
    
    return YES;
}

- (void) timerTest:(NSTimer*) timer {

    self.timerDate = [NSDate dateWithTimeInterval: -60 * 60 * 24 sinceDate:self.timerDate];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy MM dd"];
    NSLog(@"%@", [dateFormatter stringFromDate:self.timerDate]);
    
    for (LDSStudent* student in self.mutArrayForTimer){
        if (![[dateFormatter stringFromDate: self.timerDate] isEqualToString: [dateFormatter stringFromDate:student.dayOfBirth]] == NSOrderedSame) {
            NSLog(@"Сongratulations %@ %@! (%@)", student.lastName, student.name, [dateFormatter stringFromDate:student.dayOfBirth]);
            
        }
    }
    
    [timer invalidate];    //чтобы лог в супермене был читаем таймер останавливаю

}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
