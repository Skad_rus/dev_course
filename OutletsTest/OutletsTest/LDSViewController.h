//
//  LDSViewController.h
//  OutletsTest
//
//  Created by Admin on 23.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *testCells;

@property (strong, nonatomic) IBOutlet UIView* boardView;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ArrChRed;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ArrChYellow;

@end