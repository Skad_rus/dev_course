//
//  main.m
//  OutletsTest
//
//  Created by Admin on 23.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LDSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LDSAppDelegate class]));
    }
}
