//
//  LDSObject.m
//  ParametersText2
//
//  Created by Admin on 06.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSObject.h"

@implementation LDSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"Object is created");
    }
    return self;
}

- (void) dealloc{
    
    NSLog(@"Object is deallocated");
    
}

- (id)copyWithZone:(NSZone *)zone;{
    return [[LDSObject alloc] init];
}

@end
