//
//  LDSAppDelegate.h
//  ParametersText2
//
//  Created by Admin on 06.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LDSObject;

@interface LDSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (copy, nonatomic, getter = getObject) LDSObject* object;

@end
