//
//  LDSViewController.m
//  TextFieldsTest
//
//  Created by Admin on 16.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"

@interface LDSViewController () <UITextFieldDelegate>

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.firstNameField.returnKeyType = UIReturnKeyNext;
    self.lastNameField.returnKeyType = UIReturnKeyDone;
    
    [self.firstNameField becomeFirstResponder];
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(notificationTextDidBeginEditing:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    [nc addObserver:self selector:@selector(notificationTextDidEndEditing:) name:UITextFieldTextDidEndEditingNotification object:nil];
    [nc addObserver:self selector:@selector(notificationTextDidChange:) name:UITextFieldTextDidChangeNotification object:nil];

}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)actionLog:(UIButton *)sender {
    
    NSLog(@"First Name = %@, Last Name = %@", self.firstNameField.text, self.lastNameField.text);
    
    if ([self.firstNameField isFirstResponder]) {
        
        [self.firstNameField resignFirstResponder];
    
    } else if ([self.lastNameField isFirstResponder]) {
        
        [self.lastNameField resignFirstResponder];
        
    }
    
}

- (IBAction)actionTextChanged:(UITextField *)sender {
    
    NSLog(@"%@", sender.text);
    
}

#pragma mark - Notifications

- (void)notificationTextDidBeginEditing:(NSNotification*) notification {
    
    NSLog(@"notificationTextDidBeginEditing");
    
}

- (void)notificationTextDidEndEditing:(NSNotification*) notification {
    
    NSLog(@"notificationTextDidEndEditing");
    
}

- (void)notificationTextDidChange:(NSNotification*) notification {
    
    NSLog(@"notificationTextDidChange");
    
}


#pragma mark - UITextFieldDelegate

/*
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return  [textField isEqual:self.firstNameField];
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    return NO;
    
}
*/

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField isEqual:self.firstNameField]) {
        
        [self.lastNameField becomeFirstResponder];
    } else {
        
        [textField resignFirstResponder];
        
    }
    
    
    return YES;
    
}

@end
