//
//  LDSViewController.m
//  TouchesTest
//
//  Created by Admin on 26.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Уровень супермен (остальных уровней не будет)
 
 1. Создайте шахматное поле (8х8), используйте черные сабвьюхи
 2. Добавьте балые и красные шашки на черные клетки (используйте начальное расположение в шашках)
 3. Реализуйте механизм драг'н'дроп подобно тому, что я сделал в примере, но с условиями:
 4. Шашки должны становиться в центр черных клеток.
 5. Даже если я отпустил шашку над центром белой клетки - она должна переместиться в центр ближайшей к отпусканию черной клетки.
 6. Шашки не могут становиться друг на друга
 7. Шашки не могут быть поставлены за пределы поля.
*/
#import "LDSViewController.h"

typedef enum {
    UIViewTypeBlackCell,
    UIViewTypeRedChecker,
    UIViewTypeYellowChecker,
    UIViewTypeBoardView,
    UIViewTypeSelfView
} UIViewType;

@interface LDSViewController ()

@property (weak, nonatomic) UIView* draggingView;
@property (assign, nonatomic) CGPoint touchOffset;

@property (weak, nonatomic) UIView* testView;
@property (strong, nonatomic) NSMutableArray* mutArrayCopy;
@property (strong, nonatomic) UIView* boardView;
@property (strong, nonatomic) NSMutableArray* mutArOfRed;
@property (strong, nonatomic) NSMutableArray* mutArOfYellow;
@property (strong, nonatomic) NSMutableArray* mutArrayForСheckersRects;
@property (strong, nonatomic) NSMutableArray* mutArrayOfRectsCenters;
@property (strong, nonatomic) NSMutableArray* mutArOfColorCellsRectsCenters;
@property (strong, nonatomic) NSMutableArray* landingPoints;


@end

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    /*Создаем восемь квадратов для драг н дроп
    for (int i = 0; i < 8; i++) {
        
        UIView* view = [[UIView alloc] initWithFrame:CGRectMake(10 + 110 * i, 100, 100, 100)];
        view.backgroundColor = [self randomColor];
        
        [self.view addSubview:view];
        
    }
    */
    
    self.view.tag = UIViewTypeSelfView;
    
    [self makeChessboard];
//    [self emptyCheckers];
    
}


#pragma mark - Draw elements

//Метод создания квадрата на вью
- (UIView*) createRectView:(CGRect) viewRect colorViewRect:(UIColor*) viewColor andParentView:(UIView*) parentView{
    
    UIView* localView = [[UIView alloc] initWithFrame:viewRect];
    localView.backgroundColor = viewColor;
    [parentView addSubview:localView];
    
    localView.autoresizingMask =    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    
    return localView;
}

//Метод, возвращающий меньшее из ширины / высоты
- (CGFloat) comparisonForViewSides:(UIView*) requiredView{
    
    if (requiredView.bounds.size.height > requiredView.bounds.size.width) {
        
        return requiredView.bounds.size.width;
        
    } else {
        
        return requiredView.bounds.size.height;
        
    }
    
    
}

//Метод поиска координаты для установки квадрата в середину по oY
- (CGFloat) requiredYCoordinate:(UIView*) requiredView{
    
    if (requiredView.bounds.size.height > requiredView.bounds.size.width) {
        
        return (requiredView.bounds.size.height - requiredView.bounds.size.width) / 2;
        
    } else {
        
        return (requiredView.bounds.size.width - requiredView.bounds.size.height) / 2;
        
    }
    
}


#pragma mark - Draw board

//Метод, задающий черные клетки доски
- (void) makeChessboard{
    
    //Задание квадратной вью по центру, для удобства
    UIView* view1 = [[UIView alloc] initWithFrame:CGRectMake( 0, [self requiredYCoordinate:self.view], [self comparisonForViewSides:self.view], [self comparisonForViewSides:self.view])];
    [self.view addSubview:view1];
    view1.autoresizingMask =    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    self.boardView = view1;
    
    self.boardView.tag = UIViewTypeBoardView;
    //
    
    NSMutableArray* mutArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i < 4; i++) {
        
        NSInteger k =0;
        
        for (NSInteger j = 0; j < 8; j++) {
            
            CGFloat requiredCoordinate = self.boardView.bounds.size.height / 8;
            
            UIView* blackRectView = [[UIView alloc] init];
            
            if (j % 2 == 0) {
                
                k = requiredCoordinate;
                
            } else {
                
                k = 0;
                
            }
            
            
            blackRectView = [self createRectView:CGRectMake(requiredCoordinate * i * 2 + k,[self requiredYCoordinate:self.boardView ] + requiredCoordinate * j,requiredCoordinate, requiredCoordinate) colorViewRect:[UIColor blackColor] andParentView:self.boardView ];
            blackRectView.tag = UIViewTypeBlackCell;
            
            [mutArray addObject:blackRectView];
        }
        
        
    }
    
    self.mutArrayCopy = mutArray;
    
    
    
    [self cellsCenters];
    [self addCheckers];
    
}

//Метод добавления шашечек
- (void) addCheckers{
    
    NSMutableArray* mutArrChRed = [[NSMutableArray alloc ] init];
    NSMutableArray* mutArrChYellow = [[NSMutableArray alloc ] init];
    NSMutableArray* mutArrChRect = [[NSMutableArray alloc ] init];
    
    
    for (NSInteger i = 0; i < 4; i++) {
        
        for (NSInteger j = 0; j < 8; j++) {
            
            NSInteger k = 0;
            
            if (j < 3 || j > 4 ) {
                
                CGFloat requiredCoordinate = [self comparisonForViewSides:self.boardView] / 8;
                
                k = requiredCoordinate;
                
                UIView* checkerView = [[UIView alloc] init];
                
                NSInteger g;
                
                if (j % 2 == 0) {
                    g = i + 1;
                }   else {
                    g = i;
                }
                
                CGRect rectChecker = CGRectMake(requiredCoordinate * i + 13 + k*g, [self requiredYCoordinate:self.boardView] + requiredCoordinate * j + 13, requiredCoordinate - 26, requiredCoordinate - 26);
                
                if (j < 3) {
                    
                    checkerView = [self createRectView:rectChecker colorViewRect:[UIColor redColor] andParentView:self.boardView];
                    
                    [mutArrChRed addObject:checkerView];

                    checkerView.tag = UIViewTypeRedChecker;
                    
                } else {
                    
                    checkerView = [self createRectView:rectChecker colorViewRect:[UIColor yellowColor] andParentView:self.boardView];
                    
                    [mutArrChYellow addObject:checkerView];

                    checkerView.tag = UIViewTypeYellowChecker;
                    
                }
                
                NSValue *rectValue = [NSValue valueWithCGRect:rectChecker];
                [mutArrChRect addObject:rectValue];
                
            }
            
        }
        
        
    }
    
    
    /*
    NSInteger i = 0;
    CGFloat requiredCoordinate = [self comparisonForViewSides:self.boardView] / 8;
    
    for (NSValue* value in self.mutArrayOfRectsCenters) {
        
        CGPoint point;
        point = [value CGPointValue];
        CGRect rect = CGRectMake(100, 100, requiredCoordinate - 26, requiredCoordinate - 26);
        rect.origin.x = point.x - (requiredCoordinate - 26)/2;
        //тут остановился
        
        
        
    }
    */
    
    self.mutArOfRed = mutArrChRed;
    self.mutArOfYellow = mutArrChYellow;
    self.mutArrayForСheckersRects = mutArrChRect;
    [self checkersCenters];
    [self emptyCheckers];
    
}//ok


#pragma mark - Calculate centers

//Метод, вычисляющий центры черных клеток доски
- (void) cellsCenters{
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    for (UIView* imgObj in self.mutArrayCopy) {
        
        CGPoint point = CGPointMake(CGRectGetMidX( [self.boardView convertRect:imgObj.frame toView:self.view] ),
                                    CGRectGetMidY( [self.boardView convertRect:imgObj.frame toView:self.view] ));
        
        //NSLog(@"%@", NSStringFromCGPoint(point));
        
        NSValue *pointValue = [NSValue valueWithCGPoint:point];
        [array addObject:pointValue];
        
    }
    
    self.mutArrayOfRectsCenters = array;
    
}//ok

//Метод, вычисляющий центры клеток, занятых шашечками
- (void) checkersCenters{
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    for (UIView* imgObj in self.mutArOfRed) {
        
        CGPoint point = CGPointMake(CGRectGetMidX( [self.boardView convertRect:imgObj.frame toView:self.view] ),
                                    CGRectGetMidY( [self.boardView convertRect:imgObj.frame toView:self.view] ));
        
        NSValue *pointValue = [NSValue valueWithCGPoint:point];
        [array addObject:pointValue];
        //NSLog(@"%@", NSStringFromCGPoint(point));
    }
    
    
    for (UIView* imgObj in self.mutArOfYellow) {
        
        CGPoint point = CGPointMake(CGRectGetMidX( [self.boardView convertRect:imgObj.frame toView:self.view] ),
                                    CGRectGetMidY( [self.boardView convertRect:imgObj.frame toView:self.view] ));
        
        NSValue *pointValue = [NSValue valueWithCGPoint:point];
        [array addObject:pointValue];
        //NSLog(@"%@", NSStringFromCGPoint(point));
    }
    
    self.mutArOfColorCellsRectsCenters = array;
    
}//ok

//Метод, вычисляющий центры пустых клеток
- (void) emptyCheckers {
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    NSMutableArray* array2 = [[NSMutableArray alloc] init];
    array = self.mutArrayOfRectsCenters;
    
    for (NSValue* blackCell in array) {
        
        NSValue* spikeValue = [[NSValue alloc] init];
        spikeValue = blackCell;
        
        for (NSValue* colorCell in self.mutArOfColorCellsRectsCenters) {
            
            if ([spikeValue isEqualToValue:colorCell]) {
                
                [array2 addObject: [NSNumber numberWithInt: [array indexOfObject:blackCell]] ];
                
            }
            
        }
        
    }
    
    for (NSInteger i = [array2 count]; i > 0; i--) {
        
        NSInteger j = [[array2 objectAtIndex:i-1] intValue];
        [array removeObjectAtIndex:j];
        
    }
    
    self.landingPoints = array;
    
    /*for (NSValue* value in array) {
        
        CGPoint point = [value CGPointValue];
        NSLog(@"emptyCheckers %@", NSStringFromCGPoint(point));
        
    }*/
    
}//ok


#pragma mark - Other calculations

//Метод, возвращающий координаты тача
- (void) logTouches:(NSSet*) touches withMethod:(NSString*) methodName{
    
    NSMutableString* string = [NSMutableString stringWithString:methodName];
    
    for (UITouch* touch in touches) {
        
        CGPoint point = [touch locationInView:self.view];
        
        [string appendFormat:@"%d %@", self.draggingView.tag, NSStringFromCGPoint(point)];
        
    }
    
    NSLog(@"%@", string);
    
}//ok

//Метод, вычисляющий расстояние между двумя CGPoint
-(float)distanceFrom:(CGPoint)point1 to:(CGPoint)point2 {
    CGFloat xDist = (point2.x - point1.x);
    CGFloat yDist = (point2.y - point1.y);
    return sqrt((xDist * xDist) + (yDist * yDist));
}//ok

//Метод, возвращающий произвольный цвет
- (UIColor*) randomColor {
    CGFloat r = (CGFloat)(arc4random() % 256) / 255.f;
    CGFloat g = (CGFloat)(arc4random() % 256) / 255.f;
    CGFloat b = (CGFloat)(arc4random() % 256) / 255.f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}//ok

//Метод, возвращающий оптимальную точку для перестновки
- (CGPoint) nearestPoint:(CGPoint) presentPoint andPossiblePoints:(NSMutableArray*) array {
    
    CGPoint point;
    //
    //presentPoint = [self.boardView convertPoint:presentPoint toView:self.view];
    //
    CGFloat bestDistance = 1448.154f;
    
    for (NSValue* value in array) {
        
        CGPoint currentPoint = [value CGPointValue];
        
        CGFloat distance = [self distanceFrom:presentPoint to:point];
        
        if (distance < bestDistance) {
            
            bestDistance = distance;
            point = currentPoint;
            
        }
        
    }
    NSLog(@" ");
    for (NSValue* value in array) {
        
        CGPoint point = [value CGPointValue];
        NSLog(@"Possible : %@", NSStringFromCGPoint(point));
        
    }
    NSLog(@" ");

    NSLog(@"Best: %@",NSStringFromCGPoint(point));

    return point;
}


#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesBegan"];
    
    UITouch* touch = [touches anyObject];
    
    CGPoint pointOnMainView = [touch locationInView:self.view];
    
    UIView* view = [self.view hitTest:pointOnMainView withEvent:event];
    
    if (view.tag == UIViewTypeRedChecker || view.tag == UIViewTypeYellowChecker ) {
        
        self.draggingView = view;
        
        [self.view bringSubviewToFront:self.draggingView];
        
        CGPoint touchPoint = [touch locationInView:self.draggingView];
        
        touchPoint = [self.draggingView convertPoint:touchPoint toView:self.view];
        
        self.touchOffset = CGPointMake(CGRectGetMidX(self.draggingView.bounds) - touchPoint.x,
                                       CGRectGetMidY(self.draggingView.bounds) - touchPoint.y);
        
        //
        //        CGPoint point = CGPointMake(CGRectGetMidX(self.draggingView.frame),CGRectGetMidY(self.draggingView.frame));

        CGPoint point = CGPointMake(CGRectGetMidX([self.boardView convertRect:self.draggingView.frame toView:self.view]),
                                    CGRectGetMidY([self.boardView convertRect:self.draggingView.frame toView:self.view]));
        // тут вроде неверно
        
        NSLog(@"Point = %@", NSStringFromCGPoint(point));
        
        
        NSValue *pointValue = [NSValue valueWithCGPoint:point];
        [self.landingPoints addObject:pointValue];
        //
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.draggingView.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
            self.draggingView.alpha = 0.3f;
            
            
        }];
        
    } else {
        
        self.draggingView = nil;
        
    }
    
    //NSLog(@"inside = %d", [self.testView pointInside:point withEvent:event]);
    
}//ok

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesMoved"];
    
    if (self.draggingView) {
        
        UITouch* touch = [touches anyObject];
        
        CGPoint pointOnView = [touch locationInView:self.boardView];
        NSLog(@"Center =  %@", NSStringFromCGPoint(pointOnView));
        
        CGPoint correction = CGPointMake(pointOnView.x + self.touchOffset.x,
                                         pointOnView.y + self.touchOffset.y);
        //CGPoint correction = CGPointMake(pointOnView.x + [self.draggingView convertPoint: self.touchOffset toView:self.view].x,
        //                                 pointOnView.y + [self.draggingView convertPoint: self.touchOffset toView:self.view].y);
        
        
        //self.draggingView.center = correction;
        self.draggingView.center = pointOnView;
        NSLog(@"correction correction correction %@", NSStringFromCGPoint(correction));
        //self.draggingView.center = [self.draggingView convertPoint:correction toView:self.view];

        
    }
    
}// ne ok kostili

- (void)onTouchesEnded {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.draggingView.transform = CGAffineTransformIdentity;
        self.draggingView.alpha = 1;
        
        CGPoint point = [self.boardView convertPoint:self.draggingView.center toView:self.view];
        
        self.draggingView.center = [self nearestPoint:point andPossiblePoints:self.landingPoints];
        
        self.draggingView.center = [self.view convertPoint:self.draggingView.center toView:self.boardView];
        
    }];
    
    self.draggingView = nil;
    
}//ok

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesEnded"];
    
    [self onTouchesEnded];

}//ok

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesCancelled"];
    
    [self onTouchesEnded];

}//ok


#pragma mark - Memory warning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
