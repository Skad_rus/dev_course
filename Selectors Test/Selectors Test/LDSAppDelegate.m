//
//  LDSAppDelegate.m
//  Selectors Test
//
//  Created by Admin on 06.05.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSObject.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //[self testMethod];
    /*SEL selector1  = @selector(testMethod);
    //SEL selector2  = @selector(testMethod:);
    //SEL selector3  = @selector(testMethod:parameter2:);
    
    [self performSelector:@selector(testMethodParameter1:) withObject:[NSNumber numberWithInt:11]];
    
    LDSObject* obj = [[LDSObject alloc] init];
    
    [self performSelector:selector1];
    [obj performSelector:selector1];
    NSString* secret = [obj performSelector:@selector(superSecretText)];
    
    NSString* a = NSStringFromSelector(selector1);
    SEL sel = NSSelectorFromString(a);
    
    NSLog(@"secret = %@",secret);
    //[self performSelector:selector2 withObject:@"test string"];
    //[self performSelector:selector3 withObject:@"string1" withObject:@"string2"];
    
    //[self performSelector:selector1 withObject:nil afterDelay:5.f];
    */
    
    //NSString* string = [self testMethodParameter1:2 parameter2:3.1f parametr3:5.2f];
    
    //NSLog(@"string = %@", string);
    
    SEL selector = @selector(testMethodParameter1: parameter2: parametr3:);
    
    NSMethodSignature* signature = [LDSAppDelegate instanceMethodSignatureForSelector:selector];
    
    NSInvocation* invocation = [NSInvocation invocationWithMethodSignature:signature];
    
    [invocation setTarget:self];
    [invocation setSelector:selector];
    
    NSInteger intVal = 2;
    CGFloat floatVal = 3.1f;
    double doubleVal = 5.5f;
    
    [invocation setArgument:&intVal atIndex:2];
    [invocation setArgument:&floatVal atIndex:3];
    [invocation setArgument:&doubleVal atIndex:4];
    
    [invocation invoke];
    
    __unsafe_unretained NSString* string = nil;
    
    [invocation getReturnValue:&string];
    
    NSLog(@"%@",string);
    
    return YES;
}

- (NSString*) testMethodParameter1:(NSInteger) intValue parameter2:(CGFloat) floatValue parametr3:(double) doubleValue{
    //NSLog(@"testMethodParameter1: %d, %f, %f", intValue, floatValue, doubleValue);
    return [NSString stringWithFormat:@"testMethodParameter1: %d, %f, %f", intValue, floatValue, doubleValue];
}

- (void) testMethodParameter1:(NSInteger) intValue{
    NSLog(@"testMethodParameter1: %d", intValue);
}

- (void) testMethod: (NSString*) string parameter2:(NSString*) string2 {
    NSLog(@"testMethod: %@ parameter2: %@", string, string2);
}

- (void) testMethod: (NSString*) string {
    NSLog(@"testMethod: %@", string);
}

- (void) testMethod {
    NSLog(@"testMethod");
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
