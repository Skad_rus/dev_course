//
//  LDSChild.h
//  ArraysTest
//
//  Created by Admin on 07.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSObject.h"

@interface LDSChild : LDSObject

- (void) action;

@property (strong, nonatomic) NSString* lastName;

@end
