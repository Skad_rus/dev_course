//
//  LDSObject.h
//  ArraysTest
//
//  Created by Admin on 07.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSObject : NSObject


@property (strong, nonatomic) NSString* name;

- (void) action;
@end
