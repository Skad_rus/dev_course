//
//  LDSChild.m
//  ArraysTest
//
//  Created by Admin on 07.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSChild.h"

@implementation LDSChild

- (void) action {
    NSLog(@"%@ Child action", self.name);
}

@end
