//
//  LDSAppDelegate.m
//  ArraysTest
//
//  Created by Admin on 07.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSObject.h"
#import "LDSChild.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    
    
    //NSArray* array = [[NSArray alloc] initWithObjects:@"String1",@"String2",@"String3", nil];
    
    /*
    for (int i = 0; i < [array count]; i++) {
        NSLog(@"%@", [array objectAtIndex:i]);
        NSLog(@"i = %d", i);
    }
     */
    
    //NSArray* array = [NSArray arrayWithObjects:@"String1",@"String2",@"String3", nil];
    
   /* 
    NSArray* array = @[@"String1",@"String2",@"String3"];
    
    for (NSString* string in array) {
        NSLog(@"%@", string);
        NSLog(@"index = %d", [array indexOfObject:string]);

    }
    */
    
    LDSObject* obj1 = [[LDSObject alloc] init];
    LDSObject* obj2 = [[LDSObject alloc] init];
    LDSChild* obj3 = [[LDSChild alloc] init];
    
    NSArray* array = [NSArray arrayWithObjects:obj1, obj2, obj3, nil];
    
    
    [obj1 setName:@"Object 1"];
    [obj2 setName:@"Object 2"];
    [obj3 setName:@"Object 3"];
    
    obj3.lastName = @"Last Name";
    
    for (LDSObject* obj in array){
        NSLog(@"Name = %@", obj.name);
        [obj action];
        
        if([obj isKindOfClass:[LDSChild class]]){
            
            LDSChild* Child = (LDSChild*) obj;
            
            NSLog(@"Last name = %@", Child.lastName);
            
        }
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
