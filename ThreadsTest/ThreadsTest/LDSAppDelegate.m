//
//  LDSAppDelegate.m
//  ThreadsTest
//
//  Created by Admin on 05.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//
/*
 Я честно говоря не знаю когда вы будете напрямую использовать NSThread, я рекомендую всегда, когда надо делать что-то в бэкграунде, то использовать либо GCD (Grand Central Dispatch) либо NSOperation. Так что задание будет соответствующее :)
 
 Ученик.
 
 1. Создайте класс студент. У него должен быть метод - угадать ответ :)
 2. В метод передается случайное целое число например в диапазоне от 0 - 100 (или больше) и сам диапазон, чтобы студент знал где угадывать
 3. Студент генерирует случайное число в том же диапазоне пока оно не будет искомым
 4. Весь процесс угадывания реализуется в потоке в классе студент
 5. Когда студент досчитал то пусть пишет в НСЛог
 6. Создайте 5 студентов и дайте им одну и туже задачу и посмотрите кто справился с ней лучше
 
 Студент.
 
 7. Задача та же, но вместе с условием передавайте студенту блок, в котором вы и объявите результаты
 8. Блок должен определяться в томже классе, где и определялись студенты
 9. Блок должен быть вызван на главном потоке
 
 Мастер.
 
 10. Создать приватный метод класса (да да, приватный метод да еще и с плюсом), который будет возвращать статическую (то есть одну на все объекты класса студент) dispatch_queue_t, которая инициализируется при первом обращении к этому методу.
 11. Лучше в этом методе реализовать блок dispatch_once, ищите в инете как и зачем :) А что, программист всегда что-то ищет и хороший программист всегда находит.
 12. Все студенты должны выполнять свои процессы в этой queue и она должна быть CONCURRENT, типа все блоки одновременно выполняются
 
 Супермен.
 
 13. Добавьте еще один класс студента, который делает все тоже самое что вы реализовали до этого, только вместо GCD он использует NSOperation и NSOperationQueue. Вообще вынос мозга в самостоятельной работе :)
 14. Все сделавшие Мастера и Супермена и с красивым кодом получают отдельный огромный РЕСПЕКТ, так как они это на самом деле заслуживают.
*/
#import "LDSAppDelegate.h"
#import "LDSStudent.h"
#import "LDSStudentOperation.h"

@interface LDSAppDelegate ()

@property (strong, nonatomic) NSMutableArray* array;

@end

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    /*[self performSelectorInBackground:@selector(testThread) withObject:nil];

    for (int i = 0; i < 10; i++){
        
        NSThread* thread = [[NSThread alloc] initWithTarget:self selector:@selector(testThread) object:nil];
        thread.name = [NSString stringWithFormat:@"Thread #%d", i + 1];
        [thread start];
    
    }
    
    
    
    NSThread* thread1 = [[NSThread alloc] initWithTarget:self selector:@selector(addStringToArray:) object:@"x"];
    NSThread* thread2 = [[NSThread alloc] initWithTarget:self selector:@selector(addStringToArray:) object:@"0"];
    thread1.name = [NSString stringWithFormat:@"Thread 1"];
    thread2.name = [NSString stringWithFormat:@"Thread 2"];
    [thread1 start];
    [thread2 start];
    
    self.array = [NSMutableArray array];
    
    [self performSelector:@selector(printArray) withObject:nil afterDelay:3];
    
    
    
    self.array = [NSMutableArray array];
    
    dispatch_queue_t quene = dispatch_queue_create("com.daniillobanov.testthreads.queue", DISPATCH_QUEUE_SERIAL);
    
    __weak id weakSelf = self;
    
    dispatch_async(quene, ^{
        double stratTime = CACurrentMediaTime();
        
        NSLog(@"%@ started", [[NSThread currentThread] name]);
        
        for (int i =0; i < 20000000; i++){
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@ finished in %f", [[NSThread currentThread] name], CACurrentMediaTime() - stratTime);

        });
        [weakSelf addStringToArray:@"x"];
    });
    
    dispatch_async(quene, ^{
        [weakSelf addStringToArray:@"0"];
    });
    
    dispatch_async(quene, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            //UI refresh
        });
    });
    

    
    [self performSelector:@selector(printArray) withObject:nil afterDelay:3];
    */
    
    void (^resultBlock) (NSString*) = ^(NSString* string){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@", string);
        });
    };
    
    LDSStudent* student1 = [[LDSStudent alloc] init];
    LDSStudent* student2 = [[LDSStudent alloc] init];
    LDSStudent* student3 = [[LDSStudent alloc] init];
    LDSStudent* student4 = [[LDSStudent alloc] init];
    LDSStudent* student5 = [[LDSStudent alloc] init];
    
    student1.name = @"Student1";
    student2.name = @"Student2";
    student3.name = @"Student3";
    student4.name = @"Student4";
    student5.name = @"Student5";
    
    NSInteger rand = arc4random() %100;
    
    NSLog(@"Randomized number is %d", rand);
    NSLog(@" ");
    
    NSArray* arrayOfStudents = [NSArray arrayWithObjects:student1, student2, student3, student4, student5, nil];
    
    for (LDSStudent* student in arrayOfStudents){
        
        [student guess:rand lowerLimit:0 higherLimit:10000000 andResultBlock: resultBlock];
    
    }
    
    LDSStudentOperation* student6 = [[LDSStudentOperation alloc] init];
    LDSStudentOperation* student7 = [[LDSStudentOperation alloc] init];
    LDSStudentOperation* student8 = [[LDSStudentOperation alloc] init];
    LDSStudentOperation* student9 = [[LDSStudentOperation alloc] init];
    LDSStudentOperation* student10 = [[LDSStudentOperation alloc] init];
    
    student6.name = @"Student6";
    student7.name = @"Student7";
    student8.name = @"Student8";
    student9.name = @"Student9";
    student10.name = @"Student10";
    
    NSArray* arrayOfStudentOperation = [NSArray arrayWithObjects:student6, student7, student8, student9, student10, nil];
    
    for (LDSStudentOperation* student in arrayOfStudentOperation){
        
        [student guess:rand lowerLimit:0 higherLimit:10000000 andResultBlock: resultBlock];
        
    }
    
    return YES;
}

- (void) testThread {
    
    @autoreleasepool {
        
        double stratTime = CACurrentMediaTime();
        
        NSLog(@"%@ started", [[NSThread currentThread] name]);
        
        @synchronized(self){
            
            NSLog(@"%@ calculation started", [[NSThread currentThread] name]);
            for (int i =0; i < 200000; i++){
            }
            NSLog(@"%@ calculation finished", [[NSThread currentThread] name]);
        }
        NSLog(@"%@ finished in %f", [[NSThread currentThread] name], CACurrentMediaTime() - stratTime);
        
    }
    
}

- (void) printArray {
    NSLog(@"%@", self.array);
}

- (void) addStringToArray:(NSString*) string {
    
    @autoreleasepool {
        
        double stratTime = CACurrentMediaTime();
        
        NSLog(@"%@ started", [[NSThread currentThread] name]);
        
        //@synchronized(self){
            
            NSLog(@"%@ calculation started", [[NSThread currentThread] name]);
            for (int i = 0; i < 20000; i++){
                [self.array addObject:string];
            }
            
            NSLog(@"%@ calculation finished", [[NSThread currentThread] name]);
            
        //}
        
        NSLog(@"%@ finished in %f", [[NSThread currentThread] name], CACurrentMediaTime() - stratTime);
            
        
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
