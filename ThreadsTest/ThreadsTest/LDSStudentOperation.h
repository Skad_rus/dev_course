//
//  LDSStudentOperation.h
//  ThreadsTest
//
//  Created by Admin on 06.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSStudentOperation : NSObject

@property (strong, nonatomic) NSString* name;

- (void) guess:(NSInteger) guesses
    lowerLimit:(NSInteger) lowerLimit
   higherLimit:(NSInteger) higherLimit
andResultBlock: (void(^)(NSString*))resultBlock;

@end
