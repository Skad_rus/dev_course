//
//  LDSStudent.m
//  ThreadsTest
//
//  Created by Admin on 05.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSStudent.h"

@interface LDSStudent ()

+ (dispatch_queue_t) queueMethod;

@end


@implementation LDSStudent

+ (dispatch_queue_t) queueMethod{
    
    static dispatch_queue_t staticQueue;
    static dispatch_once_t task;
    
    dispatch_once(&task, ^{
        staticQueue = dispatch_queue_create("com.daniillobanov.testthreads.taskqueue", DISPATCH_QUEUE_CONCURRENT);
    });

    return staticQueue;
}

- (void) guess:(NSInteger) guesses
    lowerLimit:(NSInteger) lowerLimit
   higherLimit:(NSInteger) higherLimit
andResultBlock: (void(^)(NSString*))resultBlock{
    
    
    dispatch_async ([LDSStudent queueMethod], ^{
        
        NSInteger j = 0;
        NSInteger i = 0;
        
        do{
            
            i++;
            j = lowerLimit + arc4random() % (higherLimit - lowerLimit);
            
        }while (j != guesses);
        
        resultBlock([NSString stringWithFormat:@"Student %@ guessed the %d attempt", self.name, i]);
    });

}

@end

