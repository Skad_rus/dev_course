//
//  LDSStudentOperation.m
//  ThreadsTest
//
//  Created by Admin on 06.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSStudentOperation.h"

@interface LDSStudentOperation ()

+ (NSOperationQueue*) queueMethod;

@end


@implementation LDSStudentOperation


+ (NSOperationQueue*) queueMethod{
    
    static NSOperationQueue* operationQueue = nil;
    static dispatch_once_t task;
    
    dispatch_once(&task, ^{
        operationQueue = [[NSOperationQueue alloc] init];
    });
    
    return operationQueue;
}


- (void) guess:(NSInteger) guesses
    lowerLimit:(NSInteger) lowerLimit
   higherLimit:(NSInteger) higherLimit
andResultBlock: (void(^)(NSString*))resultBlock{
    
        
    __block NSInteger j = 0;
    __block NSInteger i = 0;
    
    
    [[LDSStudentOperation queueMethod] addOperationWithBlock:^{
    //[[[NSOperationQueue alloc] init] addOperationWithBlock:^{
        
        __weak LDSStudentOperation* weakSelf = self;
            
        do{
            
            i++;
            j = lowerLimit + arc4random() % (higherLimit - lowerLimit);
            
        }while (j != guesses);
        
        resultBlock([NSString stringWithFormat:@"Student %@ guessed the %d attempt", weakSelf.name, i]);
            
    }];
    
}
@end
