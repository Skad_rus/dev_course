//
//  LDSStudent.h
//  ThreadsTest
//
//  Created by Admin on 05.06.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSStudent : NSObject

@property (strong, nonatomic) NSString* name;

- (void) guess:(NSInteger) guesses
    lowerLimit:(NSInteger) lowerLimit
   higherLimit:(NSInteger) higherLimit
andResultBlock: (void(^)(NSString*))resultBlock;

@end
