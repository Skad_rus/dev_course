//
//  LDSBoxer.h
//  PropertiesTest
//
//  Created by Admin on 02.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSBoxer : NSObject

@property(strong, nonatomic) NSString* name;
@property(assign, nonatomic) NSInteger age;
@property(assign, nonatomic) CGFloat height;
@property(assign, nonatomic) CGFloat weight;

- (NSInteger) howOldAreYou;

@end
