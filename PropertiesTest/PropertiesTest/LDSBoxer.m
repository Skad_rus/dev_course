//
//  LDSBoxer.m
//  PropertiesTest
//
//  Created by Admin on 02.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSBoxer.h"

@implementation LDSBoxer
@synthesize name = _name;



-(void) setName:(NSString *) inputName {
    NSLog(@"setter setName: is called");
    
    _name = inputName;
}

- (NSString*) name {
    return _name;
}

-(NSInteger) age {
    
    NSLog(@"age getter is called");
    
    return _age;
}

- (NSInteger) howOldAreYou{
    return self.age;
}

@end
