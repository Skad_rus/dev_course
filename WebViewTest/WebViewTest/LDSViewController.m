//
//  LDSViewController.m
//  WebViewTest
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSViewController.h"

@interface LDSViewController ()

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    //Вариант с загрузкой странички по url
    /*
    NSString* urlString = @"http://vk.com/iosdevcourse";
    
    NSURL* url = [NSURL URLWithString:urlString];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
     */
    
    //Открыть PDF через  data
    /*
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"1.pdf" ofType:nil];
    
    NSData* fileData = [NSData dataWithContentsOfFile:filePath];
    
    [self.webView loadData:fileData MIMEType:@"application/pdf" textEncodingName:@"UTF-8" baseURL:nil];
    */
    
    //Открыть PDF через  request
    /*
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"2.pdf" ofType:nil];
    
    NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:fileUrl];
    
    [self.webView loadRequest:request];
     */
    
    [self.webView loadRequest:self.request];
    
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Actions

- (IBAction) actionBack:(UIBarButtonItem*)sender{
    
    if ([self.webView canGoBack]) {
        [self.webView stopLoading];
        [self.webView goBack];
    }
    
}

- (IBAction) actionForward:(UIBarButtonItem*)sender{
    
    if ([self.webView canGoForward]) {
        [self.webView stopLoading];
        [self.webView goForward];
    }
    
}

- (IBAction) actionRefresh:(UIBarButtonItem*)sender{
    
    [self.webView stopLoading];
    [self.webView reload];
    
}

#pragma mark - Methods

- (void) refreshButtons {
    
    self.backButtonItem.enabled = [self.webView canGoBack];
    self.forwardButtonItem.enabled = [self.webView canGoForward];
    
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSLog(@"shouldStartLoadWithRequest %@", [request description]);
    
    return YES;
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    NSLog(@"webViewDidStartLoad");
    
    [self.indicator startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    NSLog(@"webViewDidFinishLoad");
    
    [self.indicator stopAnimating];
    
    [self refreshButtons];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    NSLog(@"didFailLoadWithError %@", [error localizedDescription]);
    
    [self.indicator stopAnimating];
    
    [self refreshButtons];

}

@end
