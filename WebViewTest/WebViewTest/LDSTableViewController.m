//
//  LDSTableViewController.m
//  WebViewTest
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSTableViewController.h"

@interface LDSTableViewController ()

@property (strong, nonatomic) NSArray* typesArray;

@end

@implementation LDSTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray* sitesArray = [[NSArray alloc] initWithObjects:@"http://vk.com/iosdevcourse", @"http://stackoverflow.com/", @"http://habrahabr.ru/", nil];
    NSArray* pdfArray = [[NSArray alloc] initWithObjects:@"1.pdf", @"2.pdf", @"3.pdf", nil];
    
    self.typesArray = [[NSArray alloc] initWithObjects:sitesArray, pdfArray, nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.typesArray count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [[self.typesArray objectAtIndex:section] count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.text = [[self.typesArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    LDSViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LDSViewController"];

    NSString* objectName = [[self.typesArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if (indexPath.section == 0) {
        
        NSString* urlString = objectName;
        
        NSURL* url = [NSURL URLWithString:urlString];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
        
        vc.request = request;
        
    } else if (indexPath.section == 1) {
        
        NSString* filePath = [[NSBundle mainBundle] pathForResource:objectName ofType:nil];
        
        NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:fileUrl];
        
        vc.request = request;
        
    }
    
    [self.navigationController pushViewController:vc animated:YES];
    
    //Вариант с загрузкой странички по url
    /*
     NSString* urlString = @"http://vk.com/iosdevcourse";
     
     NSURL* url = [NSURL URLWithString:urlString];
     
     NSURLRequest* request = [NSURLRequest requestWithURL:url];
     
     [self.webView loadRequest:request];
     */
    
    //Открыть PDF через  data
    /*
     NSString* filePath = [[NSBundle mainBundle] pathForResource:@"1.pdf" ofType:nil];
     
     NSData* fileData = [NSData dataWithContentsOfFile:filePath];
     
     [self.webView loadData:fileData MIMEType:@"application/pdf" textEncodingName:@"UTF-8" baseURL:nil];
     */
    
    //Открыть PDF через  request
    /*
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"2.pdf" ofType:nil];
    
    NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:fileUrl];
    
    [self.webView loadRequest:request];
    */
    
}

@end
