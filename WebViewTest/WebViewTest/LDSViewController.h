//
//  LDSViewController.h
//  WebViewTest
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView* webView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* indicator;

@property (weak, nonatomic) IBOutlet UIBarButtonItem* backButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* forwardButtonItem;

@property (weak, nonatomic) NSURLRequest* request;

- (IBAction) actionBack:(UIBarButtonItem*)sender;
- (IBAction) actionForward:(UIBarButtonItem*)sender;
- (IBAction) actionRefresh:(UIBarButtonItem*)sender;

@end
