//
//  LDSViewController.h
//  ButtonsTest
//
//  Created by Admin on 09.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *indicatorLabel;

- (IBAction)actionTest2:(UIButton *)sender;

- (IBAction)actionTest2TouchDown:(UIButton *) sender;

@end
