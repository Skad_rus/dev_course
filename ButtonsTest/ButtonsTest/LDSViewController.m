//
//  LDSViewController.m
//  ButtonsTest
//
//  Created by Admin on 09.08.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 Ученик.
 
 1. Создайте и разместите кнопки цифр и операторов, создайте нужные акшины
 2. Добавьте лейбл и создайте соответствующий проперти
 3. По нажатию на кнопку на индикатор должна выводиться либо цифра, либо оператор (никаких вычислений - выводим просто значения кнопок)
 
 Студент.
 
 4. Наведите более / менее красоту, можете насоздавать линий (вьюхи с малой шириной)б ставить бэкграундыб используйте картинки если надо и тд. - проявите творчество :)
 5. Сделайте так, чтобы когда нажимаешь на цифровую кнопку, то число росло
 6. Сделайте кнопку сброса
 
 Мастер.
 
 7. Сохраняйте вводимое число
 8. Первую операцию тоже надо сохранить, пока не ввели второе число (используйте енумы!)
 9. Когда нажимаешь равно то первое число выболняет операцию над вторым и выводится результат
 10. После того, как результат вывелся на экран, он автоматом становиться первым числом и к нему уже можно прибавлять, вычитать и т.д. - как в калькуляторе
 
 Супермен
 
 11. Добавьте точку, теперь можно вводить и дробное число, точка должна работать также, как и точка на обычном калькуляторе (имею в виду, что двух точек в числе быть не может в случае повторного нажатия)
 12. С дробным вводом будет посложнее - задание для настоящих суперменов :)
*/

#import "LDSViewController.h"

@interface LDSViewController ()

enum operations {
    percent,
    divide,
    multiply,
    minus,
    plus
};

@property (assign, nonatomic) CGFloat firstNumber;
@property (assign, nonatomic) CGFloat secondNumber;
@property (assign, nonatomic) CGFloat firstFractional;
@property (assign, nonatomic) CGFloat secondFractional;
@property (assign, nonatomic) NSInteger chosenOperaion;
@property (assign, nonatomic) BOOL flagOperaion;


@end

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.firstNumber        = 0;
    self.secondNumber       = 0;
    self.chosenOperaion     = 0;
    self.firstFractional    = 10;
    self.secondFractional   = 10;
    self.flagOperaion       = NO;
    
	// Do any additional setup after loading the view, typically from a nib.
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(200, 200, 200, 200);
    button.backgroundColor = [UIColor lightGrayColor];
    
    /*
    NSDictionary* attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:30],
                                 NSForegroundColorAttributeName: [UIColor orangeColor]};
    
    NSAttributedString* title = [[NSAttributedString alloc] initWithString:@"Button" attributes:attributes];
    
    [button setAttributedTitle:title forState:UIControlStateNormal];
    
    NSDictionary* attributes2 = @{NSFontAttributeName: [UIFont systemFontOfSize:20],
                                 NSForegroundColorAttributeName: [UIColor redColor]};
    
    NSAttributedString* title2 = [[NSAttributedString alloc] initWithString:@"Button pressed" attributes:attributes2];
    
    [button setAttributedTitle:title2 forState:UIControlStateHighlighted];
    */
    
    
    [button setTitle:@"Button" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button setTitle:@"Button Pressed" forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
    
    /*
    UIEdgeInsets inset = UIEdgeInsetsMake(100, 100, 0, 0);
    button.titleEdgeInsets = inset;
    */
    
    //[self.view addSubview:button];
    
    [button addTarget:self action:@selector(actionTest: event:) forControlEvents:UIControlEventTouchUpInside];
    
    [button addTarget:self action:@selector(actionTestOutside:) forControlEvents:UIControlEventTouchUpOutside];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Operaions

- (void) numberIn:(UIButton *)sender{

    if (self.flagOperaion == NO) {
        self.firstNumber = self.firstNumber * 10 + sender.tag;
        
        self.indicatorLabel.text = [NSString stringWithFormat:@"%f",self.firstNumber / 10 * self.firstFractional];

        if (self.firstFractional < 10) {

            self.firstFractional = self.firstFractional / 10;
        
        }
        
    } else if (self.flagOperaion == YES) {
        
        self.secondNumber = self.secondNumber * 10 + sender.tag;
        
        self.indicatorLabel.text = [NSString stringWithFormat:@"%f",self.secondNumber / 10 * self.secondFractional];

        if (self.secondFractional < 10) {

            self.secondFractional = self.secondFractional / 10;
            
        }

    }
    

}

- (void) operationCancel:(UIButton *)sender{
    
    self.indicatorLabel.text = @"AC";

    self.firstNumber        = 0;
    self.secondNumber       = 0;
    self.chosenOperaion     = 0;
    self.firstFractional    = 10;
    self.secondFractional   = 10;
    self.flagOperaion       = NO;
    
}

- (void) changeSign:(UIButton *)sender{
    
    if (self.flagOperaion == NO) {
        self.firstNumber = self.firstNumber * -1;
        self.indicatorLabel.text = [NSString stringWithFormat:@"%f",self.firstNumber];
    } else if (self.flagOperaion == YES) {
        self.secondNumber = self.secondNumber * -1;
        self.indicatorLabel.text = [NSString stringWithFormat:@"%f",self.secondNumber];
    }
    

}

- (void) chosenOperaion:(UIButton *)sender{
    
    switch (sender.tag) {
            
        case 30:
            self.indicatorLabel.text = @"%";
            self.chosenOperaion = 1;
            self.flagOperaion = YES;
            break;
            
        case 40:
            self.indicatorLabel.text = @"÷";
            self.chosenOperaion = 2;
            self.flagOperaion = YES;
            break;
            
        case 50:
            self.indicatorLabel.text = @"x";
            self.chosenOperaion = 3;
            self.flagOperaion = YES;
            break;
            
        case 60:
            self.indicatorLabel.text = @"-";
            self.chosenOperaion = 4;
            self.flagOperaion = YES;
            break;
            
        case 70:
            self.indicatorLabel.text = @"+";
            self.chosenOperaion = 5;
            self.flagOperaion = YES;
            break;
            
        case 80:
            [self equalsOperaion:sender];
            self.chosenOperaion = 0;
            break;
    }

    
}

- (void) equalsOperaion:(UIButton *)sender{
    
    NSLog(@"Fract %f, %f",self.firstFractional, self.secondFractional);
    if (self.firstFractional == 10) {
        self.firstFractional = 1;
    }
    
    if (self.secondFractional == 10) {
        self.secondFractional = 1;
    }

    switch (self.chosenOperaion) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            self.indicatorLabel.text = [NSString stringWithFormat:@"%f",
                                        ( self.firstNumber / 10 * self.firstFractional * 10 ) / ( self.secondNumber / 10 * self.secondFractional * 10 )];
            self.flagOperaion = NO;
            break;
        case 3:
            self.indicatorLabel.text = [NSString stringWithFormat:@"%f",
                                        ( self.firstNumber / 10 * self.firstFractional * 10 ) * ( self.secondNumber / 10 * self.secondFractional * 10 )];
            self.flagOperaion = NO;
            break;
        case 4:
            self.indicatorLabel.text = [NSString stringWithFormat:@"%f",
                                        ( self.firstNumber / 10 * self.firstFractional * 10 ) - ( self.secondNumber / 10 * self.secondFractional * 10 )];
            self.flagOperaion = NO;
            break;
        case 5:
            self.indicatorLabel.text = [NSString stringWithFormat:@"%f",
                                        ( self.firstNumber / 10 * self.firstFractional * 10 ) + ( self.secondNumber / 10 * self.secondFractional * 10 )];
            self.flagOperaion = NO;
            break;
    }
    
    NSLog(@"%f,%f",( self.firstNumber / 10 * self.firstFractional * 10 ), ( self.secondNumber / 10 * self.secondFractional * 10 ));

    self.firstNumber    = 0;
    self.secondNumber   = 0;


}

- (void) fractionalOption:(UIButton *)sender{
 
    if (self.flagOperaion == NO) {
        self.firstFractional = 1;
    } else if (self.flagOperaion == YES) {
        self.secondFractional = 1;
    }
    
}

#pragma mark - Actions

- (void) actionTest:(UIButton*) but event:(UIEvent*) event {
    
    NSLog(@"Button pressed inside!");
    
}

- (void) actionTestOutside:(UIButton*) but {
    
    NSLog(@"Button pressed outside!");
    
}

- (IBAction)actionTest2:(UIButton *)sender {
    
    NSLog(@"actionTest2 tag = %d", sender.tag);
    
    switch (sender.tag) {
            
        case 10:
            [self operationCancel:sender];
            break;
            
        case 20:
            [self changeSign:sender];
            break;
            
        case 30:
            [self chosenOperaion:sender];
            break;
            
        case 40:
            [self chosenOperaion:sender];
            break;
            
        case 50:
            [self chosenOperaion:sender];
            break;
            
        case 60:
            [self chosenOperaion:sender];
            break;
            
        case 70:
            [self chosenOperaion:sender];
            break;
            
        case 80:
            [self chosenOperaion:sender];
            break;
            
        case 90:
            [self fractionalOption:sender];
            break;
            
            
        default:
            [self numberIn:sender];
            break;
    }

}

- (IBAction)actionTest2TouchDown:(UIButton *)sender{
    
    
    
}

@end
