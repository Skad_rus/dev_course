//
//  LDSViewController.m
//  GesturesTest
//
//  Created by Admin on 29.07.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

/*
 
 Мастер
 
 5. Если я делаю свайп вправо, то давайте картинке анимацию поворота по часовой стрелке на 360 градусов
 6. То же самое для свайпа влево, только анимация должна быть против часовой (не забудьте остановить предыдущее кручение)
 7. По двойному тапу двух пальцев останавливайте анимацию
 
*/
#import "LDSViewController.h"

@interface LDSViewController () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) UIImageView* testImageView;

@property (assign, nonatomic) CGFloat testViewScale;
@property (assign, nonatomic) CGFloat testViewRotation;
@property (assign, nonatomic) CGPoint tapLocation;

@end

@implementation LDSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView* view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1.png"]];
    view.frame = CGRectMake(CGRectGetMidX(self.view.bounds) - 50, CGRectGetMidY(self.view.bounds) - 50, 100, 100);
    
    view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
                            UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    self.testImageView = view;
    
    [self.view addSubview:self.testImageView];
	
    //Tap
    UITapGestureRecognizer* tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                         action:@selector(handleTap:)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    //Double tap
    UITapGestureRecognizer* doubleTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleDoubleTap:)];
    
    doubleTapGesture.numberOfTapsRequired = 2;
    
    [self.view addGestureRecognizer:doubleTapGesture];
    
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];
    
    //Double tap double touch
    UITapGestureRecognizer* doubleTapDoubleTouchGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleDoubleTapDoubleTouch:)];
    
    doubleTapDoubleTouchGesture.numberOfTapsRequired = 2;
    doubleTapDoubleTouchGesture.numberOfTouchesRequired = 2;
    
    [self.view addGestureRecognizer:doubleTapDoubleTouchGesture];
    
    //Pinch
    UIPinchGestureRecognizer* pinchGesture =
    [[UIPinchGestureRecognizer alloc]initWithTarget:self
                                             action:@selector(handePinch:)];
    
    pinchGesture.delegate = self;
    
    [self.view addGestureRecognizer:pinchGesture];
    
    //Rotation
    UIRotationGestureRecognizer* rotationGesture =
    [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
    
    rotationGesture.delegate = self;
    
    [self.view addGestureRecognizer:rotationGesture];
    
    //Pan
    UIPanGestureRecognizer* panGesture =
    [[UIPanGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handlePan:)];
    
    panGesture.delegate = self;
    
    [self.view addGestureRecognizer:panGesture];
    
    //Swipe right
    UISwipeGestureRecognizer* rightSwipeGessure =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipe:)];
    
    rightSwipeGessure.direction = UISwipeGestureRecognizerDirectionRight;
    rightSwipeGessure.delegate = self;
    
    [self.view addGestureRecognizer:rightSwipeGessure];
    
    //Swipe left
    UISwipeGestureRecognizer* leftSwipeGessure =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipe:)];
    
    leftSwipeGessure.direction = UISwipeGestureRecognizerDirectionLeft;
    leftSwipeGessure.delegate = self;
    
    [self.view addGestureRecognizer:leftSwipeGessure];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Methods

//Метод для задания случайного флоат от 0.f до 1.f
- (CGFloat) randomFromZeroToOne {
    
    return (float)(arc4random() % 256) / 255;
    
}

//Метод для задания произвольного цвета
- (UIColor*) randomColor {
    
    CGFloat r = [self randomFromZeroToOne];
    CGFloat g = [self randomFromZeroToOne];
    CGFloat b = [self randomFromZeroToOne];
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

//Метод, реализующий поворот на 360
- (void)rotateSpinningView:(NSInteger) i {
    UIImageView* spiningView = self.testImageView;
    
    [UIView animateWithDuration:1.075 delay:0 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState animations:^{
        [spiningView setTransform:CGAffineTransformRotate(self.testImageView.transform, i * M_PI_2)];
    } completion:^(BOOL finished) {
        if (finished && !CGAffineTransformEqualToTransform(spiningView.transform, CGAffineTransformIdentity)) {
            [self rotateSpinningView:i];
        }
    }];
}



#pragma mark - Gestures

- (void) handleRightSwipe:(UISwipeGestureRecognizer*) swipeGesture {

    NSLog(@"Right Swipe");
    
    [self.testImageView.layer removeAllAnimations];
    [self rotateSpinningView: - 1];
    
}

- (void) handleLeftSwipe:(UISwipeGestureRecognizer*) swipeGesture {
    
    NSLog(@"Left Swipe");
    
    [self.testImageView.layer removeAllAnimations];
    [self rotateSpinningView: 1];

}

- (void) handlePan:(UIPanGestureRecognizer*) panGesture {
    
    NSLog(@"handlePan");
    
    self.testImageView.center = [panGesture locationInView:self.view];

}

- (void) handleRotation:(UIRotationGestureRecognizer*) rotationGesture {
    
    NSLog(@"handleRotation %1.3f", rotationGesture.rotation);
    
    if (rotationGesture.state == UIGestureRecognizerStateBegan) {
        
        self.testViewRotation = 0.f;
        
    }
    
    CGFloat newRotation = rotationGesture.rotation - self.testViewRotation;
    
    CGAffineTransform currentTransform = self.testImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform, newRotation);
    
    self.testImageView.transform = newTransform;
    
    self.testViewRotation = rotationGesture.rotation;
    
}

- (void) handePinch:(UIPinchGestureRecognizer*) pinchGesture {
    
    NSLog(@"handePinch %1.3f", pinchGesture.scale);
    
    if (pinchGesture.state == UIGestureRecognizerStateBegan) {
        
        self.testViewScale = 1.f;
        
    }
    
    CGFloat newScale = 1.f + pinchGesture.scale - self.testViewScale;
    
    CGAffineTransform currentTransform = self.testImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, newScale, newScale);
    
    self.testImageView.transform = newTransform;

    
    self.testViewScale = pinchGesture.scale;
    
}

- (void) handleTap:(UIGestureRecognizer*) tapGesture {
    
    NSLog(@"Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
    
    //self.testView.backgroundColor = [self randomColor];
    
    self.tapLocation = [tapGesture locationInView:self.view];
    
    [UIView animateWithDuration:2.3 delay:0 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        self.testImageView.center = self.tapLocation;

    } completion:^(BOOL finished) {
        
    }];

}

- (void) handleDoubleTap:(UIGestureRecognizer*) tapGesture {
    
    NSLog(@"Double Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
    
    CGAffineTransform currentTransform = self.testImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, 1.2f, 1.2f);

    [UIView animateWithDuration:0.3 animations:^{
        
        self.testImageView.transform = newTransform;

        
    }];
    
    self.testViewScale = 1.2f;

}

- (void) handleDoubleTapDoubleTouch:(UIGestureRecognizer*) tapGesture {
    
    NSLog(@"Double Tap Double Touch: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
    
    
    //[self.testImageView.layer removeAllAnimations];
    
    
    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.testImageView setTransform:CGAffineTransformRotate(self.testImageView.transform, 0)];
    } completion:^(BOOL finished) {
        
    }];
    
    /*
    //CGAffineTransform currentTransform = self.testView.transform;
    CGAffineTransform currentTransform = self.testImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, 0.8f, 0.8f);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        //self.testView.transform = newTransform;
        self.testImageView.transform = newTransform;

        
    }];
    
    self.testViewScale = 0.8f;
    */
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    
    return YES;
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]];
    
}

@end
