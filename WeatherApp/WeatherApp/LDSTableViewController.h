//
//  LDSTableViewController.h
//  WeatherApp
//
//  Created by Admin on 06.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSTableViewController : UITableViewController


- (IBAction)addCity:(id)sender;
- (IBAction)manageCities:(id)sender;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *daytimeTemp;
@property (weak, nonatomic) IBOutlet UILabel *nightTemp;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImg;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *nextDayOutletColl;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *nextDayWeatherViewOutletColl;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *nextDayDaytimeTempOutletColl;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *nextDayNightTempOutletColl;

@end
