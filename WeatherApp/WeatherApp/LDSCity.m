//
//  LDSCity.m
//  WeatherApp
//
//  Created by Admin on 08.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSCity.h"

@implementation LDSCity

- (instancetype)initWithName:(NSString*) cityName{
    self = [super init];
    if (self) {
        
        [self fillAllFieldsWithName:cityName];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    
    NSString* string = nil;
    
    [encoder encodeObject:self.name forKey:@"name"];
    
    [encoder encodeObject:self.daytimeTemp forKey:@"daytimeTemp"];
    [encoder encodeObject:self.nightTemp forKey:@"nightTemp"];
    string = [NSString stringWithFormat:@"%d", self.weather];
    [encoder encodeObject:string forKey:@"weather"];
    
    [encoder encodeObject:self.daytimeTemp1 forKey:@"daytimeTemp1"];
    [encoder encodeObject:self.nightTemp1 forKey:@"nightTemp1"];
    string = [NSString stringWithFormat:@"%d", self.weather1];
    [encoder encodeObject:string forKey:@"weather1"];
    
    [encoder encodeObject:self.daytimeTemp2 forKey:@"daytimeTemp2"];
    [encoder encodeObject:self.nightTemp2 forKey:@"nightTemp2"];
    string = [NSString stringWithFormat:@"%d", self.weather2];
    [encoder encodeObject:string forKey:@"weather2"];
    
    [encoder encodeObject:self.daytimeTemp3 forKey:@"daytimeTemp3"];
    [encoder encodeObject:self.nightTemp3 forKey:@"nightTemp3"];
    string = [NSString stringWithFormat:@"%d", self.weather3];
    [encoder encodeObject:string forKey:@"weather3"];
    
    [encoder encodeObject:self.daytimeTemp4 forKey:@"daytimeTemp4"];
    [encoder encodeObject:self.nightTemp4 forKey:@"nightTemp4"];
    string = [NSString stringWithFormat:@"%d", self.weather4];
    [encoder encodeObject:string forKey:@"weather4"];
    
    [encoder encodeObject:self.daytimeTemp5 forKey:@"daytimeTemp5"];
    [encoder encodeObject:self.nightTemp5 forKey:@"nightTemp5"];
    string = [NSString stringWithFormat:@"%d", self.weather5];
    [encoder encodeObject:string forKey:@"weather5"];
    
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        
        NSString* string = nil;
        
        self.name = [decoder decodeObjectForKey:@"name"];
        
        self.daytimeTemp = [decoder decodeObjectForKey:@"daytimeTemp"];
        self.nightTemp = [decoder decodeObjectForKey:@"nightTemp"];
        string = [decoder decodeObjectForKey:@"weather"];
        self.weather = [string intValue];
        
        self.daytimeTemp1 = [decoder decodeObjectForKey:@"daytimeTemp1"];
        self.nightTemp1 = [decoder decodeObjectForKey:@"nightTemp1"];
        string = [decoder decodeObjectForKey:@"weather1"];
        self.weather1 = [string intValue];
        
        self.daytimeTemp2 = [decoder decodeObjectForKey:@"daytimeTemp2"];
        self.nightTemp2 = [decoder decodeObjectForKey:@"nightTemp2"];
        string = [decoder decodeObjectForKey:@"weather2"];
        self.weather2 = [string intValue];
        
        self.daytimeTemp3 = [decoder decodeObjectForKey:@"daytimeTemp3"];
        self.nightTemp3 = [decoder decodeObjectForKey:@"nightTemp3"];
        string = [decoder decodeObjectForKey:@"weather3"];
        self.weather3 = [string intValue];
        
        self.daytimeTemp4 = [decoder decodeObjectForKey:@"daytimeTemp4"];
        self.nightTemp4 = [decoder decodeObjectForKey:@"nightTemp4"];
        string = [decoder decodeObjectForKey:@"weather4"];
        self.weather4 = [string intValue];
        
        self.daytimeTemp5 = [decoder decodeObjectForKey:@"daytimeTemp5"];
        self.nightTemp5 = [decoder decodeObjectForKey:@"nightTemp5"];
        string = [decoder decodeObjectForKey:@"weather5"];
        self.weather5 = [string intValue];   
        
    }
    return self;
}

- (void) fillAllFieldsWithName:(NSString*) cityName {

    self.name = cityName;
    
    NSInteger minTemp = arc4random() % 11;
    
    self.daytimeTemp = [NSString stringWithFormat:@"%d°", minTemp + arc4random() % 21];
    self.nightTemp = [NSString stringWithFormat:@"%d°", minTemp];
    self.weather = arc4random() % 3;
    
    self.daytimeTemp1 = [NSString stringWithFormat:@"%d°", minTemp + arc4random() % 21];
    self.nightTemp1 = [NSString stringWithFormat:@"%d°", minTemp];
    self.weather1 = arc4random() % 3;
    
    self.daytimeTemp2 = [NSString stringWithFormat:@"%d°", minTemp + arc4random() % 21];
    self.nightTemp2 = [NSString stringWithFormat:@"%d°", minTemp];
    self.weather2 = arc4random() % 3;
    
    self.daytimeTemp3 = [NSString stringWithFormat:@"%d°", minTemp + arc4random() % 21];
    self.nightTemp3 = [NSString stringWithFormat:@"%d°", minTemp];
    self.weather3 = arc4random() % 3;
    
    self.daytimeTemp4 = [NSString stringWithFormat:@"%d°", minTemp + arc4random() % 21];
    self.nightTemp4 = [NSString stringWithFormat:@"%d°", minTemp];
    self.weather4 = arc4random() % 3;
    
    self.daytimeTemp5 = [NSString stringWithFormat:@"%d°", minTemp + arc4random() % 21];
    self.nightTemp5 = [NSString stringWithFormat:@"%d°", minTemp];
    self.weather5 = arc4random() % 3;
    
}

@end
