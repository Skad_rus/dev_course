//
//  LDSTableViewController.m
//  WeatherApp
//
//  Created by Admin on 06.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSTableViewController.h"
#import "LDSCity.h"
#import "LDSCitiesController.h"
#import "LDSAddCityController.h"

@interface LDSTableViewController () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSMutableArray* arrayOfCities;
@property (assign, nonatomic) NSInteger cityIndex;

@end

@implementation LDSTableViewController
{
    NSCache* citiesCache;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.arrayOfCities = [NSMutableArray array];
    
    //Создание нескольких объектов для первого запуска
    /*
    for (int i = 0; i < 3; i++) {
     
        switch (i) {
            case 0:{
                LDSCity* city = [[LDSCity alloc] initWithName:@"Minsk"];
                [self.arrayOfCities addObject:city];
                break;
            }
                
            case 1:{
                LDSCity* city = [[LDSCity alloc] initWithName:@"Moscow"];
                [self.arrayOfCities addObject:city];
                break;
            }
                
            case 2:{
                LDSCity* city = [[LDSCity alloc] initWithName:@"St. Petersburg"];
                [self.arrayOfCities addObject:city];
                break;
            }
        }
    }
    
    [self saveToDefaults];
    */
    
    [self loadFromDefaults];
    
    self.cityIndex = 0;
    
    self.pageControl.currentPage = self.cityIndex;
    self.pageControl.hidesForSinglePage = NO;
    
    self.tableView.allowsSelection = NO;

    [self addGestures];
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    if (self.cityIndex >= [self.arrayOfCities count]) {
        self.cityIndex = 0;
    }
    
    self.pageControl.currentPage = self.cityIndex;
    
    [self saveSettings];
    
    [self fillAllFields];

    self.pageControl.numberOfPages = [self.arrayOfCities count];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Save and Load

//Прошу обратить внимание на то, что я не видел смысла использовать кешированние данных при столь малых объемах данных (даже в случае получения данных с сервера), потому я реализовал его чисто номинально.

- (void) saveSettings {

    citiesCache = [[NSCache alloc] init];
    [citiesCache setObject:self.arrayOfCities forKey:@"key"];
    
}

- (void) loadSettings {
    
    self.arrayOfCities = [citiesCache objectForKey:@"key"];
    
}

- (void) saveToDefaults {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.arrayOfCities];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:@"arrKey"];
    [defaults synchronize];
    
}

- (void) loadFromDefaults {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"arrKey"];
    self.arrayOfCities = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    [self saveSettings];
    
}

#pragma mark - Methods

- (void) fillAllFields {
    
    [self loadSettings];
    
    //Main cell
    LDSCity* city = [self.arrayOfCities objectAtIndex:self.cityIndex];

    self.cityName.text = city.name;

    self.daytimeTemp.text = city.daytimeTemp;
    self.nightTemp.text = city.nightTemp;

    switch (city.weather) {
        case 0:
            self.weatherImg.image = [UIImage imageNamed:@"sum100.png"];
            break;
            
        case 1:
            self.weatherImg.image = [UIImage imageNamed:@"cloud100.png"];
            break;
            
        case 2:
            self.weatherImg.image = [UIImage imageNamed:@"rain100.png"];
            break;
    }
    
    //Other cells
    NSDate* date = nil;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    
    for (int i = 0; i < 5; i++) {
        
        date = [NSDate dateWithTimeIntervalSinceNow:i*24*3600];
        UILabel* label = nil;
        
        label = [self.nextDayOutletColl objectAtIndex:i];
        label.text = [dateFormatter stringFromDate:date];
        
        NSString* daytimeTemp;
        NSString* nightTemp;
        NSInteger weather = 0;
        
        switch (i) {
                
            case 0:
                daytimeTemp     =   city.daytimeTemp1;
                nightTemp       =   city.nightTemp1;
                weather         =   city.weather1;
                break;
                
            case 1:
                daytimeTemp     =   city.daytimeTemp2;
                nightTemp       =   city.nightTemp2;
                weather         =   city.weather2;
                break;
                
            case 2:
                daytimeTemp     =   city.daytimeTemp3;
                nightTemp       =   city.nightTemp3;
                weather         =   city.weather3;
                break;
                
            case 3:
                daytimeTemp     =   city.daytimeTemp4;
                nightTemp       =   city.nightTemp4;
                weather         =   city.weather4;
                break;
                
            case 4:
                daytimeTemp     =   city.daytimeTemp5;
                nightTemp       =   city.nightTemp5;
                weather         =   city.weather5;
                break;
                
        }
        
        label = [self.nextDayDaytimeTempOutletColl objectAtIndex:i];
        label.text = daytimeTemp;
        
        label = [self.nextDayNightTempOutletColl objectAtIndex:i];
        label.text = nightTemp;
        
        UIImageView* imView = [self.nextDayWeatherViewOutletColl objectAtIndex:i];
        
        switch (weather) {
                
            case 0:
                imView.image = [UIImage imageNamed:@"sum33.png"];
                break;
                
            case 1:
                imView.image = [UIImage imageNamed:@"cloud33.png"];
                break;
                
            case 2:
                imView.image = [UIImage imageNamed:@"rain33.png"];
                break;
                
        }
          
    }

}

#pragma mark - Gestures

- (void) addGestures {
    
    //Swipe right
    UISwipeGestureRecognizer* rightSwipeGessure =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipe:)];
    
    rightSwipeGessure.direction = UISwipeGestureRecognizerDirectionRight;
    rightSwipeGessure.delegate = self;
    
    [self.view addGestureRecognizer:rightSwipeGessure];
    
    //Swipe left
    UISwipeGestureRecognizer* leftSwipeGessure =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipe:)];
    
    leftSwipeGessure.direction = UISwipeGestureRecognizerDirectionLeft;
    leftSwipeGessure.delegate = self;
    
    [self.view addGestureRecognizer:leftSwipeGessure];
    
}

- (void) handleRightSwipe:(UISwipeGestureRecognizer*) swipeGesture {
    
    self.cityIndex = self.cityIndex - 1;
    
    if (self.cityIndex < 0) {
        self.cityIndex = [self.arrayOfCities count] - 1;
    }
    
    self.pageControl.currentPage = self.cityIndex;
    
    [self fillAllFields];
    
}

- (void) handleLeftSwipe:(UISwipeGestureRecognizer*) swipeGesture {
    
    self.cityIndex = self.cityIndex + 1;
    
    if (self.cityIndex > [self.arrayOfCities count] - 1) {
        self.cityIndex = 0;
    }
    
    self.pageControl.currentPage = self.cityIndex;
    
    [self fillAllFields];

}

#pragma mark - Actions

- (IBAction)addCity:(id)sender {
    
    LDSAddCityController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LDSAddCityController"];
    vc.arrayOfCities = self.arrayOfCities;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (IBAction)manageCities:(id)sender {
    
    LDSCitiesController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LDSCitiesController"];
    vc.arrayOfCities = self.arrayOfCities;
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
