//
//  ScopedSearchController.h
//  WeatherApp
//
//  Created by DANIIL LOBANOV on 30.12.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "SABaseViewController.h"

@interface ScopedSearchController : SABaseViewController <UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UITextField *woNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchByButton;
@property (weak, nonatomic) IBOutlet UITableView *searchByTypeTableView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIImageView *fadeTopImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fadeBottomImageView;

- (IBAction)cancelButtonPressed:(UIButton *)sender;
- (IBAction)searchByButtonPressed:(UIButton *)sender;
- (IBAction)searchButtonPressed:(UIButton *)sender;

@property (assign, nonatomic) NSInteger initHeight;
@property (assign, nonatomic) NSInteger searchHeight;

@end