//
//  LDSCity.h
//  WeatherApp
//
//  Created by Admin on 08.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSCity : NSObject <NSCoding>

@property (strong, nonatomic) NSString* name;

@property (strong, nonatomic) NSString* daytimeTemp;
@property (strong, nonatomic) NSString* nightTemp;
@property (assign, nonatomic) NSInteger weather;

@property (strong, nonatomic) NSString* daytimeTemp1;
@property (strong, nonatomic) NSString* nightTemp1;
@property (assign, nonatomic) NSInteger weather1;

@property (strong, nonatomic) NSString* daytimeTemp2;
@property (strong, nonatomic) NSString* nightTemp2;
@property (assign, nonatomic) NSInteger weather2;

@property (strong, nonatomic) NSString* daytimeTemp3;
@property (strong, nonatomic) NSString* nightTemp3;
@property (assign, nonatomic) NSInteger weather3;

@property (strong, nonatomic) NSString* daytimeTemp4;
@property (strong, nonatomic) NSString* nightTemp4;
@property (assign, nonatomic) NSInteger weather4;

@property (strong, nonatomic) NSString* daytimeTemp5;
@property (strong, nonatomic) NSString* nightTemp5;
@property (assign, nonatomic) NSInteger weather5;

- (instancetype)initWithName:(NSString*) cityName;

@end

