//
//  LDSCitiesController.h
//  WeatherApp
//
//  Created by Admin on 08.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSCitiesController : UITableViewController

@property (strong, nonatomic) NSMutableArray* arrayOfCities;

@end
