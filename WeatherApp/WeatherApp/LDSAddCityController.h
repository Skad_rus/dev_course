//
//  LDSAddCityController.h
//  WeatherApp
//
//  Created by Admin on 08.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDSAddCityController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) NSMutableArray* arrayOfCities;

- (IBAction)didEndAction:(id)sender;

@end
