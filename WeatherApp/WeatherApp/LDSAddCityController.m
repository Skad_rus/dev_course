//
//  LDSAddCityController.m
//  WeatherApp
//
//  Created by Admin on 08.10.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAddCityController.h"
#import "LDSCity.h"

@interface LDSAddCityController ()

@end

@implementation LDSAddCityController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.textField becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (IBAction)didEndAction:(id)sender {
    
    LDSCity* city = [[LDSCity alloc] initWithName:self.textField.text];
    [self.arrayOfCities insertObject:city atIndex:0];
    [self saveToDefaults];
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Save and Load

- (void) saveToDefaults {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.arrayOfCities];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:@"arrKey"];
    [defaults synchronize];
    
}

@end
