//
//  ScopedSearchController.m
//  WeatherApp
//
//  Created by DANIIL LOBANOV on 30.12.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "ScopedSearchController.h"

@interface ScopedSearchController ()

@end

@implementation ScopedSearchController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setConstraintProperties];
    //[self setInitRects];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self setInitRects];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Methods

-(void) setConstraintProperties{
    
    self.initHeight = 20 + self.woNumberTextField.frame.size.height + 8 + self.searchByButton.frame.size.height + 8 + self.searchButton.frame.size.height + 20;
    
    self.searchHeight = 20 + self.woNumberTextField.frame.size.height + 8 + self.searchByTypeTableView.frame.size.height + 8 + self.searchButton.frame.size.height + 20;
    
}

-(void) setInitRects{
    
    CGRect filterRect               = CGRectMake(self.filterView.frame.origin.x,
                                                 self.filterView.frame.origin.y,
                                                 self.filterView.frame.size.width,
                                                 self.initHeight);
    
    CGRect choseTableViewRect       = CGRectMake(320,
                                                 58,
                                                 self.searchByTypeTableView.frame.size.width,
                                                 self.searchByTypeTableView.frame.size.height);
    
    CGRect searchByButtonRect       = CGRectMake(20,
                                                 58,
                                                 self.searchByButton.frame.size.width,
                                                 self.searchByButton.frame.size.height);
    
    
    
    self.filterView.frame               = filterRect;
    self.searchByTypeTableView.frame    = choseTableViewRect;
    self.searchByButton.frame           = searchByButtonRect;
    
}

-(void) setSearchByRects{
    
    CGRect filterViewRect           = CGRectMake(0/*self.filterView.frame.origin.x*/,
                                                 self.filterView.frame.origin.y,
                                                 self.filterView.frame.size.width,
                                                 self.searchHeight);
    
    __block CGRect searchByTableViewRect    = CGRectMake(0,
                                                         58,
                                                         self.searchByTypeTableView.frame.size.width,
                                                         self.searchByTypeTableView.frame.size.height);
    
    CGRect searchByButtonRect       = CGRectMake(-300,
                                                 58,
                                                 self.searchByButton.frame.size.width,
                                                 self.searchByButton.frame.size.height);
    
    CGRect oldSearchByButtonRect    = CGRectMake(self.searchByButton.frame.origin.x - 20,
                                                 self.searchByButton.frame.origin.y,
                                                 self.searchByButton.frame.size.width + 40,
                                                 self.searchByButton.frame.size.height);
    
    
    NSLog(@"Before     %@", NSStringFromCGRect(self.searchByTypeTableView.frame));
    
    self.searchByTypeTableView.frame = CGRectMake(320,
                                                  58,
                                                  self.searchByTypeTableView.frame.size.width,
                                                  self.searchByButton.frame.size.height);
    
    //[self.filterView            removeConstraints:self.filterView.constraints];
    
    [UIView animateWithDuration:0.15 animations:^{
        
        self.searchByButton.frame           = searchByButtonRect;
        self.searchByTypeTableView.frame    = oldSearchByButtonRect;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.15
                         animations:^{
                             
                             self.filterView.frame               = filterViewRect;
                             self.searchByTypeTableView.frame    = searchByTableViewRect;
                             
                             NSLog(@"Middle     %@", NSStringFromCGRect(self.searchByTypeTableView.frame));
                             
                             
                         } completion:^(BOOL finished) {
                             
                             NSLog(@"Finished   %@", NSStringFromCGRect(self.searchByTypeTableView.frame));
                             /*
                              [UIView animateWithDuration:0.15
                              animations:^{
                              self.searchByTypeTableView.frame    = searchByTableViewRect;
                              } completion:^(BOOL finished) {
                              NSLog(@"Finaly      %@", NSStringFromCGRect(self.searchByTypeTableView.frame));
                              }
                              ];
                              */
                         }
         ];
        
    }];
    
    
    
}

-(void) showSerachTypeModeTableView{
    
    
    
}

-(void) showSearchView{
    
    CGRect filterRect       = CGRectMake(0,
                                         20,
                                         self.filterView.frame.size.width,
                                         self.filterView.frame.size.height   -   self.searchByTypeTableView.frame.size.height);
    NSLog(@"Before  = %@", NSStringFromCGRect(self.filterView.frame));
    NSLog(@"After   = %@", NSStringFromCGRect(filterRect));
    
    [UIView animateWithDuration:0.30 animations:^{
        self.filterView.frame = filterRect;
    } completion:^(BOOL finished) {
        [self.woNumberTextField becomeFirstResponder];
    }];
    
}

-(void) hideSearchView{
    
    CGRect filterRect       = CGRectMake(320,
                                         20,
                                         self.filterView.frame.size.width    +   self.searchByTypeTableView.frame.size.width,
                                         self.filterView.frame.size.height);
    
    [UIView animateWithDuration:0.3 animations:^{
        self.filterView.frame = filterRect;
    } completion:^(BOOL finished) {
        [self.woNumberTextField resignFirstResponder];
    }];
    
}

-(void) showChooseTableView{
    
    CGRect filterRect       = CGRectMake(0,
                                         20,
                                         self.filterView.frame.size.width    +   self.searchByTypeTableView.frame.size.width,
                                         self.filterView.frame.size.height   +   self.searchByTypeTableView.frame.size.height);
    
    CGRect tableViewRect    = CGRectMake(0,
                                         96,
                                         self.searchByTypeTableView.frame.size.width,
                                         self.searchByTypeTableView.frame.size.height);
    
    [UIView animateWithDuration:0.15 animations:^{
        
        self.filterView.frame = filterRect;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.15 animations:^{
            self.searchByTypeTableView.frame = tableViewRect;
        }];
        
    }];
    
}

-(void) hideChooseTableView{
    
    CGRect filterRect       = CGRectMake(0,
                                         20,
                                         self.filterView.frame.size.width    -   self.searchByTypeTableView.frame.size.width,
                                         self.filterView.frame.size.height   -   self.searchByTypeTableView.frame.size.height);
    
    CGRect tableViewRect    = CGRectMake(320,
                                         96,
                                         self.searchByTypeTableView.frame.size.width,
                                         self.searchByTypeTableView.frame.size.height);
    
    [UIView animateWithDuration:0.15 animations:^{
        
        self.searchByTypeTableView.frame = tableViewRect;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.15 animations:^{
            self.filterView.frame = filterRect;
        }];
        
    }];
    
}

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchByButtonPressed:(UIButton *)sender{
    [self setSearchByRects];
}

- (IBAction)searchButtonPressed:(UIButton *)sender{
    
}

@end
