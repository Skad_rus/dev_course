//
//  LDSHuman.h
//  DictionaryTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSHuman : NSObject

@property (strong, nonatomic) NSString* name;
@property (weak, nonatomic) NSString* lastName;
@property (weak, nonatomic) NSString* greetingPhrase;

@end
