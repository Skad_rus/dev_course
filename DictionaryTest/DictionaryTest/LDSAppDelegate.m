//
//  LDSAppDelegate.m
//  DictionaryTest
//
//  Created by Admin on 28.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSAppDelegate.h"
#import "LDSHuman.h"

@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    
    /*
    NSDictionary* dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"Petrov",@"lastName",
                                @"Vasiliy", @"name",
                                @25, @"age",
                                nil];
    
    NSLog(@"%@", dictionary);
    
    NSLog(@"%@, \ncount = %d", dictionary, [dictionary count]);
    
    NSLog(@"name = %@, lastname = %@, age = %d", [dictionary objectForKey:@"name"], [dictionary objectForKey:@"lastName"], [[dictionary objectForKey:@"age"] integerValue]);
    
    for (NSString* key in [dictionary allKeys]) {
        id obj = [dictionary objectForKey:key];
        
        NSLog(@"key = %@, value = %@", key, obj);
    }
    */
    
    LDSHuman* human1 = [[LDSHuman alloc] init];
    LDSHuman* human2 = [[LDSHuman alloc] init];
    LDSHuman* human3 = [[LDSHuman alloc] init];
    LDSHuman* human4 = [[LDSHuman alloc] init];
    LDSHuman* human5 = [[LDSHuman alloc] init];
    LDSHuman* human6 = [[LDSHuman alloc] init];
    LDSHuman* human7 = [[LDSHuman alloc] init];
    LDSHuman* human8 = [[LDSHuman alloc] init];
    LDSHuman* human9 = [[LDSHuman alloc] init];
    
    human1.name = @"human1";
    human2.name = @"human2";
    human3.name = @"human3";
    human4.name = @"human4";
    human5.name = @"human5";
    human6.name = @"human6";
    human7.name = @"human7";
    human8.name = @"human8";
    human9.name = @"human9";
    
    human1.lastName = @"Petrov";
    human2.lastName = @"Ivanov";
    human3.lastName = @"Sidorov";
    human4.lastName = @"Sleptsov";
    human5.lastName = @"Cherkasin";
    human6.lastName = @"Knyazev";
    human7.lastName = @"Vochehovskii";
    human8.lastName = @"Domogarov";
    human9.lastName = @"Metelskii";
    
    human1.greetingPhrase = @"human1 welcomes you!";
    human2.greetingPhrase = @"human2 welcomes you!";
    human3.greetingPhrase = @"human3 welcomes you!";
    human4.greetingPhrase = @"human4 welcomes you!";
    human5.greetingPhrase = @"human5 welcomes you!";
    human6.greetingPhrase = @"human6 welcomes you!";
    human7.greetingPhrase = @"human7 welcomes you!";
    human8.greetingPhrase = @"human8 welcomes you!";
    human9.greetingPhrase = @"human9 welcomes you!";
    
    NSDictionary* dictionary2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 /*
                                 [human1 greetingPhrase], [[[human1 name] stringByAppendingString:@" "] stringByAppendingString: [human1 lastName]],
                                 [human2 greetingPhrase], [[[human2 name] stringByAppendingString:@" "] stringByAppendingString: [human2 lastName]],
                                 [human3 greetingPhrase], [[[human3 name] stringByAppendingString:@" "] stringByAppendingString: [human3 lastName]],
                                 [human4 greetingPhrase], [[[human4 name] stringByAppendingString:@" "] stringByAppendingString: [human4 lastName]],
                                 [human5 greetingPhrase], [[[human5 name] stringByAppendingString:@" "] stringByAppendingString: [human5 lastName]],
                                 [human6 greetingPhrase], [[[human6 name] stringByAppendingString:@" "] stringByAppendingString: [human6 lastName]],
                                 [human7 greetingPhrase], [[[human7 name] stringByAppendingString:@" "] stringByAppendingString: [human7 lastName]],
                                 [human8 greetingPhrase], [[[human8 name] stringByAppendingString:@" "] stringByAppendingString: [human8 lastName]],
                                 [human9 greetingPhrase], [[[human9 name] stringByAppendingString:@" "] stringByAppendingString: [human9 lastName]],
                                  */
                                 human1, [[[human1 name] stringByAppendingString:@" "] stringByAppendingString: [human1 lastName]],
                                 human2, [[[human2 name] stringByAppendingString:@" "] stringByAppendingString: [human2 lastName]],
                                 human3, [[[human3 name] stringByAppendingString:@" "] stringByAppendingString: [human3 lastName]],
                                 human4, [[[human4 name] stringByAppendingString:@" "] stringByAppendingString: [human4 lastName]],
                                 human5, [[[human5 name] stringByAppendingString:@" "] stringByAppendingString: [human5 lastName]],
                                 human6, [[[human6 name] stringByAppendingString:@" "] stringByAppendingString: [human6 lastName]],
                                 human7, [[[human7 name] stringByAppendingString:@" "] stringByAppendingString: [human7 lastName]],
                                 human8, [[[human8 name] stringByAppendingString:@" "] stringByAppendingString: [human8 lastName]],
                                 human9, [[[human9 name] stringByAppendingString:@" "] stringByAppendingString: [human9 lastName]],
                                 nil];
    NSLog(@"%@", dictionary2);
    
    for (id key in dictionary2) {
        id obj = [dictionary2 objectForKey:key];
        NSLog(@"%@, his phrase is = %@", key, [obj greetingPhrase]);
    }
    
     NSLog(@" ");
    
    NSArray* arrayOfKeys = [dictionary2 allKeys];
    NSArray* sortedArrayOfKeys = [arrayOfKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (id key in sortedArrayOfKeys) {
        id obj = [dictionary2 objectForKey:key];
        NSLog(@"%@, his phrase is = %@", key, [obj greetingPhrase]);
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
