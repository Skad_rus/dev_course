//
//  LDSFootbaler.h
//  les5hw1
//
//  Created by Admin on 11.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import "LDSHuman.h"

@interface LDSFootbaler : LDSHuman

- (void) move;

@property (assign, nonatomic) CGFloat Speed;

@end
