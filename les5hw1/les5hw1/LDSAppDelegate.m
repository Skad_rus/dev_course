//
//  LDSAppDelegate.m
//  les5hw1
//
//  Created by Admin on 07.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//


#import "LDSAppDelegate.h"
#import "LDSHuman.h"
#import "LDSBeast.h"

#import "LDSSwimmer.h"
#import "LDSRunner.h"
#import "LDSCyclist.h"
#import "LDSFootbaler.h"

#import "LDSTiger.h"
#import "LDSGiraffe.h"


@implementation LDSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    LDSCyclist* athlete1 = [[LDSCyclist alloc] init];
    LDSRunner* athlete2 = [[LDSRunner alloc] init];
    LDSSwimmer* athlete3 = [[LDSSwimmer alloc] init];
    LDSFootbaler* athlete4 = [[LDSFootbaler alloc] init];
 
    
    [athlete1 setName:@"Ivan"];
    [athlete2 setName:@"Maxim"];
    [athlete3 setName:@"Ira"];
    [athlete4 setName:@"Dmitrii"];
    
    [athlete1 setGender:@"Male"];
    [athlete2 setGender:@"Male"];
    [athlete3 setGender:@"Female"];
    [athlete4 setGender:@"Male"];
    
    [athlete1 setHeight:1.7f];
    [athlete2 setHeight:2.1f];
    [athlete3 setHeight:1.5f];
    [athlete4 setHeight:1.6f];
    
    [athlete1 setWeight:82.5f];
    [athlete2 setWeight:95.1f];
    [athlete3 setWeight:63.5f];
    [athlete4 setWeight:58.7f];
    
    [athlete4 setSpeed:34.7f];
    
    
    LDSTiger* beast1 = [[LDSTiger alloc] init];
    LDSGiraffe* beast2 = [[LDSGiraffe alloc] init];
    
    [beast1 setSpecies:@"Tiger"];
    [beast2 setSpecies:@"Giraffe"];
    
    [beast1 setWeight:96.7f];
    [beast2 setWeight:86.3f];
    
    [beast1 setFeeding:@"Carnivorous"];
    [beast2 setFeeding:@"Herbivorous"];
   
    
    NSArray* array1 = [NSArray arrayWithObjects:athlete1, athlete2, athlete3, athlete4, nil];
    NSArray* array2 = [NSArray arrayWithObjects: beast1, beast2, nil];
    

    if (array1.count > array2.count) {
    
    for (int i = array1.count - 1; i >= array2.count  ; i--) {
        
        LDSHuman* objH = [array1 objectAtIndex:i];
        
            
            NSLog(@"This is human");
        
            NSLog(@"Name = %@", objH.name);
            NSLog(@"Gender = %@", objH.gender);
            NSLog(@"Height = %f", objH.height);
            NSLog(@"Weight = %f", objH.weight);
        
            if([objH isKindOfClass:[LDSFootbaler class]]){
            
                LDSFootbaler* Athlete = (LDSFootbaler*) objH;

                NSLog(@"Unique action = %f", Athlete.Speed);
                
            }
    
            [objH move];

            
        }
        
    }
        for (int i = array2.count - 1; i >= 0 ; i--) {
            
            LDSHuman* objH = [array1 objectAtIndex:i];
            LDSBeast* objB = [array2 objectAtIndex:i];
            
            
            
            NSLog(@"This is human");
            
            NSLog(@"Name = %@", objH.name);
            NSLog(@"Gender = %@", objH.gender);
            NSLog(@"Height = %f", objH.height);
            NSLog(@"Weight = %f", objH.weight);
            
            if([objH isKindOfClass:[LDSFootbaler class]]){
                
                LDSFootbaler* Athlete = (LDSFootbaler*) objH;
                
                NSLog(@"Unique action = %f", Athlete.Speed);
                
            }
            
            [objH move];
            
            
            if([objB isKindOfClass:[LDSBeast class]]){
                
                NSLog(@"This is beast");
                
                NSLog(@"Species = %@", objB.species);
                NSLog(@"Feeding = %@", objB.feeding);
                NSLog(@"Weight = %f", objB.weight);
                
                [objB move];
                
        }
            
            
        else {
                
                
                for (int i = array2.count - 1; i >= array1.count ; i--) {
                    
                    LDSBeast* objB = [array2 objectAtIndex:i];
                    
                        
                        NSLog(@"This is beast");
                        
                        NSLog(@"Species = %@", objB.species);
                        NSLog(@"Feeding = %@", objB.feeding);
                        NSLog(@"Weight = %f", objB.weight);
                        
                        [objB move];
                    
            }
                
                for (int i = array1.count - 1; i >= 0 ; i--) {
                    
                    LDSHuman* objH = [array1 objectAtIndex:i];
                    LDSBeast* objB = [array2 objectAtIndex:i];
                    
                    
                    
                    NSLog(@"This is human");
                    
                    NSLog(@"Name = %@", objH.name);
                    NSLog(@"Gender = %@", objH.gender);
                    NSLog(@"Height = %f", objH.height);
                    NSLog(@"Weight = %f", objH.weight);
                    
                    if([objH isKindOfClass:[LDSFootbaler class]]){
                        
                        LDSFootbaler* Athlete = (LDSFootbaler*) objH;
                        
                        NSLog(@"Unique action = %f", Athlete.Speed);
                        
                    }
                    
                    [objH move];
                    
                    
                    if([objB isKindOfClass:[LDSBeast class]]){
                        
                        NSLog(@"This is beast");
                        
                        NSLog(@"Species = %@", objB.species);
                        NSLog(@"Feeding = %@", objB.feeding);
                        NSLog(@"Weight = %f", objB.weight);
                        
                        [objB move];
                        
                    }
                }
    
                
        }
    
}
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
