//
//  LDSHuman.h
//  les5hw1
//
//  Created by Admin on 07.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSHuman : NSObject

- (void) move;

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat weight;
@property (weak, nonatomic) NSString* gender;

@end
