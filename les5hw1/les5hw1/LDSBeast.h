//
//  LDSBeast.h
//  les5hw1
//
//  Created by Admin on 11.04.14.
//  Copyright (c) 2014 Daniil Lobanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDSBeast : NSObject

- (void) move;

@property (strong, nonatomic) NSString* species;
@property (assign, nonatomic) CGFloat weight;
@property (weak, nonatomic) NSString* feeding;

@end
